<?php


if (!function_exists('response'))
{	
	function response($code, $message, $data, $errors, $filterData = array())
	{
		$out = [
			'code' => $code,
			'message' => $message,
			'data' => $data,
			'errors' => $errors
		];

		if(is_array($filterData) && count($filterData) > 0){
			$out["filter_data"] = $filterData;
		}
		return $out;
	}
}