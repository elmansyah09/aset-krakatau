<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CI_Efr_excel {

	public $spreadsheet = null;	
	function __construct(){
		$this->_ci = &get_instance();
	}
	
	function createSheet(){
		$spreadsheet = new Spreadsheet();
		$this->spreadsheet = $spreadsheet;
		return $this->spreadsheet;
	}

	public function download($filename = "",$spreadsheet)
	{
		$writer = new Xlsx($spreadsheet);
		// $filename = 'laporan-siswa';
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}

}