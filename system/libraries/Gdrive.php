<?php 


/**
 * 
 */

require_once(APPPATH_SYSTEM.'libraries/Gdrive/src/Google_Client.php');
require_once(APPPATH_SYSTEM.'libraries/Gdrive/src/contrib/Google_DriveService.php');

class CI_Gdrive
{
	
	function __construct()
	{
		$url_array = explode('?', 'http://'.$_SERVER ['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		$url = $url_array[0];		
		$this->client = new Google_Client();
		$this->client->setClientId('830866807308-j66rrir593nqpaerornri2d3ou2a39ds.apps.googleusercontent.com');
		$this->client->setClientSecret('_4EQauKNCUTG9i0113nUcLr3');
		$this->client->setRedirectUri($url);
		$this->client->setScopes(array('https://www.googleapis.com/auth/drive'));

		if (isset($_GET['code'])) {
		    $_SESSION['accessToken'] = $this->client->authenticate($_GET['code']);
		    header('location:'.$url);exit;
		} elseif (!isset($_SESSION['accessToken'])) {
		    $this->client->authenticate();
		}
	}

	function _accessTokenService()
	{
		$this->client->setAccessToken($_SESSION['accessToken']);
	    return new Google_DriveService($this->client);
		 
	}

	public function createFolder($folder_name = "",$description = "")
	{		
	    $service =  $this->_accessTokenService();
	    $fileResult = false;
	    if (!empty($folder_name)) {
	    	$resCheck = $this->getFolder(array("name"=>$folder_name));
	    	if (isset($resCheck["items"]) && is_array($resCheck["items"]) && count($resCheck["items"]) == 0) {	    	
			    $file = new Google_DriveFile();
			    $file->setTitle($folder_name);
			 	$file->setDescription($description);
			    $file->setMimeType('application/vnd.google-apps.folder');
			    $fileResult = $service->files->insert($file, array(
			        'fields' => 'id'));
	    	} else {
	    		 $fileResult = array("id"=>$resCheck["items"][0]["id"]);
	    	}
	    } else {
	    	show_auth_failed();
	    }

	    return $fileResult;
	}

	public function getFolder($args = array())
	{
		$service =  $this->_accessTokenService();
		$where = "  ";
		if (isset($args["name"]) && !empty($args["name"])) {
			$where .= " and title = '". $args["name"] ."' ";
		}
		$query = "mimeType = 'application/vnd.google-apps.folder' and trashed = FALSE $where";

		$params = array(
                "q" => $query
        );
        $result = array();
        $files = $service->files->listFiles($params); 

        return $files;
	}
}