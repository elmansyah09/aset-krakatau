<?php 

/**
 * Autoload Tools/File Utility
 *
 *
 * @category   Setup autoload Application
 * @package    Configuration
 * @author     Elmansyah Fauzi rachman
 * @copyright  2019 Elmansyah
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: config@0.01
 * @link       http://pear.php.net/package/PackageName
 */

	$auto["api"]['packages']   = array();
	$auto["api"]['libraries']  = array('database','session',"Efr_upload");
	$auto["api"]['helper']     = array('url',"efr_input","efr_common","efr_tanggal","efr_response");
	$auto["api"]['drivers']      = array();
	$auto["api"]['config']     = array();
	$auto["api"]['language']   = array();
	$auto["api"]['model']      = array();


 ?>