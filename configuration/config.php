<?php 

/**
 * Configuration Standard Application
 *
 *
 * @category   Config
 * @package    Configuration
 * @author     Elmansyah Fauzi rachman
 * @copyright  2021 Elmansyah
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: config@0.01
 * @link       http://pear.php.net/package/PackageName
 */

if(!isset($_SERVER['HTTP_HOST'])){
$_SERVER['HTTP_HOST']='localhost';
}

if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { 
    $protokol = "https";
}else{
    $protokol = "http";
}
$config['base_url'] = $protokol."://".$_SERVER['HTTP_HOST'];
$config['base_url'] .= preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])=="\\"?"":dirname($_SERVER['SCRIPT_NAME'])).'/';		

$config['application_name']	= "Sistem Pelayanan Kegiatan Posyandu";
$config['alamat'] = "Desa Sukamanah";
$config['alamat2'] = "Mande - Cianjur";

$config['assets_path'] = FCPATH2."assets/";
$config['upload_path'] = $config['assets_path']."uploads/";
$config['template_path'] = $config['assets_path']."templates/";
$config['foto_path'] = $config['upload_path']."foto/";
$config['url_asset'] = $config['base_url']."assets/";
$config['url_upload'] = $config['url_asset']."uploads/";
$config['url_foto'] = $config['url_upload']."foto/";

$config['media_path'] = FCPATH2."media/";
$config['media_public'] = $config['media_path']."public/";
$config['public_css'] = $config['media_public']."css/";
$config['public_js'] = $config['media_public']."js/";

$config['base_main'] = $config['base_url']."main/";

$config['url_media_path'] = $config['base_url']."media/";
$config['url_media_public'] = $config['url_media_path']."public/";
$config['url_public_css'] = $config['url_media_public']."css/";
$config['url_public_images'] = $config['url_media_public']."images/";
$config['url_public_js'] = $config['url_media_public']."js/";

$config['url_plugin'] = $config['url_media_path']."plugins/";

$config['url_src'] = $config['base_url']."/src/";
$config['url_src_pages'] = $config['url_src']."pages/";
$config['url_src_component'] = $config['url_src']."component/";

$config['encryptCode'] = "(*%&&NNV201753500756JKSJF6$*&";
$config['allowed_types'] 	= '*';

$config["powered_by"] = "Elmansyah Fauzi Rachman";

$config["limit_page"] = 20;
$config["maxsize"] = 2048;

 ?>