<?php 

/**
 * Database Configuration
 *
 *
 * @category   Config Database
 * @package    Configuration
 * @author     Elmansyah Fauzi rachman
 * @copyright  2021 Elmansyah
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: config@0.01
 * @link       http://pear.php.net/package/PackageName
 */


$database["api"] = array(
   'hostname' => "localhost",
   'username' => 'root',
   'password' => '',
   'database' => 'krakatau_aset',
   'dbdriver' => 'mysqli',
   'dbprefix' => '',
   'pconnect' => FALSE,
   'db_debug' => TRUE,
   'cache_on' => FALSE,
   'cachedir' => '',
   'char_set' => 'utf8',
   'dbcollat' => 'utf8_general_ci',
   'swap_pre' => '',
   'encrypt' => FALSE,
   'compress' => FALSE,
   'stricton' => FALSE,
   'autoinit' => FALSE,
   'failover' => array(),
);



 ?>