<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include FCPATH2."configuration/database.php";

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $database['api']['hostname'],
	'username' => $database['api']['username'],
	'password' => $database['api']['password'],
	'database' => $database['api']['database'],
	'dbdriver' => $database['api']['dbdriver'],
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

if(isset($database['mitra'])){
	$db['mitra'] = array(
		'dsn'	=> '',
		'hostname' => $database['mitra']['hostname'],
		'username' => $database['mitra']['username'],
		'password' => $database['mitra']['password'],
		'database' => $database['mitra']['database'],
		'dbdriver' => $database['mitra']['dbdriver'],
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt' => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
}