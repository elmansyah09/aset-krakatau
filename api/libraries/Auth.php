<?php

class Auth 
{

    function __construct()
	{
		$this->_ci = & get_instance();
    }

    function validate_token($token)
    {
        $out = true;
        $this->_ci->load->config('jwt');
        $this->_ci->load->library(array('Jwt'));						
        if (count(explode('.',$token)) == 3) 
        {		
            try{
                $decodedToken = JWT::decode($token,$this->_ci->config->item('jwt_key'),array('HS256'));
                if ($decodedToken != false) 
                {	        
                    $out = (array) $decodedToken;		        			        	
                } else {
                    $out = false;
                }
            }catch(ExpiredException $e){
                $out = false;
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        } else {
            $out = false;
        }		
        if ($out) {
            return $out;
        } else {
            show_auth_failed();
        }
    }
}


?>