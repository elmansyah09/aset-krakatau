<?php 

/**
 * Middleware Authentication
 *
 *
 * @category   Middleware Authentication
 * @package    Core
 * @author     Elmansyah Fauzi rachman
 * @copyright  2019 Elmansyah
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: config@0.01
 * @link       http://pear.php.net/package/PackageName
 */


/**
 * 
 */
class EFR_Controller extends CI_Controller
{
	
	function __construct($config = array())
	{
		parent::__construct();		
		$this->user = array();
		$this->menu = array();
		$this->initialize_middleware($config);
		
	}


	private function initialize_middleware($config = array())
	{

		$token = $this->get_token();
		if (is_array($config)) {
			$bypass = false;

			if(isset($config["iswb"]) && !empty($config["iswb"]) && $config["iswb"] == 989) {
				$bypass = true;
			}

			if (isset($config["allow_origin"]) && $config["allow_origin"]) {
				header('Access-Control-Allow-Origin: *');				
				header('Access-Control-Allow-Headers: *');	
			}

			// cek method disable auth
			if (isset($config["except_auth_method"]) && !empty($config["except_auth_method"])) {
				if (!is_array($config["except_auth_method"]) && strtolower($config["except_auth_method"]) === strtolower($this->uri->segment(2))) {
					$bypass = true;
				} else if (is_array($config["except_auth_method"]) && in_array($this->uri->segment(2),$config["except_auth_method"]) ) {
					$bypass = true;
				}			
			}
					
			// cek method enable auth
			if (isset($config["auth_method"]) && !empty($config["auth_method"])) {

				$resToken = null;
				if (!is_array($config["auth_method"]) && !$bypass && ($config["auth_method"] === ":ALL" || strtolower($config["auth_method"]) === strtolower($this->uri->segment(2)))) {
					$resToken = $this->validate_token($token);
				} else if ( !$bypass && (is_array($config["auth_method"]) && in_array($this->uri->segment(2),$config["auth_method"])) ) {
					$resToken = $this->validate_token($token);
				}			

				if($resToken && is_array($resToken)){
					if(isset($resToken["user"])){
						$this->user = (array) $resToken["user"];
					}
				}
			} 

		}

	}

	function _get_params()
	{
		$postdata = file_get_contents("php://input");
		$data = json_decode($postdata,true);		
		if(!$data){
			$data = array();
		}
		if(is_array($data) && count($data) == 0) {
			$data = $_POST;
		}
		if(is_array($data) && count($data) == 0) {
			$data = $_GET;

		}

		return $data;
	}

	function get_token()
	{
		$token = $this->session->userdata('token');
		
		
		if (!$token) {
			$token = ifunset($_GET,"token",false);		
		}

		if (!$token) {
			$header = getallheaders();			
			$token = ifunset($header,"token",false);		
		}

		return $token;
	}

	function get_header( $headerKey )
	{
	     $test = getallheaders();
	    if ( array_key_exists($headerKey, $test) ) {
	        $headerValue = $test[ $headerKey ];
	    }
	    return $headerValue;
	}

	function validate_token($token)
	{
		$out = true;
		$this->load->config('jwt');
		$this->load->library(array('Jwt'));						
		if (count(explode('.',$token)) == 3) 
		{		
			try{
				$decodedToken = JWT::decode($token,$this->config->item('jwt_key'),array('HS256'));
		        if ($decodedToken != false) 
		        {	        
		        	$out = (array) $decodedToken;		        			        						
		        } else {
		        	$out = false;
		        }
		    }catch(ExpiredException $e){
		    	$out = false;
		         echo 'Caught exception: ',  $e->getMessage(), "\n";
		    }
		} else {
			$out = false;
		}		
        if ($out) {
        	return $out;
        } else {
        	show_auth_failed();
        }
	}

	public function generate_token($data = array())
	{
		$this->load->config('jwt');
		$this->load->library(array('Jwt'));			
		$token = JWT::encode($data,$this->config->item('jwt_key'));
		return $token;
	}


	function get_data_app(){
		$data_app = array(        	
        	'base_url' => base_url(),
			'site_url' => site_url(),			
			'current_url' => current_url(),
			'base_main' => config_item("base_main"),
			'user' => $this->user,
			'menu' => $this->menu,			
			'segment' => array(
				1 => $this->uri->segment(1),
				2 => $this->uri->segment(2),
				3 => $this->uri->segment(3),
				4 => $this->uri->segment(4),
				5 => $this->uri->segment(5),
			)
        );
		
        return $data_app;
	}

	function get_user()
	{
		$user = array();
		if ($this->user && is_array($this->user)) {
			$user = $this->user;
		}
		return $user;
	}

	public function set_pagination($res, $link,$setting = array())
	{
		$this->load->library('pagination');

		$config['page_query_string'] = true;
		$config['reuse_query_string'] = true;
		$config['base_url'] = $link;
		$config['total_rows'] = 0;
		if (isset($res['data'][0]['num_rows'])) {
			$config['total_rows'] = $res['data'][0]['num_rows'];			
		}		
		if (isset($res['count'])) {
			$config['total_rows'] = $res['count'];			
		}		
		$config['per_page'] = ifunsetempty($setting,"per_page",10);
		

		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';

		$config['first_link'] = '|<i class="fa fa-angle-left"></i>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_url'] = '';

		$config['last_link'] = '<i class="fa fa-angle-right"></i>|';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['last_url'] = '';

		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a>';
		$config['cur_tag_close'] = '</a></li>';		

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';		
		$this->pagination->initialize($config);		
		return $this->pagination->create_links();
	}

	function _get_message($type = "common",$custom = array())
	{
		$message = array();

		$message["common"] = array(
			"success" => 'Transaksi Berhasil dilaksanakan',
			"failure" => 'Transaksi Gagal dilaksanakan'
		);
		$message["get"] = array(
			"success" => 'Get Data Berhasil',
			"failure" => 'Get Data Gagal'
		);
		$message["create"] = array(
			"success" => 'Data berhasil disimpan',
			"failure" => 'Data gagal disimpan'
		);
		$message["update"] = array(
			"success" => 'Data berhasil disimpan',
			"failure" => 'Data gagal disimpan'
		);
		$message["delete"] = array(
			"success" => 'Data berhasil dihapus',
			"failure" => 'Data gagal dihapus'
		);

		$out = (object) $message["common"];

		if(isset($message[$type])){
			$out = (object) $message[$type];
		}else{
			$out = (object) $custom;
		}
		return $out;

	}

	function _respon($in,$data = false,$type = "common",$message = array()){
		if($in){
			$out = array(
				'success' => true,
				'msg' => $this->_get_message($type,$message)->success
			);			
			if ($data) {
				$out = array_merge($out,$data);
			}
		}else{
			$out = array(
				'success' => false,
				'msg' => $this->_get_message($type,$message)->failure
				);
			if ($data) {
				$out = array_merge($out,$data);
			}
		}
		return $out;
	}
}