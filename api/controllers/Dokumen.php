	
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends EFR_Controller {

	public function __construct()
	{		
		$config = array(
			"allow_origin" => true,
			// "auth_method" => ":ALL",			
		);
		parent::__construct($config);
		$this->load->model("M_dokumen");		
	}


	function download($dokumenId = "")
	{
				
		$out = $this->M_dokumen->getById($dokumenId);
		if(isset($out["code"]) && $out["code"] == 200) {
			$this->load->helper("download");
			$data = $out["data"];
			force_download($data["NAMAFILE"],file_get_contents($data["file"]));			
		} else {
			show_404();
		}
	}

	
}	