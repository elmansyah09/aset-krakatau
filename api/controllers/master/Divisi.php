<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Divisi extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_divisi");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search","")			
		);
				
        $out = array();
        
		$out = $this->M_divisi->get($params);		

		echo json_encode($out);
	}	

	public function get_master()
	{
		
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"limit" => ifunsetempty($_GET,"limit",10),
			"offset" => ifunsetempty($_GET,"m",0),	
		);			        
        
		$out = $this->M_divisi->get_master($params);		

		if(isset($out["code"]) && $out["code"] == 200){
			$data = $out["data"];			
			$dt = array(				
				"data" => $data["data"],				
				"count" => $data["count"],	
				"from" => $data["from"],	
				"end" => $data["end"],				
			);
			
			$dt['paging'] = $this->set_pagination($dt, site_url('/master/divisi/get_master'));
			$out["data"] = $dt;
			echo json_encode($out);
		} else {
			echo json_encode($out);
		}
	}

	public function get_detail()
	{
		
		$params = array(
			"id_divisi" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_divisi->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{								
		$params = array(			
			
			"id_divisi" => ifunsetempty($_POST,"id_divisi",""),			
			"kode_divisi" => ifunsetempty($_POST,"kode_divisi",""),
			"nama_divisi" => ifunsetempty($_POST,"nama_divisi",""),
			"id_parent" => ifunsetempty($_POST,"id_parent",null),
			"user" => $this->user
		);

		$validation = $this->validationForm($params);

		if($validation["code"] !== 200) {
			echo json_encode($validation);
			exit;
		}

		$out = $this->M_divisi->simpan($params);

		echo json_encode($out);

	}

	private function validationForm($args = array())
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('kode_divisi', 'NIK', 'required');
		$this->form_validation->set_rules('nama_divisi', 'Nama', 'required');		
				
		if(isset($args["kode_divisi"])){
			$this->load->database();
			if(isset($args["id_divisi"]) && !empty($args["id_divisi"])){
				$this->db->where("id_divisi <>",$args["id_divisi"]);
			}
			$this->db->where("kode_divisi",$args["kode_divisi"]);
			$this->db->where("status","aktif");
			$cekKode = $this->db->get("divisi");			
			if($cekKode->num_rows() > 0){
				return response(245,"Validasi gagal",
					"Kode sudah terdaftar."
				,null);
			}
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			return response(245,"Validasi gagal",
				strip_tags(validation_errors())
			,null);
		}
		else
		{
			return response(200,"Valid",array(),array());
		}
	}

	function hapus()
	{
		$params = array(
			"id_divisi" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_divisi->hapus($params);

		echo json_encode($out);
	}
	
}
