<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_karyawan");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search","")			
		);
				
        $out = array();
        
		$out = $this->M_karyawan->get($params);		

		echo json_encode($out);
	}	

	public function get_master()
	{
		
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"limit" => ifunsetempty($_GET,"limit",10),
			"offset" => ifunsetempty($_GET,"m",0),	
		);			        
        
		$out = $this->M_karyawan->get_master($params);		

		if(isset($out["code"]) && $out["code"] == 200){
			$data = $out["data"];			
			$dt = array(				
				"data" => $data["data"],				
				"count" => $data["count"],	
				"from" => $data["from"],	
				"end" => $data["end"],				
			);
			
			$dt['paging'] = $this->set_pagination($dt, site_url('/master/karyawan/get_master'));
			$out["data"] = $dt;
			echo json_encode($out);
		} else {
			echo json_encode($out);
		}
	}

	public function get_detail()
	{
		
		$params = array(
			"id_karyawan" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_karyawan->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{								
		$params = array(			
			
			"id_karyawan" => ifunsetempty($_POST,"id_karyawan",""),			
			"nik_karyawan" => ifunsetempty($_POST,"nik_karyawan",""),
			"nama_karyawan" => ifunsetempty($_POST,"nama_karyawan",""),		
			"id_divisi" => ifunsetempty($_POST,"id_divisi",""),		
			"user" => $this->user
		);

		$validation = $this->validationForm($params);

		if($validation["code"] !== 200) {
			echo json_encode($validation);
			exit;
		}

		$out = $this->M_karyawan->simpan($params);

		echo json_encode($out);

	}

	private function validationForm($args = array())
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nik_karyawan', 'NIK', 'required');
		$this->form_validation->set_rules('nama_karyawan', 'Nama', 'required');
		$this->form_validation->set_rules('id_divisi', 'Divisi', 'required');
				
		if(isset($args["nik_karyawan"])){
			$this->load->database();
			if(isset($args["id_karyawan"]) && !empty($args["id_karyawan"])){
				$this->db->where("id_karyawan <>",$args["id_karyawan"]);
			}
			$this->db->where("nik_karyawan",$args["nik_karyawan"]);
			$this->db->where("status","aktif");
			$cekNIK = $this->db->get("karyawan");			
			if($cekNIK->num_rows() > 0){
				return response(245,"Validasi gagal",
					"NIK sudah terdaftar."
				,null);
			}
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			return response(245,"Validasi gagal",
				strip_tags(validation_errors())
			,null);
		}
		else
		{
			return response(200,"Valid",array(),array());
		}
	}

	function hapus()
	{
		$params = array(
			"id_karyawan" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_karyawan->hapus($params);

		echo json_encode($out);
	}	
	
}
