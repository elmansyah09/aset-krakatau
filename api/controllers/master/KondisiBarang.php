<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class KondisiBarang extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_kondisi_barang");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search","")			
		);
				
        $out = array();
        
		$out = $this->M_kondisi_barang->get($params);		

		echo json_encode($out);
	}	

	public function get_master()
	{
		
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"limit" => ifunsetempty($_GET,"limit",10),
			"offset" => ifunsetempty($_GET,"m",0),	
		);			        
        
		$out = $this->M_kondisi_barang->get_master($params);		

		if(isset($out["code"]) && $out["code"] == 200){
			$data = $out["data"];			
			$dt = array(				
				"data" => $data["data"],				
				"count" => $data["count"],	
				"from" => $data["from"],	
				"end" => $data["end"],				
			);
			
			$dt['paging'] = $this->set_pagination($dt, site_url('/master/kondisibarang/get_master'));
			$out["data"] = $dt;
			echo json_encode($out);
		} else {
			echo json_encode($out);
		}
	}

	public function get_detail()
	{
		
		$params = array(
			"id_kondisi_barang" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_kondisi_barang->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{								
		$params = array(			
			
			"id_kondisi_barang" => ifunsetempty($_POST,"id_kondisi_barang",""),			
			"nama_kondisi_barang" => ifunsetempty($_POST,"nama_kondisi_barang",""),				
			"user" => $this->user
		);

		$out = $this->M_kondisi_barang->simpan($params);

		echo json_encode($out);

	}

	function hapus()
	{
		$params = array(
			"id_kondisi_barang" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_kondisi_barang->hapus($params);

		echo json_encode($out);
	}	
	
}
