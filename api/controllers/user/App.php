<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_user");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"level" => ifunsetempty($_GET,"f_level","")			
		);
				
        $out = array();
        
		$out = $this->M_user->get($params);		

		echo json_encode($out);
	}

	public function get_detail()
	{
		
		$params = array(
			"id_admin" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_user->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{						
		$params = array(			
			"id_admin" => ifunsetempty($_POST,"id_admin",""),
			"username" => ifunsetempty($_POST,"username",""),
			"password" => ifunsetempty($_POST,"password",""),
			"re_password" => ifunsetempty($_POST,"re_password",""),
			"id_karyawan" => ifunsetempty($_POST,"id_karyawan",""),
			"level" => ifunsetempty($_POST,"level",""),
			"is_ganti_password" => ifunsetempty($_POST,"is_ganti_password",""),
			"user" => $this->user
		);


		$validation = $this->validationForm($params);

		if($validation["code"] !== 200) {
			echo json_encode($validation);
			exit;
		}

	
		$out = $this->M_user->simpan($params);

		echo json_encode($out);

	}
	
	function hapus()
	{
		$params = array(
			"id_admin" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_user->hapus($params);

		echo json_encode($out);
	}	

	private function validationForm($args = array())
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		if(!(isset($args["id_admin"]) && !empty($args["id_admin"]))){
			$this->form_validation->set_rules('password', 'Password', 'required');		
			$this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');
		}
		
		if(isset($args["username"])){
			$this->load->database();
			if(isset($args["id_admin"]) && !empty($args["id_admin"])){
				$this->db->where("id_admin <>",$args["id_admin"]);
			}
			$this->db->where("username",$args["username"]);
			$this->db->where("status","aktif");
			$cekUsername = $this->db->get("admin");			
			if($cekUsername->num_rows() > 0){
				return response(245,"Validasi gagal",
					"Username sudah terdaftar."
				,null);
			}
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			return response(245,"Validasi gagal",
				strip_tags(validation_errors())
			,null);
		}
		else
		{
			return response(200,"Valid",array(),array());
		}
	}


	public function ubah_password()
	{								
		$params = array(			
			"id_admin" => ifunsetempty($this->user,"id_admin",""),			
			"username" => ifunsetempty($this->user,"username",""),		
			"old_password" => ifunsetempty($_POST,"old_password",""),
			"password" => ifunsetempty($_POST,"password",""),
			"confirm_password" => ifunsetempty($_POST,"confirm_password",""),			
			"user" => $this->user
		);


		$validation = $this->validationUbahPassword($params);

		if($validation["code"] !== 200) {
			echo json_encode($validation);
			exit;
		}

	
		$out = $this->M_user->ubah_password($params);

		echo json_encode($out);

	}

	private function validationUbahPassword($args = array())
	{
		$this->load->library('form_validation');		
		if(empty($args["id_admin"])) {
        	show_auth_failed();
		}
		
		$this->form_validation->set_rules('old_password', 'Password Lama', 'required');
		$this->form_validation->set_rules('password', 'Password Baru', 'required');
		$this->form_validation->set_rules('confirm_password', 'Konfirmasi Password', 'required|matches[password]');
		if(isset($args["id_admin"])){
			$this->load->database();
			$this->db->where("id_admin",$args["id_admin"]);			
			$this->db->where("password", md5(md5(ifunsetempty($args,"old_password","").ifunsetempty($args,"username",""))));
			$this->db->where("status","aktif");
			$cekUsername = $this->db->get("admin");			
			if($cekUsername->num_rows() == 0){
				return response(245,"Validasi gagal",
					"Password lama tidak sesuai"
				,null);
			}
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			return response(245,"Validasi gagal",
				strip_tags(validation_errors())
			,null);
		}
		else
		{
			return response(200,"Valid",array(),array());
		}
	}

	
	
}
