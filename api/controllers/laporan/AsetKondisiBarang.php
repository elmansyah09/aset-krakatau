<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AsetKondisiBarang extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			// "auth_method" => ":ALL"			
		);
		parent::__construct($config);		
		$this->load->model("M_laporan");
	}

	public function cetak()
	{
		$this->load->library("EFR_excel");
		$spreadsheet = $this->efr_excel->createSheet();		
		$this->load->model("M_kondisi_barang");
		$resKondisiBarang = $this->M_kondisi_barang->get();
		if(!(isset($resKondisiBarang["code"]) && $resKondisiBarang["code"] == 200)){
			show_404();
			exit;
		}
		
		foreach ($resKondisiBarang["data"] as $i => $row) {			
			$spreadsheet->createSheet();
			// Zero based, so set the second tab as active sheet
			$spreadsheet->setActiveSheetIndex($i);
			$spreadsheet->getActiveSheet()->setTitle($row["nama_kondisi_barang"]);				
			$sheet = $spreadsheet->getActiveSheet();
			$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
			$drawing->setPath('media/public/images/logo.png'); // put your path and image here
			$drawing->setCoordinates('A2');
			$drawing->getShadow()->setVisible(true);		
			$drawing->setHeight(72);
			$drawing->setWorksheet($sheet);
			
			$sheet->setCellValue('A2', 'Daftar Aset');
			$sheet->setCellValue('A3', $row["nama_kondisi_barang"]);
			$sheet->getRowDimension("2")->setRowHeight(25);
			$sheet->getRowDimension("3")->setRowHeight(25);
			$sheet->getStyle('A2:A3')->getAlignment()->setWrapText(true);			
			$styleArrayHeader = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000'),				
					"size" => 22
				),
				'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
				),
			);
			$sheet->getStyle("A2:A3")->applyFromArray($styleArrayHeader);
			$sheet->mergeCells('A2:H2');
			$sheet->mergeCells('A3:H3');
			$rowStart = 5;
			$sheet->setCellValue('A'.$rowStart, 'NO');
			$sheet->setCellValue('B'.$rowStart, 'NO. ASET');
			$sheet->setCellValue('C'.$rowStart, 'NAMA BARANG');			
			$sheet->setCellValue('D'.$rowStart, 'JENIS BARANG');
			$sheet->setCellValue('E'.$rowStart, 'TANGGAL PEROLEHAN');
			$sheet->setCellValue('F'.$rowStart, 'HARGA BELI');
			$sheet->setCellValue('G'.$rowStart, 'NILAI BUKU');
			$sheet->setCellValue('H'.$rowStart, 'KETERANGAN');					
			$sheet->getStyle("A$rowStart:H$rowStart")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF333399');
			$sheet->getColumnDimension('A')->setWidth(10);
			$sheet->getColumnDimension('B')->setWidth(30);
			$sheet->getColumnDimension('C')->setWidth(32);
			$sheet->getColumnDimension('D')->setAutoSize(true);
			$sheet->getColumnDimension('E')->setAutoSize(true);			
			$sheet->getColumnDimension('F')->setWidth(30);
			$sheet->getColumnDimension('G')->setWidth(30);					
			$sheet->getColumnDimension('H')->setWidth(50);		
			$sheet->getRowDimension("$rowStart")->setRowHeight(45);
			$styleArray = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => 'FFFFFF'),				
				),
				'alignment' => array(
					'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
				),
				  'borders' => array(
					'allBorders' => array(
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						 'color' => array('rgb' => '000000'),
					),
				),
				);
			$sheet->getStyle("A$rowStart:I$rowStart")->applyFromArray($styleArray);
			$rowStartBody = $rowStart;
			
			$params = array(
				"kondisi_barang" => $row["id_kondisi_barang"]
			);
			$data = $this->M_laporan->get_kondisi_barang($params);
			
			if(isset($data["code"]) && $data["code"] == 200){					
				$rowStartBody++;
				$summary = [
					"harga_barang" => 0,
					"nilai_buku" => 0,				
				];
				foreach ($data["data"] as $key => $value) {
					$rowStart++;				
					$sheet->setCellValue('A'.$rowStart, $value["no"]);
					$sheet->setCellValue('B'.$rowStart, $value["nomor_seri"]);
					$sheet->setCellValue('C'.$rowStart, $value["nama_barang"]);					
					$sheet->setCellValue('D'.$rowStart, $value["nama_jenis_barang"]);
					$sheet->setCellValue('E'.$rowStart, $value["tgl_pembelian"]);
					$sheet->setCellValue('F'.$rowStart, $value["harga_barang"]);								
					$sheet->setCellValue('G'.$rowStart, $value["nilai_buku"]);
					$sheet->setCellValue('H'.$rowStart, $value["keterangan"]);			
	
					$summary["harga_barang"] += $value["harga_barang"];
					$summary["nilai_buku"] += $value["nilai_buku"];				
				}
	
				$rowStart++;
				$sheet->mergeCells("A$rowStart:E$rowStart");				
				$sheet->setCellValue('A'.$rowStart, "TOTAL");			
				$sheet->setCellValue('F'.$rowStart, $summary["harga_barang"]);
				$sheet->setCellValue('G'.$rowStart, $summary["nilai_buku"]);			
	
				$styleArrayFooter = array(
					'font'  => array(
						'bold'  => true,
						'color' => array('rgb' => '000000'),				
					)				
				);
				$sheet->getStyle("A$rowStart")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle("A$rowStart:H$rowStart")->applyFromArray($styleArrayFooter);
	
				$styleArrayBody = array(				
					'borders' => array(
						'allBorders' => array(
							'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
							'color' => array('rgb' => '000000'),
						),
					),
				);			
				$sheet->getStyle("F$rowStartBody:G$rowStart")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_ACCOUNTING_IDR);
				$sheet->getStyle("E$rowStartBody:E$rowStart")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);				
				$sheet->getStyle("A$rowStartBody:H$rowStart")->applyFromArray($styleArrayBody);
			} else {
				show_404();
			}
		}

		$this->efr_excel->download("Aset per Jenis Barang-".time(),$spreadsheet);		

	}	
	
}
