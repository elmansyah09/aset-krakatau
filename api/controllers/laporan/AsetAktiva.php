<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AsetAktiva extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			// "auth_method" => ":ALL"			
		);
		parent::__construct($config);		
		$this->load->model("M_laporan");
	}

	public function get()
	{
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"jenis_barang" => ifunsetempty($_GET,"f_jenis_barang",""),
			"tipe_barang" => ifunsetempty($_GET,"f_tipe_barang",""),
			"kondisi_barang" => ifunsetempty($_GET,"f_kondisi_barang",""),
			"limit" => ifunsetempty($_GET,"limit",10),
			"offset" => ifunsetempty($_GET,"m",0),
		);
				
        $out = array();
        
		$out = $this->M_laporan->get_aktiva($params,true);		

		if(isset($out["code"]) && $out["code"] == 200){
			$data = $out["data"];
			
			$dt = array(				
				"data" => $data["data"],				
				"count" => $data["count"],	
				"from" => $data["from"],	
				"end" => $data["end"],				
			);
			
			$dt['paging'] = $this->set_pagination($dt, site_url('/laporan/asetaktiva/get'));
			$out["data"] = $dt;
			echo json_encode($out);
		} else {
			echo json_encode($out);
		}
	}

	public function cetak()
	{
		$this->load->library("EFR_excel");
		$spreadsheet = $this->efr_excel->createSheet();				
		$sheet = $spreadsheet->getActiveSheet();
		$drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		$drawing->setPath('media/public/images/logo.png'); // put your path and image here
		$drawing->setCoordinates('A2');
		$drawing->getShadow()->setVisible(true);		
		$drawing->setHeight(72);
		$drawing->setWorksheet($sheet);
		
		$sheet->setCellValue('A2', 'Daftar Aset Aktiva Tetap');
		$sheet->getStyle('A2')->getAlignment()->setWrapText(true);
		$sheet->getStyle('A2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$styleArrayHeader = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000'),				
				"size" => 22
			),
		);
		$sheet->getStyle("A2")->applyFromArray($styleArrayHeader);
		$sheet->mergeCells('A2:M2');
		$rowStart = 5;
		$sheet->setCellValue('A'.$rowStart, 'NO');
		$sheet->setCellValue('B'.$rowStart, 'NO. ASET');
		$sheet->setCellValue('C'.$rowStart, 'NAMA BARANG');
		$sheet->setCellValue('D'.$rowStart, 'JENIS BARANG');
		$sheet->setCellValue('E'.$rowStart, 'TANGGAL PEROLEHAN');
		$sheet->setCellValue('F'.$rowStart, 'HARGA BELI');
		$sheet->setCellValue('G'.$rowStart, 'JANGKA WAKTU PENYUSUTAN');
		$sheet->setCellValue('H'.$rowStart, 'PENYUSUTAN PER BULAN');
		$sheet->setCellValue('I'.$rowStart, 'NILAI BUKU');
		$sheet->setCellValue('J'.$rowStart, 'KONDISI');
		$sheet->setCellValue('K'.$rowStart, 'KETERANGAN');
		$sheet->setCellValue('L'.$rowStart, 'LOKASI ASET');
		$sheet->setCellValue('M'.$rowStart, 'PENANGGUNG JAWAB');
		$sheet->getRowDimension('2')->setRowHeight(56);
		$sheet->getStyle("A$rowStart:M$rowStart")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FF333399');
		$sheet->getColumnDimension('A')->setWidth(10);
		$sheet->getColumnDimension('B')->setWidth(30);
		$sheet->getColumnDimension('C')->setWidth(32);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setWidth(30);
		$sheet->getColumnDimension('G')->setWidth(20);		
		$sheet->getStyle('G'.$rowStart)->getAlignment()->setWrapText(true);
		$sheet->getColumnDimension('H')->setWidth(30);
		$sheet->getColumnDimension('I')->setWidth(30);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
		$sheet->getColumnDimension('M')->setWidth(50);
		$sheet->getRowDimension("$rowStart")->setRowHeight(45);
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFFFFF'),				
			),
			'alignment' => array(
				'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
			),
			  'borders' => array(
				'allBorders' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					 'color' => array('rgb' => '000000'),
				),
			),
			);
		$sheet->getStyle("A$rowStart:M$rowStart")->applyFromArray($styleArray);
		$rowStartBody = $rowStart;
		$data = $this->M_laporan->get_aktiva();		
		if(isset($data["code"]) && $data["code"] == 200){					
			$rowStartBody++;
			$summary = [
				"harga_barang" => 0,
				"nilai_buku" => 0,
				"penyusutan_perbulan" => 0,
			];
			foreach ($data["data"]["data"] as $key => $value) {
				$rowStart++;				
				$sheet->setCellValue('A'.$rowStart, $value["no"]);
				$sheet->setCellValue('B'.$rowStart, $value["nomor_seri"]);
				$sheet->setCellValue('C'.$rowStart, $value["nama_barang"]);
				$sheet->setCellValue('D'.$rowStart, $value["nama_jenis_barang"]);
				$sheet->setCellValue('E'.$rowStart, $value["tgl_pembelian"]);
				$sheet->setCellValue('F'.$rowStart, $value["harga_barang"]);
				$sheet->setCellValue('G'.$rowStart, $value["jangka_waktu"]);
				$sheet->setCellValue('H'.$rowStart, ($value["harga_barang"]/$value["jangka_waktu"]));
				$sheet->setCellValue('I'.$rowStart, $value["nilai_buku"]);
				$sheet->setCellValue('J'.$rowStart, $value["nama_kondisi_barang"]);
				$sheet->setCellValue('K'.$rowStart, $value["keterangan"]);
				$sheet->setCellValue('L'.$rowStart, $value["nama_divisi"]);
				$sheet->setCellValue('M'.$rowStart, $value["nama_penanggung_jawab"]);			

				$summary["harga_barang"] += $value["harga_barang"];
				$summary["nilai_buku"] += $value["nilai_buku"];
				$summary["penyusutan_perbulan"] += ($value["harga_barang"]/$value["jangka_waktu"]);
			}

			$rowStart++;
			$sheet->mergeCells("A$rowStart:E$rowStart");
			$sheet->mergeCells("J$rowStart:M$rowStart");
			$sheet->setCellValue('A'.$rowStart, "TOTAL");			
			$sheet->setCellValue('F'.$rowStart, $summary["harga_barang"]);
			$sheet->setCellValue('H'.$rowStart, $summary["penyusutan_perbulan"]);
			$sheet->setCellValue('I'.$rowStart, $summary["nilai_buku"]);

			$styleArrayFooter = array(
				'font'  => array(
					'bold'  => true,
					'color' => array('rgb' => '000000'),				
				)				
			);
			

			$styleArrayBody = array(
				'font'  => array(
					'bold'  => false,
				),				
				'borders' => array(
					'allBorders' => array(
						'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						'color' => array('rgb' => '000000'),
					),
				),
			);
			$sheet->getStyle("F$rowStartBody:F$rowStart")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_ACCOUNTING_IDR);
			$sheet->getStyle("H$rowStartBody:I$rowStart")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_ACCOUNTING_IDR);
			$sheet->getStyle("E$rowStartBody:E$rowStart")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("G$rowStartBody:G$rowStart")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A$rowStartBody:M$rowStart")->applyFromArray($styleArrayBody);

			$sheet->getStyle("A$rowStart")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle("A$rowStart:M$rowStart")->applyFromArray($styleArrayFooter);

			$this->efr_excel->download("Aset Tetap-".time(),$spreadsheet);
		} else {
			show_404();
		}
		

	}	
	
}
