<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AsetMasuk extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_aset_masuk");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"jenis_barang" => ifunsetempty($_GET,"f_jenis_barang",""),
			"tipe_barang" => ifunsetempty($_GET,"f_tipe_barang",""),
			"limit" => ifunsetempty($_GET,"limit",10),
			"offset" => ifunsetempty($_GET,"m",0),
		);
				
        $out = array();
        
		$out = $this->M_aset_masuk->get($params,true);		

		if(isset($out["code"]) && $out["code"] == 200){
			$data = $out["data"];
			
			$dt = array(				
				"data" => $data["data"],				
				"count" => $data["count"],	
				"from" => $data["from"],	
				"end" => $data["end"],				
			);
			
			$dt['paging'] = $this->set_pagination($dt, site_url('/aset/asetmasuk/get'));
			$out["data"] = $dt;
			echo json_encode($out);
		} else {
			echo json_encode($out);
		}
	}

	public function get_detail()
	{
		
		$params = array(
			"id_aset" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_aset_masuk->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{						
		$params = array(			
			"id_aset_masuk" => ifunsetempty($_POST,"id_aset_masuk",""),
			"id_aset" => ifunsetempty($_POST,"id_aset",""),
			// "nomor_seri" => ifunsetempty($_POST,"nomor_seri",""),
			"nama_barang" => ifunsetempty($_POST,"nama_barang",""),
			"id_jenis_barang" => ifunsetempty($_POST,"id_jenis_barang",""),
			"id_tipe_barang" => ifunsetempty($_POST,"id_tipe_barang",""),
			"tanggal_pembelian" => ifunsetempty($_POST,"tanggal_pembelian",""),
			"harga_barang" => ifunsetempty($_POST,"harga_barang",""),
			"nilai_buku" => ifunsetempty($_POST,"nilai_buku",""),
			"jumlah" => ifunsetempty($_POST,"jumlah",""),
			"jangka_waktu" => ifunsetempty($_POST,"jangka_waktu",""),
			"keterangan" => ifunsetempty($_POST,"keterangan",""),
			"id_kondisi_barang" => ifunsetempty($_POST,"id_kondisi_barang",""),
			"id_penanggung_jawab" => ifunsetempty($_POST,"id_penanggung_jawab",""),
			"user" => $this->user
		);

		$out = $this->M_aset_masuk->simpan($params);

		echo json_encode($out);

	}

	function hapus()
	{
		$params = array(
			"id_aset" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_aset_masuk->hapus($params);

		echo json_encode($out);
	}	

	
	
}
