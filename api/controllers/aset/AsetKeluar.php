<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class AsetKeluar extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_aset_keluar");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"jenis_barang" => ifunsetempty($_GET,"f_jenis_barang",""),
			"tipe_barang" => ifunsetempty($_GET,"f_tipe_barang",""),
			"kondisi_barang" => ifunsetempty($_GET,"f_kondisi_barang",""),
		);
				
        $out = array();
        
		$out = $this->M_aset_keluar->get($params);		

		echo json_encode($out);
	}

	public function get_detail()
	{
		
		$params = array(
			"id_aset_keluar" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_aset_keluar->get_detail($params);		

		echo json_encode($out);
	}

	public function simpan()
	{						
		$params = array(			
			"id_aset_keluar" => ifunsetempty($_POST,"id_aset_keluar",""),
			"id_aset" => ifunsetempty($_POST,"id_aset",""),
			"id_kondisi_barang" => ifunsetempty($_POST,"id_kondisi_barang",""),
			"user" => $this->user
		);

		$out = $this->M_aset_keluar->simpan($params);

		echo json_encode($out);

	}

	function hapus()
	{
		$params = array(
			"id_aset_keluar" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_aset_keluar->hapus($params);

		echo json_encode($out);
	}	

	
	
}
