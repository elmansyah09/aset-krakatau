<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);
		$this->load->model("M_aset");
	}

	public function get()
	{
		
		$params = array(
			"search" => ifunsetempty($_GET,"f_search",""),
			"jenis_barang" => ifunsetempty($_GET,"f_jenis_barang",""),
			"tipe_barang" => ifunsetempty($_GET,"f_tipe_barang",""),
		);
				
        $out = array();
        
		$out = $this->M_aset->get($params);		

		echo json_encode($out);
	}

	public function get_detail()
	{
		
		$params = array(
			"id_aset" => base64_decode(ifunsetempty($_POST,"uuid","")),
		);
				
        $out = array();
        
		$out = $this->M_aset->get_detail($params);		

		echo json_encode($out);
	}
	
	function hapus()
	{
		$params = array(
			"id_aset" => ifunsetempty($_POST,"uuid",""),			
		);
		
		$out = $this->M_aset->hapus($params);

		echo json_encode($out);
	}	

	
	
}
