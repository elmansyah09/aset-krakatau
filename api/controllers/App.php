	
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends EFR_Controller {

	public function __construct()
	{
		$header = getallheaders();			
		$iswb = isset($header["iswb"])? $header["iswb"] : false;
		$config = array(
			"allow_origin" => true,
			"auth_method" => ":ALL",
			"iswb" => $iswb
		);
		parent::__construct($config);
		$this->load->model("M_ensiklopedia");
	}


	function uploadMedia()
	{
        $params = array(

        );
        
        $config['upload_path']          = $this->config->item("assets_path")."images/";
		$config['allowed_types']        = '*';
		$config['max_size']             = '5120';

        $this->load->library('upload', $config);
        
        if (isset($_FILES['upload']['size']) && $_FILES['upload']['size'] != 0){
	        if ( ! $this->upload->do_upload('upload'))
	        {
	                $error = array('success'=>false, 'msg' => $this->upload->display_errors());
	                echo json_encode($error);
	                exit();
	        }
	        else
	        {
                    $dokumen = $this->upload->data();                    
	                $params['file'] = $dokumen['file_name'];
	        }
        }

        if (count($params) > 0) {
             $function_number = $_GET['CKEditorFuncNum'];
            $url = $this->config->item("url_asset")."images/" . $params['file'];
            $message = '';
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($function_number, '$url', '$message');</script>";
        } else {
			echo "Failed upload";
			// $out = array(
			// 	"success" => false,
			// 	"items" => array(),
			// 	"msg" => "Upload media gagal",
			// );
		}
        
        // echo json_encode($out);
	}

	
}	