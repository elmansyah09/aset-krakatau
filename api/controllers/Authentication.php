<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			"allow_origin" => true,
			"auth_method" => ":ALL",
			"except_auth_method" => array("login","initToken","generate_password","check","reset_password")
		);
		parent::__construct($config);
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function login()
	{
		$params = $this->_get_params();
		$this->load->model("M_authentication");
		
		$params_login = array(
			"username" => ifunsetempty($params,"username",""),
			"password" => md5(md5(ifunsetempty($params,"password","").ifunsetempty($params,"username","")))
		);		
				
		$res_login = $this->M_authentication->check_login($params_login["username"],$params_login["password"]);

		if ($res_login->num_rows() == 1) {
			$data_user = $res_login->row_array();
			unset($data_user["password"]);
			$data_token = array(
				"user" => $data_user,
			);
			$token = $this->generate_token($data_token);
			$out = array(
				"success" => true,
				"data_user" => $data_user,
				"token" => $token,				
				"msg" => "Login Berhasil"
			);
		} else {
			$out = array(
				"success" => false,
				"msg" => "Login Gagal Periksa Kembali Username dan Password"
			);
		}

		echo json_encode($out);
	}


	public function initToken()
	{
		$token = ifunset($_POST,"token","");

		$res = false;
		if(!empty($token)) {
			$res=$this->session->set_userdata(array("token"=>$token));
			$res = true;
		}

		if($res) {
			$out = array(
				"success" => true,
				"msg" => "Success init token"
			);
		} else {
			$out = array(
				"success" => false,
				"msg" => "Failed init token"
			);
		}

		echo json_encode($out);
	}

	public function change_password()
	{
		$this->load->model("M_user");
		$params = array(
			"us.userid" => ifunset($_POST,"userid","")
		);
		$out = array(
				"success" => false,
				"msg" => "Failed process"
			);
		
		$res_user = $this->M_user->get($params);

		if($res_user->num_rows() > 0) {
			$dt_user = $res_user->row_array();
			$password = md5(md5(ifunsetempty($_POST,"password","")).$this->config->item('encryptCode').ifunsetempty($dt_user,"username",""));
			
			if(trim($password) != trim($dt_user["password"])) {
				$out = array(
					"success" => false,
					"msg" => "Kata sandi lama salah"
				);		
				echo json_encode($out);
				exit;
			} else {
				if(ifunset($_POST,"new_password",'&') != ifunset($_POST,"conf_new_password",'&1')) {
					$out = array(
						"success" => false,
						"msg" => "Konfirmasi Kata sandi baru salah"
					);		
					echo json_encode($out);
					exit;
				} else {
					$params_change = array(
						"userid" => ifunset($_POST,"userid",""),
						"password" => md5(md5(ifunset($_POST,"new_password",'')).$this->config->item('encryptCode').ifunsetempty($dt_user,"username",""))						
					);
					$res = $this->M_user->upd($params_change);
					if($res) {
						$out = array(
							"success" => true,
							"msg" => "Success process"
						);
					}
				}
			}


		} 		

		echo json_encode($out);
	}


	function _get_data_sync($userid)
	{
		$params = array(
			"userid" => $userid			
		);
		$res = $this->M_authentication->get_sync_data($params);

		$data = array();

		if($res) {

			foreach ($res->result_array() as $key => $value) {
				$data[$value["kategori"]] = (int) $value["value"];
			}	
		}

		return $data;
	}

	function data_sync($value='')
	{
		$method = $this->_get_params();
		$this->load->model("M_authentication");
		$params = array(
			"userid" => ifunsetempty($method,"userid",""),
			"kategori" => ifunsetempty($method,"kategori",""),
		);
		$time = time();
		$res = false;
		if(!empty($params["userid"]) && !empty($params["kategori"])) {
			$res_get = $this->M_authentication->get_sync_data($params);
			if($res_get) {
				if($res_get->num_rows() > 0) {
					$last_data = $res_get->row_array();
					$params["id"] = $last_data["id"];					
				}
			}
			$params["value"] = time();
			if(isset($params["id"]) && !empty($params["id"])) {
				$params["date_modified"] = date("Y-m-d H:i:s");
				$res = $this->M_authentication->upd_sync_data($params);
			} else {
				$params["date_created"] = date("Y-m-d H:i:s");
				$res = $this->M_authentication->add_sync_data($params);
			}
		}

		if($res) {
			$out = array(
				"success" => true,
				"time" => $time,
				"msg" => "Success sync data"
			);
		} else {
			$out = array(
				"success" => false,
				"msg" => "Failed sync data"
			);
		}

		echo json_encode($out);

	}

	public function check()
	{
		$method = $this->_get_params();
		$this->load->model("M_authentication");
		$this->load->model("M_user");
		$params = array(			
			"username" => ifunsetempty($method,"username",""),
			"status" => 1
		);
		
		$res = $this->M_authentication->get_user($params);
		if($res && $res->num_rows() > 0){
				$out = array(
					"success" => true,					
					"msg" => "username terdaftar"
				);
		} else {
			$out = array(
				"success" => false,
				"msg" => "username belum terdaftar"
			);
		}

		echo json_encode($out);
	}

	public function reset_password()
	{
		$method = $this->_get_params();
		$this->load->model("M_authentication");
		$this->load->model("M_user");
		$params = array(			
			"username" => ifunsetempty($method,"username",""),
			"status" => 1
		);

		$new_pass = ifunsetempty($method,"password","");
		
		$out = array(
			"success" => false,
			"msg" => "Proses Gagal"
		);

		$res = $this->M_authentication->get_user($params);

		if($res && $res->num_rows() > 0){
			$row = $res->row_array();
			if(!empty($new_pass)) {
				$params_change = array(
					"userid" => ifunset($row,"userid",""),
					"password" => md5(md5($new_pass).$this->config->item('encryptCode').ifunsetempty($params,"username",""))						
				);
	
				$res_pass = $this->M_user->upd($params_change);
	
				if($res_pass) {
					$out = array(
						"success" => true,
						"newp" => $new_pass,
						"msg" => "Proses Berhasil"
					);
				}
			} else {
				$out["msg"] = "Password tidak kosong";
			}
		} else {
			$out = array(
				"success" => false,
				"msg" => "username belum terdaftar"
			);
		}

		echo json_encode($out);

	}

	function _rand($n = 7) { 
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
		$randomString = ''; 
	
		for ($i = 0; $i < $n; $i++) { 
			$index = rand(0, strlen($characters) - 1); 
			$randomString .= $characters[$index]; 
		} 
	
		return $randomString; 
	} 

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}
	
}
