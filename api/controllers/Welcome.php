<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			"allow_origin" => true,
			"auth_method" => ":ALL",
			"except_auth_method" => array("gtoken","drive")
		);
		parent::__construct($config);
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function getDataUser()
	{
		$data = array(
			"UserName" => "elmansyah",
			"UserGroup" => "admin"
		);

		echo json_encode($data,true);		
	}

	public function gtoken()
	{
		echo $this->generate_token(array("Elmansyah"));
	}

	public function drive()
	{
		$this->load->library("Gdrive");

		$res = $this->gdrive->createFolder("Test2");

		var_dump($res);

	}

	
}
