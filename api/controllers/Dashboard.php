<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends EFR_Controller {

	public function __construct()
	{
		$config = array(
			// "allow_origin" => true,
			// "auth_method" => ":ALL",
			// "except_auth_method" => array("gtoken","drive")
		);
		parent::__construct($config);
		$this->load->model("M_dashboard");

	}

	public function aset_keseluruhan()
	{
		$params = array(
			
		);
				
        $out = array();
        
		$out = $this->M_dashboard->get_keseluruhan($params);		

		echo json_encode($out);
	}

	public function aset_bulanan()
	{
		$params = array(
			
		);
				
        $out = array();
        
		$out = $this->M_dashboard->get_bulanan($params);		

		echo json_encode($out);
	}

	public function aset_jenis()
	{
		$params = array(
			
		);
				
        $out = array();
        
		$out = $this->M_dashboard->get_aset_jenis($params);		

		echo json_encode($out);
	}

	public function aset_kondisi()
	{
		$params = array(
			
		);
				
        $out = array();
        
		$out = $this->M_dashboard->get_aset_kondisi($params);		

		echo json_encode($out);
	}

	
}
