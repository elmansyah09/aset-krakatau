<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
    
<body>
    <center>
        <table>
            <tr>
                <td align="center">
                    <img style="width:72px;text-align:center;" src="<?php echo $this->config->item("url_public_images"); ?>/logo_bpn.png" alt="">
                </td>
            </tr>
            <tr>
                <td align="center">
                    <div style="font-size:11px;"><b>KEMENTERIAN AGARIA DAN TATA RUANG/ BADAN PERTANAHAN NASIONAL</b></div>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>        
    </center>    
    <hr>
    <table cellspacing="0" cellpadding="-5">
        <tr>
            <td align="center">
                <div style="font-size:11px;"><b>DAFTAR RIWAYAT HIDUP</b></div>                                
            </td>
        </tr>
        <tr>
            <td align="center">
                <div style="font-size:11px;"><b>PEGAWAI PEMERINTAH NON PEGAWAI NEGERI</b></div>
            </td>
        </tr>
        <tr>            
            <td align="center" style="height:150px;">
                <br>
                <br>
                <br>
                <img style="width:96x;height:120x;" src="<?php echo (!empty($detail["FOTO_FILE"]))?$detail["FOTO_FILE"]:$this->config->item('url_upload')."foto/avatar.png"; ?>" alt="">
            </td>            
        </tr>
    </table>
    <br>
    <table style="width:100%;">
        <tr>
            <td style="width:5%;" align="center">
                <div style="font-size:12px;"><b>I.</b></div>
            </td>
            <td colspan="3">
                <div style="font-size:12px;"><b>IDENTITAS</b></div>
            </td>
        </tr>        
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Nomor Induk Kependudukan (NIK) </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["NIKBARU"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Nama </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["NAMA"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Tempat, Tanggal Lahir </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["TEMPATLAHIR"].", ".$detail["TANGGAL_LAHIR"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Alamat </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["ALAMAT"]; ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Agama </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["AGAMA"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Jenis Kelamin </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["JENISKELAMIN_TEXT"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Status Perkawinan  </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["STATUSKAWIN_TEXT"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Nomor Pokok Wajib Pajak (NPWP)  </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["NPWP"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Nomor Telepon  </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["TELEPON"] ?></td>
        </tr>
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Email </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["EMAIL"] ?></td>
        </tr> 
        <tr>
            <td ></td>
            <td style="width:40%;font-size:11px;">Unit Kerja </td>
            <td style="width:5%" align="center">:</td>
            <td style="width:45%;font-size:11px;"><?php echo $detail["SATKER"] ?></td>
        </tr> 
    </table>
    <br>
    <table style="width:100%;"  width="100%" >
        <tr>
            <td style="width:5%;" align="center">
                <div style="font-size:12px;"><b>II.</b></div>
            </td>
            <td colspan="6">
                <div style="font-size:12px;"><b>PENDIDIKAN</b></div>
            </td>
        </tr>        
        <tr style="border:1px solid black;">         
            <th></th>   
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:25px;line-height: 250%;">No</th>
            <th align="center" valign="middle" style="background-color:lightgray;font-size:11px;border:1px solid black;width:20%;line-height: 250%;">Nomor Ijazah</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:10%;">Tahun Lulus</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:23%;line-height: 250%;">Perguruan Tinggi</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:20%;line-height: 250%;">Jurusan/Prodi</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:15%;">Kabupaten/ Kota</th>
        </tr>   
        <?php 
            foreach ($riwayat_pendidikan as $key => $row) {
        ?>
            <tr>
                <td></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NO"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["NOIJAZAH"]?></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["TAHUNLULUS"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["NAMAINSTANSI"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["JURUSAN"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["KABUPATEN"]?></td>
            </tr>
        
        <?php
            }
        ?>     
    </table>    
    <br>
    <table style="width:100%;">
        <tr>
            <td style="width:5%;" align="center">
                <div style="font-size:12px;"><b>III.</b></div>
            </td>
            <td colspan="3">
                <div style="font-size:12px;"><b>KONTRAK</b></div>
            </td>
        </tr>    
        <tr style="border:1px solid black;">         
            <th></th>   
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:25px;line-height: 250%;">No</th>
            <th align="center" valign="middle" style="background-color:lightgray;font-size:11px;border:1px solid black;width:38%;line-height: 250%;">Nomor Kontrak</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:15%;">Tanggal Kontrak</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:35%;line-height: 250%;">Unit Kerja</th>            
        </tr>       
        <?php 
            foreach ($riwayat_kontrak as $key => $row) {
        ?>
            <tr>
                <td></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NO"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["NOKONTRAK"]?></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["TMTJABATANAWAL"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["SATKER"]?></td>                
            </tr>        
        <?php
            }
        ?>
    </table>
    <br>
    <table style="width:100%;">
        <tr>
            <td style="width:5%;" align="center">
                <div style="font-size:12px;"><b>IV.</b></div>
            </td>
            <td colspan="3">
                <div style="font-size:12px;"><b>JABATAN</b></div>
            </td>
        </tr>    
        <tr style="border:1px solid black;">         
            <th></th>   
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:25px;">No</th>
            <th align="center" valign="middle" style="background-color:lightgray;font-size:11px;border:1px solid black;width:28%;">Nomor SK</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:30%;">Nama Jabatan</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:30%;">Unit Kerja</th>            
        </tr>        
        <?php 
            foreach ($riwayat_jabatan as $key => $row) {
        ?>
            <tr>
                <td></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NO"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["NOSK"]?></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NAMAJABATAN"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["SATKER"]?></td>                
            </tr>        
        <?php
            }
        ?>
    </table>
     <br>
    <table style="width:100%;">
        <tr>
            <td style="width:5%;" align="center">
                <div style="font-size:12px;"><b>V.</b></div>
            </td>
            <td colspan="3">
                <div style="font-size:12px;"><b>PENILAIAN KINERJA</b></div>
            </td>
        </tr>        
        <tr style="border:1px solid black;">         
            <th></th>   
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:25px;">No</th>
            <th align="center" valign="middle" style="background-color:lightgray;font-size:11px;border:1px solid black;width:10%;">Tahun</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:30%;">Nama Jabatan</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:35%;">Unit Kerja</th>
            <th align="center" style="background-color:lightgray;font-size:11px;border:1px solid black;width:13%;">Nilai</th>            
        </tr>  
        <?php 
            foreach ($riwayat_kinerja as $key => $row) {
        ?>
            <tr>
                <td></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NO"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["TAHUNPENILAIAN"]?></td>
                <td align="center" style="font-size:11px;border:1px solid black;"><?php echo $row["NAMAJABATAN"]?></td>
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["SATKER"]?></td>                
                <td style="font-size:11px;border:1px solid black;"><?php echo $row["SKORPENILAIAN"]?></td>                
            </tr>        
        <?php
            }
        ?>
    </table>    
</body>
</html>