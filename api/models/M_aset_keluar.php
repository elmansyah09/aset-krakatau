<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_aset_keluar extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}

	public function get($args = array())
	{	
		try {			
			
			$this->db->select("
				aset_keluar.id_aset_keluar,
				aset_keluar.tanggal_input,
				aset.*,
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("kondisi_barang.nama_kondisi_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			if(isset($args["kondisi_barang"]) && !empty($args["kondisi_barang"])){
				$filters["kondisi_barang"] = $args["kondisi_barang"];			
				$this->db->where("kondisi_barang.id_kondisi_barang",$args["kondisi_barang"]);			
			}

			$this->db->from("aset_keluar");
			$this->db->join("aset","aset_keluar.id_aset = aset.id_aset","left");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
			$this->db->order_by("aset_keluar.tanggal_buat","ASC");
			
			$data = $this->db->get()->result_array();

			$hasil = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');
				$value["tgl_input"] = convert_tgl($value["tanggal_input"],'y-m-d');
				$hasil[] = $value;
			}
			return response(200,"Data Aset Keluar",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
			
			$this->db->select("
				aset_keluar.id_aset_keluar,
				aset_keluar.tanggal_input,
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang
			");
			$hasil = array();
			$data = array();
			if(isset($args["id_aset_keluar"]) && !empty($args["id_aset_keluar"])){
				
				$this->db->where("aset_keluar.id_aset_keluar",$args["id_aset_keluar"]);			
				$this->db->from("aset_keluar");
				$this->db->join("aset","aset_keluar.id_aset = aset.id_aset","left");
				$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = tipe_barang.id_tipe_barang","left");
				$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
				$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
				$this->db->where("aset.status","aktif");
				$this->db->order_by("aset.tanggal_buat","ASC");
				$this->db->group_by("aset.id_aset");
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Aset Keluar",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function simpan($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsData = array(
				"id_aset_keluar" => ifunsetempty($_POST,"id_aset_keluar",""),
				"id_aset" => ifunsetempty($_POST,"id_aset",""),				
				"id_kondisi_barang" => ifunsetempty($_POST,"id_kondisi_barang",""),
			);			
			
			if(!empty($paramsData["id_aset_keluar"])) {									
				$this->db->where("id_aset_keluar",$paramsData["id_aset_keluar"]);				
				unset($paramsData["id_aset"]);				
				$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsData["diubah_oleh"] = $dataUser["id_admin"];
				$res = $this->db->update("aset_keluar",$paramsData);				
			} else {				
				$paramsData["tanggal_input"] = date("Y-m-d");
				$paramsData["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsData["dibuat_oleh"] = $dataUser["id_admin"];
				$res = $this->db->insert("aset_keluar",$paramsData);					
			}

			$paramsDataAset = array(
				"id_aset" => ifunsetempty($_POST,"id_aset",""),				
				"id_kondisi_barang" => ifunsetempty($_POST,"id_kondisi_barang",""),
			);	
			$resAset = false;
			if(!empty($paramsDataAset["id_aset"])) {									
				$this->db->where("id_aset",$paramsDataAset["id_aset"]);				
				unset($paramsDataAset["id_aset"]);				
				$paramsDataAset["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsDataAset["diubah_oleh"] = $dataUser["id_admin"];
				$paramsDataAset["status"] = "dijual";				
				$resAset = $this->db->update("aset",$paramsDataAset);				
			}
			
			
			$this->db->trans_complete();
			if(!$res && !$resAset){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsDataAset = array(						
				"id_aset_keluar" => ifunsetempty($args,"id_aset_keluar",""),									
			);

			$res = false;			
			
			if(!empty($paramsDataAset["id_aset_keluar"])) {								
				$this->db->where("id_aset_keluar",$paramsDataAset["id_aset_keluar"]);				
				$res = $this->db->delete("aset_keluar");
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
}