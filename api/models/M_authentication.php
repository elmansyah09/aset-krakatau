<?php 

/**
 * 
 */
class M_authentication extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function check_login($username,$password)
	{				
		$q = $this->db->query(" 
			select
				admin.*,
				karyawan.nik_karyawan,
				karyawan.nama_karyawan,
				case when admin.level = '1' then
					'Superadmin'
				when admin.level = '2' then
					'Admin'
				else
					'-'
				end as nama_usergroup
			from
				admin
			left join karyawan on karyawan.id_karyawan = admin.id_karyawan
			where 
			admin.status = 'aktif' and admin.username = ? AND admin.password = ?		
		",
				array($username,$password));
		
		return $q;
		
	}		
}