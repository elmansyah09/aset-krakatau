<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_aset_masuk extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}

	public function get($args = array(), $pagging = false)
	{	
		try {			
			
			

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			$this->db->from("aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("aset_masuk_detail","aset_masuk_detail.id_aset = aset.id_aset","left");
			$this->db->join("aset_masuk","aset_masuk.id_aset_masuk = aset_masuk_detail.id_aset_masuk","left");			
			$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("aset.status","aktif");
			$this->db->order_by("aset.tanggal_buat","ASC");
			// $this->db->group_by("aset.id_aset");
			$this->db_count = clone $this->db;

			$this->db->select("
				aset.*,
				aset_masuk.tanggal_input,
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");

			if($pagging) {
				$limit = isset($args["limit"])?$args["limit"]:10;
				$offset = isset($args["offset"])?$args["offset"]:0;
				$limit = (int) $limit;
				$offset = (int) $offset;				
				if($offset == 1){
					$offset = 0;
				}
				
				$resCount = $this->db_count->select("count(aset.id_aset) as COUNT");
				$totaldata = $resCount->get()->row()->COUNT;
				$this->db->limit($limit,$offset);
				$data = $this->db->get()->result_array();
				
				$end = $offset+$limit;
				if($end > $totaldata) {
					$end = $totaldata;
				}
				$from = $offset+1;
				if($totaldata == 0) {
					$from = 0;
				}

				$hasil = array(
					"data" => array(),
					"count" => $totaldata,
					"from" => $from,
					"end" => $end,
				);
				$no = $offset;
				$no++;
			} else {
				$data = $this->db->get()->result_array();
				$no = 1;
				$hasil = array(
					"data" => array()
				);
			}
			
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');
				$value["tgl_input"] = convert_tgl($value["tanggal_input"],'y-m-d');
				$hasil["data"][] = $value;
			}
			return response(200,"Data Aset Masuk",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				aset_masuk.tanggal_input,
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");
			$hasil = array();
			$data = array();
			if(isset($args["id_aset"]) && !empty($args["id_aset"])){
				
				$this->db->where("aset.id_aset",$args["id_aset"]);			

				$this->db->from("aset");
				$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = tipe_barang.id_tipe_barang","left");
				$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
				$this->db->join("aset_masuk_detail","aset_masuk_detail.id_aset = aset.id_aset","left");
				$this->db->join("aset_masuk","aset_masuk.id_aset_masuk = aset_masuk_detail.id_aset_masuk","left");			
				$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
				$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
				$this->db->where("aset.status","aktif");
				$this->db->order_by("aset.tanggal_buat","ASC");
				$this->db->group_by("aset.id_aset");
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Aset Masuk",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function simpan($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsDataAset = array(
				"id_aset" => ifunsetempty($args,"id_aset",""),				
				"nomor_seri" => ifunsetempty($args,"nomor_seri",""),
				"nama_barang" => ifunsetempty($args,"nama_barang",""),
				"id_jenis_barang" => ifunsetempty($args,"id_jenis_barang",""),
				"id_tipe_barang" => ifunsetempty($args,"id_tipe_barang",""),
				"tanggal_pembelian" => ifunsetempty($args,"tanggal_pembelian",""),
				"harga_barang" => ifunsetempty($args,"harga_barang",""),
				"nilai_buku" => ifunsetempty($args,"nilai_buku",""),
				"jumlah" => ifunsetempty($args,"jumlah",""),
				"jangka_waktu" => ifunsetempty($args,"jangka_waktu",""),
				"keterangan" => ifunsetempty($args,"keterangan",""),
				"id_kondisi_barang" => ifunsetempty($args,"id_kondisi_barang",""),
				"id_penanggung_jawab" => ifunsetempty($args,"id_penanggung_jawab",""),				
			);

			// print_r($paramsDataAset);
			$jumlah = ifunsetempty($args,"jumlah","");				
			
			$paramsData = array(
				"id_aset_masuk" => ifunsetempty($args,"id_aset_masuk",""),
			);			

			$paramsDataAset["tanggal_pembelian"] = convert_tgl2($paramsDataAset["tanggal_pembelian"]);
			$validTanggal = validasi_tanggal($paramsDataAset["tanggal_pembelian"],"Y-m-d");
			if(!$validTanggal){
				return response(403,"Data gagal disimpan",array(),array("Format tanggal tidak valid"));
				
			}			

			$idAsetMasuk = $paramsData["id_aset_masuk"];
			if(!empty($paramsData["id_aset_masuk"])) {									
				$this->db->where("id_aset_masuk",$paramsData["id_aset_masuk"]);				
				unset($paramsData["id_aset"]);				
				unset($paramsData["nomor_seri"]);
				$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsData["diubah_oleh"] = $dataUser["id_admin"];
				$res = $this->db->update("aset_masuk",$paramsData);				
			} else {								

				$paramsData["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsData["dibuat_oleh"] = $dataUser["id_admin"];
				$res = $this->db->insert("aset_masuk",$paramsData);	
				$idAsetMasuk = $this->db->insert_id();
			}
			
			$idAset = $paramsDataAset["id_aset"];
			$listNewIdAset = array();
			if(!empty($paramsDataAset["id_aset"])) {					
				$paramsDataAset["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsDataAset["diubah_oleh"] = $dataUser["id_admin"];
				$this->db->where("id_aset",$paramsDataAset["id_aset"]);
				$listNewIdAset[] = $paramsDataAset["id_aset"];
				unset($paramsDataAset["id_aset"]);				
				unset($paramsDataAset["jumlah"]);
				$res = $this->db->update("aset",$paramsDataAset);
				$hasil = $paramsDataAset;
			} else {				

				$resJenisBarang = $this->db->get_where("jenis_barang",array(
					"id_jenis_barang" => $paramsDataAset["id_jenis_barang"]
				));
				$jenisBarang = $resJenisBarang->row_array();			
				$prefixKode = $jenisBarang["kode_barang"].date("y")."-";
				$newNumber = $this->db->query("
					select
						coalesce(max(cast(replace(coalesce(nomor_seri),?,'') as int)),0)+1 as new_number
					from
						aset
					where
						status = 'aktif'
						and nomor_seri LIKE ?
				",array(
					$prefixKode,
					$prefixKode.'%'
				));


				$newNumber = $newNumber->row()->new_number;
				$paramsDataAset["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsDataAset["dibuat_oleh"] = $dataUser["id_admin"];
				for ($i=1; $i <= $jumlah; $i++) { 
					$nomor_seri = $prefixKode.substr(("00000".$newNumber),-6);
					$paramsDataAset["nomor_seri"] = $nomor_seri;
					$paramsDataAset["jumlah"] = 1;
					$newNumber++;
					$res = $this->db->insert("aset",$paramsDataAset);
					if(!$res){
						$this->db->trans_rollback();		
						return response(403,"Data gagal disimpan",array(),$res);
					}
					$hasil = $paramsDataAset;
					$listNewIdAset[] = $this->db->insert_id();					
				}
			}

			
			$resDeleteDetail = $this->db->delete("aset_masuk_detail",array("id_aset_masuk",$idAsetMasuk));	
			foreach ($listNewIdAset as $value) {
				$paramsDataDetail = array(
					"id_aset_masuk" => $idAsetMasuk,
					"id_aset" => $value
				);				
				$resDetail = $this->db->insert("aset_masuk_detail",$paramsDataDetail);
				if(!$resDetail){
					$this->db->trans_rollback();		
					return response(403,"Data gagal disimpan",array(),$res);
				}
			}		

			$this->db->trans_complete();

			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res || !$resDeleteDetail){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsDataAset = array(						
				"id_aset" => ifunsetempty($args,"id_aset",""),										
			);

			$res = false;			
			
			if(!empty($paramsDataAset["id_aset"])) {								
				$this->db->where("id_aset",$paramsDataAset["id_aset"]);
				$paramsDataAset["status"] = 'nonaktif';
				$res = $this->db->update("aset",$paramsDataAset);
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
}