<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_dashboard extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}


	public function get_keseluruhan($args = array())
	{	
		try {			
			
			$res = $this->db->query("
				select					
					count(id_aset) as jml
				from
					aset				
				where aset.status = 'aktif'					
			");

			$hasil = $res->result_array();

			return response(200,"Data Aset Keseluruhan",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_bulanan($args = array())
	{	
		try {			
			$params = array();
			$params[] = date("n");
			$params[] = date("Y");
			$params[] = date("n");
			$params[] = date("Y");
			$res = $this->db->query("
				select 
				(select					
					count(id_aset) as jml
				from
					aset				
				where aset.status = 'aktif'
				and MONTH(tanggal_buat) = ?
				and YEAR(tanggal_buat) = ?) as jml_aset_masuk,
				(select					
					count(id_aset_keluar) as jml
				from
					aset_keluar				
				where
				MONTH(tanggal_buat) = ?
				and YEAR(tanggal_buat) = ? ) as jml_aset_keluar
			",$params);

			$hasil = $res->result_array();

			return response(200,"Data Rekap Aset Bulanan",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_aset_jenis($args = array())
	{	
		try {			
			
			$res = $this->db->query("
				select
					jenis_barang.nama_jenis_barang,
					count(id_aset) as jml
				from
					jenis_barang
				left join aset on aset.id_jenis_barang = jenis_barang.id_jenis_barang and aset.status = 'aktif'	
				where jenis_barang.status = 'aktif'
				group by jenis_barang.id_jenis_barang
			");

			$hasil = $res->result_array();

			return response(200,"Data Aset Jenis Barang",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_aset_kondisi($args = array())
	{	
		try {			
			
			$res = $this->db->query("
				select
					kondisi_barang.nama_kondisi_barang,
					count(id_aset) as jml
				from
					kondisi_barang
				left join aset on aset.id_kondisi_barang = kondisi_barang.id_kondisi_barang and aset.status = 'aktif'	
				where kondisi_barang.status = 'aktif'
				group by kondisi_barang.id_kondisi_barang
			");

			$hasil = $res->result_array();

			return response(200,"Data Aset Kondisi Barang",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
	
	
}