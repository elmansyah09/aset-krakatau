<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_aset extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}

	public function get($args = array())
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			$this->db->from("aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");			
			$this->db->where("aset.status","aktif");
			$this->db->order_by("aset.tanggal_buat","ASC");
			$this->db->group_by("aset.id_aset");
			$data = $this->db->get()->result_array();

			$hasil = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');				
				$hasil[] = $value;
			}
			return response(200,"Data Aset Masuk",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang
			");
			$hasil = array();
			$data = array();
			if(isset($args["id_aset"]) && !empty($args["id_aset"])){
				
				$this->db->where("aset.id_aset",$args["id_aset"]);			

				$this->db->from("aset");
				$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = tipe_barang.id_tipe_barang","left");
				$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");				
				$this->db->where("aset.status","aktif");
				$this->db->order_by("aset.tanggal_buat","ASC");
				$this->db->group_by("aset.id_aset");
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Aset Masuk",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsDataAset = array(						
				"id_aset" => ifunsetempty($args,"id_aset",""),										
			);

			$res = false;			
			
			if(!empty($paramsDataAset["id_aset"])) {								
				$this->db->where("id_aset",$paramsDataAset["id_aset"]);
				$paramsDataAset["status"] = 'nonaktif';
				$res = $this->db->update("aset",$paramsDataAset);
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
}