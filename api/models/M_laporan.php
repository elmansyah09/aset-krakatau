<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_laporan extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}

	public function get_aktiva($args = array(), $pagging = false)
	{	
		try {			
			
			

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			if(isset($args["kondisi_barang"]) && !empty($args["kondisi_barang"])){
				$filters["kondisi_barang"] = $args["kondisi_barang"];			
				$this->db->where("kondisi_barang.id_kondisi_barang",$args["kondisi_barang"]);			
			}

			$this->db->from("vw_aset as aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
			$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("aset.status","aktif");
			$this->db->order_by("aset.tanggal_buat","ASC");			
			$this->db_count = clone $this->db;
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");

			if($pagging) {
				$limit = isset($args["limit"])?$args["limit"]:10;
				$offset = isset($args["offset"])?$args["offset"]:0;
				$limit = (int) $limit;
				$offset = (int) $offset;				
				if($offset == 1){
					$offset = 0;
				}
				
				$resCount = $this->db_count->select("count(aset.id_aset) as COUNT");
				$totaldata = $resCount->get()->row()->COUNT;
				$this->db->limit($limit,$offset);
				$data = $this->db->get()->result_array();
				
				$end = $offset+$limit;
				if($end > $totaldata) {
					$end = $totaldata;
				}
				$from = $offset+1;
				if($totaldata == 0) {
					$from = 0;
				}

				$hasil = array(
					"data" => array(),
					"count" => $totaldata,
					"from" => $from,
					"end" => $end,
				);
				$no = $offset;
				$no++;
			} else {
				$data = $this->db->get()->result_array();
				$no = 1;
				$hasil = array(
					"data" => array()
				);
			}

			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');
				$value["nilai_buku"] = $value["nilai_buku_baru"];				
				$hasil["data"][] = $value;
			}

			return response(200,"Data Aset Tetap",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_dijual($args = array(), $pagging = false)
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			$this->db->from("vw_aset as aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
			$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("aset.status","dijual");
			$this->db->order_by("aset.tanggal_buat","ASC");
			$this->db_count = clone $this->db;
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");
			
			if($pagging) {
				$limit = isset($args["limit"])?$args["limit"]:10;
				$offset = isset($args["offset"])?$args["offset"]:0;
				$limit = (int) $limit;
				$offset = (int) $offset;				
				if($offset == 1){
					$offset = 0;
				}
				
				$resCount = $this->db_count->select("count(aset.id_aset) as COUNT");
				$totaldata = $resCount->get()->row()->COUNT;
				$this->db->limit($limit,$offset);
				$data = $this->db->get()->result_array();
				
				$end = $offset+$limit;
				if($end > $totaldata) {
					$end = $totaldata;
				}
				$from = $offset+1;
				if($totaldata == 0) {
					$from = 0;
				}

				$hasil = array(
					"data" => array(),
					"count" => $totaldata,
					"from" => $from,
					"end" => $end,
				);
				$no = $offset;
				$no++;
			} else {
				$data = $this->db->get()->result_array();
				$no = 1;
				$hasil = array(
					"data" => array()
				);
			}

			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');
				$value["nilai_buku"] = $value["nilai_buku_baru"];				
				$hasil["data"][] = $value;
			}
			return response(200,"Data Aset Dijual",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_jenis_barang($args = array())
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			$this->db->from("vw_aset as aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
			$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("aset.status","aktif");
			$this->db->order_by("aset.tanggal_buat","ASC");
			$this->db->group_by("aset.id_aset");
			$data = $this->db->get()->result_array();

			$hasil = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');
				$value["nilai_buku"] = $value["nilai_buku_baru"];				
				$hasil[] = $value;
			}
			return response(200,"Data Aset",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_kondisi_barang($args = array())
	{	
		try {			
			
			$this->db->select("
				aset.*,				
				tipe_barang.nama_tipe_barang,
				jenis_barang.nama_jenis_barang,
				kondisi_barang.nama_kondisi_barang,
				karyawan.nama_karyawan as nama_penanggung_jawab,
				divisi.nama_divisi
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("aset.nama_barang",$search);
				$this->db->or_like("aset.nomor_seri",$search);
				$this->db->or_like("tipe_barang.nama_tipe_barang",$search);
				$this->db->or_like("jenis_barang.nama_jenis_barang",$search);				
				$this->db->group_end();
			}

			if(isset($args["jenis_barang"]) && !empty($args["jenis_barang"])){
				$filters["jenis_barang"] = $args["jenis_barang"];			
				$this->db->where("jenis_barang.id_jenis_barang",$args["jenis_barang"]);			
			}

			if(isset($args["tipe_barang"]) && !empty($args["tipe_barang"])){
				$filters["tipe_barang"] = $args["tipe_barang"];			
				$this->db->where("tipe_barang.id_tipe_barang",$args["tipe_barang"]);			
			}

			if(isset($args["kondisi_barang"]) && !empty($args["kondisi_barang"])){
				$filters["kondisi_barang"] = $args["kondisi_barang"];			
				$this->db->where("kondisi_barang.id_kondisi_barang",$args["kondisi_barang"]);			
			}

			$this->db->from("vw_aset as aset");
			$this->db->join("tipe_barang","tipe_barang.id_tipe_barang = aset.id_tipe_barang","left");
			$this->db->join("jenis_barang","jenis_barang.id_jenis_barang = tipe_barang.id_jenis_barang","left");
			$this->db->join("kondisi_barang","kondisi_barang.id_kondisi_barang = aset.id_kondisi_barang","left");
			$this->db->join("karyawan","karyawan.id_karyawan = aset.id_penanggung_jawab","left");
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("aset.status","aktif");
			$this->db->order_by("aset.tanggal_buat","ASC");
			$this->db->group_by("aset.id_aset");
			$data = $this->db->get()->result_array();

			$hasil = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$value["tgl_pembelian"] = convert_tgl($value["tanggal_pembelian"],'y-m-d');				
				$value["nilai_buku"] = $value["nilai_buku_baru"];
				$hasil[] = $value;
			}
			return response(200,"Data Aset",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
	
}