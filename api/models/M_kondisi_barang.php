<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_kondisi_barang extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get($args = array())
	{
		try {			
				
			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("kondisi_barang.nama_kondisi_barang",$search);				
				$this->db->group_end();
			}
			$this->db->where("status","aktif");
			$this->db->order_by("tanggal_buat",'ASC');
			$data = $this->db->get("kondisi_barang")->result_array();			

			return response(200,"Data Kondisi Barang",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_master($args = array())
	{
		try {			
				
			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("kondisi_barang.nama_kondisi_barang",$search);								
				$this->db->group_end();
			}
			$this->db->where("status","aktif");
			$this->db->from("kondisi_barang");		

			$this->db_count = clone $this->db;
			$this->db->order_by("tanggal_buat",'ASC');
			
			$limit = isset($args["limit"])?$args["limit"]:10;
			$offset = isset($args["offset"])?$args["offset"]:0;
			$limit = (int) $limit;
			$offset = (int) $offset;				
			if($offset == 1){
				$offset = 0;
			}
			
			$resCount = $this->db_count->select("count(id_kondisi_barang) as COUNT");
			$totaldata = $resCount->get()->row()->COUNT;
			$this->db->limit($limit,$offset);
			$data = $this->db->get()->result_array();
			
			$end = $offset+$limit;
			if($end > $totaldata) {
				$end = $totaldata;
			}
			$from = $offset+1;
			if($totaldata == 0) {
				$from = 0;
			}

			$hasil = array(
				"data" => array(),
				"count" => $totaldata,
				"from" => $from,
				"end" => $end,
			);
			$no = $offset;
			$no++;

			foreach ($data as $key => $value) {
				$value["no"] = $no++;				
				$hasil["data"][] = $value;
			}

			return response(200,"Data Jenis Barang",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function simpan($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsData = array(
				"id_kondisi_barang" => ifunsetempty($args,"id_kondisi_barang",""),
				"nama_kondisi_barang" => ifunsetempty($args,"nama_kondisi_barang",""),								
			);			
			
			if(!empty($paramsData["id_kondisi_barang"])) {									
				$this->db->where("id_kondisi_barang",$paramsData["id_kondisi_barang"]);				
				unset($paramsData["id_kondisi_barang"]);				
				$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsData["diubah_oleh"] = $dataUser["id_admin"];
				$res = $this->db->update("kondisi_barang",$paramsData);				
			} else {								
				$paramsData["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsData["dibuat_oleh"] = $dataUser["id_admin"];
				$res = $this->db->insert("kondisi_barang",$paramsData);					
			}

			
			$this->db->trans_complete();
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
						
			$hasil = array();
			$data = array();
			if(isset($args["id_kondisi_barang"]) && !empty($args["id_kondisi_barang"])){
				
				$this->db->where("kondisi_barang.id_kondisi_barang",$args["id_kondisi_barang"]);			

				$this->db->from("kondisi_barang");				
				$this->db->where("kondisi_barang.status","aktif");
				$this->db->order_by("kondisi_barang.tanggal_buat","ASC");				
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Aset Masuk",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}	
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsData = array(						
				"id_kondisi_barang" => ifunsetempty($args,"id_kondisi_barang",""),									
			);

			$res = false;			
			
			if(!empty($paramsData["id_kondisi_barang"])) {								
				$this->db->where("id_kondisi_barang",$paramsData["id_kondisi_barang"]);				
				$res = $this->db->update("kondisi_barang",array(
					"status" => "non_aktif"
				));
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

}