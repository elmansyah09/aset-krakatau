<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_user extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->db->cache_delete_all();
	}

	public function get($args = array())
	{	
		try {			
			
			$this->db->select("
				admin.id_admin,
				admin.username,
				admin.level,
				karyawan.nama_karyawan,
				case when admin.level = '1' then
					'Superadmin'
				when admin.level = '2' then
					'Admin'
				else
					''
				end as nama_level
			");

			$filters = array();

			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("admin.username",$search);				
				$this->db->group_end();
			}

			if(isset($args["level"]) && !empty($args["level"])){
				$filters["level"] = $args["level"];			
				$this->db->where("admin.level",$args["level"]);			
			}

			$this->db->from("admin");
			$this->db->join("karyawan","karyawan.id_karyawan = admin.id_karyawan","left");			
			$this->db->where("admin.status","aktif");
			$this->db->order_by("admin.tanggal_buat","ASC");			
			$data = $this->db->get()->result_array();

			$hasil = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;				
				$hasil[] = $value;
			}
			return response(200,"Data Pengguna",$hasil,array(),$filters);

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
			
			$this->db->select("
				admin.id_admin,
				admin.username,
				admin.level,
				karyawan.id_karyawan,
				karyawan.nama_karyawan,
				case when admin.level = '1' then
					'Superadmin'
				when admin.level = '2' then
					'Admin'
				else
					''
				end as nama_level
			");
			$hasil = array();
			$data = array();
			if(isset($args["id_admin"]) && !empty($args["id_admin"])){
				
				$this->db->where("admin.id_admin",$args["id_admin"]);			

				$this->db->from("admin");
				$this->db->join("karyawan","karyawan.id_karyawan = admin.id_karyawan","left");			
				$this->db->where("admin.status","aktif");
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Pengguna",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function simpan($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsData = array(
				"id_admin" => ifunsetempty($args,"id_admin",""),								
				"username" => ifunsetempty($args,"username",""),
				"id_karyawan" => ifunsetempty($args,"id_karyawan",""),
				"password" => md5(md5(ifunsetempty($args,"password","").ifunsetempty($args,"username",""))),
				"level" => ifunsetempty($args,"level",""),
			);

			$isGantiPassowrd = $args["is_ganti_password"];

			if(!empty($paramsData["id_admin"])) {									
				$this->db->where("id_admin",$paramsData["id_admin"]);				
				unset($paramsData["id_admin"]);
				if($isGantiPassowrd == 1){
					unset($paramsData["level"]);
				} else {
					unset($paramsData["password"]);
				}
				$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsData["diubah_oleh"] = $dataUser["id_admin"];
				$res = $this->db->update("admin",$paramsData);				
			} else {				
				$paramsData["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsData["dibuat_oleh"] = $dataUser["id_admin"];
				$res = $this->db->insert("admin",$paramsData);				
			}
			
			$this->db->trans_complete();
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsData = array(						
				"id_admin" => ifunsetempty($args,"id_admin",""),										
			);

			$res = false;			
			
			if(!empty($paramsData["id_admin"])) {								
				$this->db->where("id_admin",$paramsData["id_admin"]);
				$paramsData["status"] = 'nonaktif';
				$res = $this->db->update("admin",$paramsData);
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function ubah_password($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsData = array(
				"id_admin" => ifunsetempty($args,"id_admin",""),												
				"password" => md5(md5(ifunsetempty($args,"password","").ifunsetempty($args,"username",""))),				
			);

			$this->db->where("id_admin",$paramsData["id_admin"]);				
			unset($paramsData["id_admin"]);			
			$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
			$paramsData["diubah_oleh"] = $dataUser["id_admin"];
			$res = $this->db->update("admin",$paramsData);		
			
			$this->db->trans_complete();
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
}
