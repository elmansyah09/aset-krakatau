<?php
/**
 * Created by Elmansyah.
 * User: Developer
 * Date: 26/12/2020

 */
class M_simplelist extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_statuspegawai($args = array())
	{
		try {			
				
			if (isset($args) && is_array($args) && count($args) > 0) {
				$this->db->where($args);
			}		
			$this->db->order_by("STATUSPEGAWAIID");
			$data = $this->db->get("MASTER_STATUSPEGAWAI")->result_array();			

			return response(200,"Data Status Pegawai",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_agama($args = array())
	{
		try {			
				
			if (isset($args) && is_array($args) && count($args) > 0) {
				$this->db->where($args);
			}		
			$this->db->order_by("AGAMAID");
			$data = $this->db->get("MASTER_AGAMA")->result_array();			

			return response(200,"Data Master Agama",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_tipependidikan($args = array())
	{
		try {			
				
			// if (isset($args) && is_array($args) && count($args) > 0) {
			// 	$this->db->where($args);
			// }		
			// $this->db->order_by("AGAMAID");
			// $data = $this->db->get("MASTER_AGAMA")->result_array();			

			$data = array(
				array(
					"ID" => "1",
					"NAMA" => "Sekolah Dasar"
				),
				array(
					"ID" => "2",
					"NAMA" => "Sekolah Menengah Pertama"
				),
				array(
					"ID" => "3",
					"NAMA" => "Sekolah Menengah Atas"
				),
				array(
					"ID" => "4",
					"NAMA" => "Diploma I"
				),
				array(
					"ID" => "5",
					"NAMA" => "Diploma II"
				),
				array(
					"ID" => "6",
					"NAMA" => "Diploma III"
				),
				array(
					"ID" => "7",
					"NAMA" => "Diploma IV"
				),
				array(
					"ID" => "8",
					"NAMA" => "Sarjana (S-1)"
				),
				array(
					"ID" => "9",
					"NAMA" => "Magister (S-2)"
				),
				array(
					"ID" => "10",
					"NAMA" => "Doktor (S-3)"
				),
				
			);

			return response(200,"Data Tipe Pendidikan",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_jeniskontrak($args = array())
	{
		try {			
				
			$data = array(
				array(
					"ID" => "Kontrak",
					"NAMA" => "Kontrak"
				),
				array(
					"ID" => "Magang",
					"NAMA" => "Magang"
				),
				
			);

			return response(200,"Data Jenis Kontrak",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}


	public function get_statusperkawinan($args = array())
	{
		try {			
				
			$data = array(
				array(
					"ID" => "Menikah",
					"NAMA" => "Menikah"
				),
				array(
					"ID" => "Cerai",
					"NAMA" => "Cerai"
				),
				
			);

			return response(200,"Data Status Perkawinan",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_tipepekerjaan($args = array())
	{
		try {			
				
			$data = array(
				array(
					"ID" => "PNS",
					"NAMA" => "PNS"
				),
				array(
					"ID" => "Non PNS",
					"NAMA" => "Non PNS"
				),
				
			);

			return response(200,"Data TIpe Pekerjaan",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_statusanak($args = array())
	{
		try {			
				
			$data = array(
				array(
					"ID" => "Anak Kandung",
					"NAMA" => "Anak Kandung"
				),
				array(
					"ID" => "Anak Angkat",
					"NAMA" => "Anak Angkat"
				),
				
			);

			return response(200,"Data Status Anak",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_eselon($args = array())
	{
		try {			
				
			if (isset($args) && is_array($args) && count($args) > 0) {
				$this->db->where($args);
			}		
			$this->db->order_by("ESELON");
			$data = $this->db->get("MASTER_ESELON")->result_array();			

			return response(200,"Data Master Eselon",$data,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}
}