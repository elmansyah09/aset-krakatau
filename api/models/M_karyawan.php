<?php
/**
 * Created by Elmansyah Fauzi Rachman.
 * User: Developer
 *
 */
class M_karyawan extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get($args = array())
	{
		try {			
				
			$this->db->select("
				karyawan.*,
				divisi.nama_divisi,
				divisi.kode_divisi
			");
			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("karyawan.nama_karyawan",$search);	
				$this->db->or_like("karyawan.nik_karyawan",$search);
				$this->db->group_end();
			}
			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("karyawan.status","aktif");
			$this->db->order_by("karyawan.tanggal_buat",'ASC');
			$data = $this->db->get("karyawan")->result_array();			

			$hasilAkhir = array();
			$no = 1;
			foreach ($data as $key => $value) {
				$value["no"] = $no++;
				$hasilAkhir[] = $value;
			}

			return response(200,"Data Karyawan",$hasilAkhir,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_master($args = array())
	{
		try {			
				
			
			if(isset($args["search"]) && !empty($args["search"])){
				$search = $args["search"];				
				$filters["search"] = $search;
				$this->db->group_start();
				$this->db->or_like("karyawan.nama_karyawan",$search);	
				$this->db->or_like("karyawan.nik_karyawan",$search);
				$this->db->group_end();
			}

			$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
			$this->db->where("karyawan.status","aktif");
			$this->db->order_by("karyawan.tanggal_buat",'ASC');
			$this->db->from("karyawan");		

			$this->db_count = clone $this->db;
			$this->db->select("
				karyawan.*,
				divisi.nama_divisi,
				divisi.kode_divisi
			");
			$this->db->order_by("karyawan.tanggal_buat",'ASC');
			
			$limit = isset($args["limit"])?$args["limit"]:10;
			$offset = isset($args["offset"])?$args["offset"]:0;
			$limit = (int) $limit;
			$offset = (int) $offset;				
			if($offset == 1){
				$offset = 0;
			}
			
			$resCount = $this->db_count->select("count(id_karyawan) as COUNT");
			$totaldata = $resCount->get()->row()->COUNT;
			$this->db->limit($limit,$offset);
			$data = $this->db->get()->result_array();
			
			$end = $offset+$limit;
			if($end > $totaldata) {
				$end = $totaldata;
			}
			$from = $offset+1;
			if($totaldata == 0) {
				$from = 0;
			}

			$hasil = array(
				"data" => array(),
				"count" => $totaldata,
				"from" => $from,
				"end" => $end,
			);
			$no = $offset;
			$no++;

			foreach ($data as $key => $value) {
				$value["no"] = $no++;				
				$hasil["data"][] = $value;
			}

			return response(200,"Data Karyawan",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function simpan($args = array())
	{	
		
		try {			
			$this->db->trans_start();
			$dataUser = isset($args["user"])?$args["user"]:array();						
			if(!is_array($dataUser)){
				return response(403,"Authentifiction Failed",array(),$res);
			}		
			
			$hasil = array();
			$paramsData = array(
				"id_karyawan" => ifunsetempty($args,"id_karyawan",""),
				"nik_karyawan" => ifunsetempty($args,"nik_karyawan",""),
				"nama_karyawan" => ifunsetempty($args,"nama_karyawan",""),			
				"id_divisi" => ifunsetempty($args,"id_divisi",""),
			);			
			
			if(!empty($paramsData["id_karyawan"])) {									
				$this->db->where("id_karyawan",$paramsData["id_karyawan"]);				
				unset($paramsData["id_karyawan"]);				
				$paramsData["tanggal_ubah"] = date("Y-m-d H:i:s");
				$paramsData["diubah_oleh"] = $dataUser["id_admin"];
				$res = $this->db->update("karyawan",$paramsData);				
			} else {								
				$paramsData["tanggal_buat"] = date("Y-m-d H:i:s");
				$paramsData["dibuat_oleh"] = $dataUser["id_admin"];
				$res = $this->db->insert("karyawan",$paramsData);					
			}

			
			$this->db->trans_complete();
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
				$this->db->trans_commit();
			}
			
			if(!$res){
				$this->db->trans_rollback();
				return response(403,"Data gagal disimpan",array(),$res);
			}

			return response(200,"Data berhasil disimpan",$hasil,array());

		} catch (\Throwable $th) {
			$this->db->trans_rollback();
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

	public function get_detail($args = array())
	{	
		try {			
						
			$hasil = array();
			$data = array();
			$this->db->select("
				karyawan.*,
				divisi.nama_divisi,
				divisi.kode_divisi
			");
			if(isset($args["id_karyawan"]) && !empty($args["id_karyawan"])){
				
				$this->db->where("karyawan.id_karyawan",$args["id_karyawan"]);			

				$this->db->join("divisi","divisi.id_divisi = karyawan.id_divisi","left");
				$this->db->from("karyawan");				
				$this->db->where("karyawan.status","aktif");
				$this->db->where("karyawan.status","aktif");
				$this->db->order_by("karyawan.tanggal_buat",'ASC');				
				$data = $this->db->get()->result_array();
			}			


			if(count($data) == 0){
				return response(403,"Data Tidak Ditemukan",$hasil,array());	
			}

			$hasil = $data[0];
			
			return response(200,"Data Detail Aset Masuk",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}	
	}

	public function hapus($args = array())
	{	
		try {			
			$hasil = array();
			$paramsData = array(						
				"id_karyawan" => ifunsetempty($args,"id_karyawan",""),									
			);

			$res = false;			
			
			if(!empty($paramsData["id_karyawan"])) {								
				$this->db->where("id_karyawan",$paramsData["id_karyawan"]);				
				$res = $this->db->update("karyawan",array(
					"status" => "non_aktif"
				));
			} 

			if(!$res){
				return response(403,"Data gagal dihapus",array(),$res);
			}

			return response(200,"Data berhasil dihapus",$hasil,array());

		} catch (\Throwable $th) {
			return response(500,"Terjadi Kesalahan dalam proses",array(),$th);
		}
	}

}