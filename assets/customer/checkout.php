<!DOCTYPE html>
<html>
<title>Nanda Permata Catering</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

* {
  box-sizing: border-box;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  margin: 0 -16px;
}

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

.container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}

input[type=text], input[type=date], input[type=time] {
  width: 100%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}

a {
  color: #2196F3;
}

hr {
  border: 1px solid lightgrey;
}

span.price {
  float: right;
  color: grey;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 800px) {
  .row {
    flex-direction: column-reverse;
  }
  .col-25 {
    margin-bottom: 20px;
  }
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/catering/customer/index.php" class="w3-bar-item w3-button"><b>CV.</b> Nanda Permata Catering</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/catering/customer/index.php" class="w3-bar-item w3-button">Home</a>
      <a href="/catering/customer/caraorder.php" class="w3-bar-item w3-button">Cara Order</a>
      <a href="/catering/customer/menu.php" class="w3-bar-item w3-button">Daftar Menu</a>
      <a href="/catering/customer/kontak.php" class="w3-bar-item w3-button">Kontak Kami</a>
       <div class="w3-dropdown-hover w3-hide-small w3-right">
          <button class="w3-button" title="Notifications"><b>Hai!</b> Chessy</button>     
          <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="right:0">
            <a href="/catering/customer/profil_akun.php" class="w3-bar-item w3-button">Profil Akun</a>
            <a href="/catering/customer/pesanan.php" class="w3-bar-item w3-button">Keranjang</a>
            <a href="/catering/customer/pembayaran_detail.php" class="w3-bar-item w3-button">Riwayat Pesanan</a>
            <a href="/catering/login.php" class="w3-bar-item w3-button">Keluar</a>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">
  <br>
  <br>
  <br>

<!-- End page content -->
</div>
 <div class="w3-content">
    <h2 align="center"><b>DETAIL</b></h2>
    <hr>
<div class="row">
  <div class="col-75">
    <div class="container">
      <form action="/catering/customer/pesan.php">
        <div class="row">
          <div class="col-50">
            <h3><b>Detail Pesanan</b></h3>
            <hr>
            <label for="nama"><i class="fa fa-user"></i> Nama Lengkap</label>
            <input type="text" id="nama" name="nama" placeholder="Nama Lengkap" required>
            <label for="telp"><i class="fa fa-envelope"></i> No Telp</label>
            <input type="text" id="telp" name="telp" placeholder="Nomor Telepon" required>
            <label for="alamat"><i class="fa fa-address-card-o"></i> Alamat Lengkap</label>
            <input type="text" id="alamat" name="alamat" placeholder="Alamat Lengkap" required>
            <div class="row">
              <div class="col-50">
                <label for="kecamatan"> Kecamatan</label>
                <input type="text" id="kecamatan" name="kecamatan" placeholder="Kecamatan" required>
              </div>
              <div class="col-50">
                <label for="kelurahan"> Kelurahan</label>
                <input type="text" id="kelurahan" name="kelurahan" placeholder="Kelurahan" required>
              </div>
            </div>
            <label for="kodepos"><i class="fa fa-institution"></i> Kode Pos</label>
            <input type="text" id="kodepos" name="kodepos" placeholder="Kode Pos" required>
          </div>

          <div class="col-50">
            <br>
            <br>
            <hr>
            <label for="tglpesan">Tanggal Pemesanan</label>
            <input type="date" id="tglpesan" name="tglpesan" required>
            <label for="tglantar">Tanggal Pengantaran Pesanan</label>
            <input type="date" id="tglantar" name="tglantar" required>
            <label for="waktuantar">Waktu Pengantaran Pesanan</label>
            <input type="time" id="waktuantar" name="waktuantar" required>
            <label for="catatan">Catatan</label>
            <input type="text" id="catatan" name="catatan" placeholder="Tambahkan Catatan">
          </div>    
        </div>
        <input type="submit" value="PESAN" class="btn">
    </div>
  </div>
  <div class="col-25">
    <div class="container">
      <h4><b>Detail Order</b></h4>
      <hr>
      <p>Paket Prasmanan Biasa<br><span class="price">1 x Rp. 50.000</span></p><br>
      <p>Nasi Ikan Telur<br><span class="price">1 x Rp. 25.000</span></p>
      <br>
      <hr>
      <p>Ongkos Kirim<span class="price">Rp. 0</span></p>
      <p>Total <span class="price" style="color:black"><b>Rp. 75.000</b></span></p>
    </div>
  </div>
</div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <hr>
  </div>





<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by Chessy</a></p>
</footer>

<script>
var modal = document.getElementById('food1');
var modal = document.getElementById('food2');
var modal = document.getElementById('food3');
var modal = document.getElementById('food4');
var modal = document.getElementById('food5');
var modal = document.getElementById('food6');
var modal = document.getElementById('food7');
var modal = document.getElementById('food8');
var modal = document.getElementById('food9');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

