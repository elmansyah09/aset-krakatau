<!DOCTYPE html>
<html>
<title>Nanda Permata Catering</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
/* The Close Button (x) */
.close {
  position: absolute;
  right: 25px;
  top: 0;
  color: #000;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: red;
  cursor: pointer;
}
}
</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/catering/customer/index.php" class="w3-bar-item w3-button"><b>CV.</b> Nanda Permata Catering</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/catering/customer/index.php" class="w3-bar-item w3-button">Home</a>
      <a href="/catering/customer/caraorder.php" class="w3-bar-item w3-button">Cara Order</a>
      <a href="/catering/customer/menu.php" class="w3-bar-item w3-button">Daftar Menu</a>
      <a href="/catering/customer/kontak.php" class="w3-bar-item w3-button">Kontak Kami</a>
       <div class="w3-dropdown-hover w3-hide-small w3-right">
          <button class="w3-button" title="Notifications"><b>Hai!</b> Chessy</button>     
          <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="right:0">
            <a href="/catering/customer/profil_akun.php" class="w3-bar-item w3-button">Profil Akun</a>
            <a href="/catering/customer/pesanan.php" class="w3-bar-item w3-button">Keranjang</a>
            <a href="/catering/customer/pembayaran.php" class="w3-bar-item w3-button">Riwayat Pesanan</a>
            <a href="/catering/login.php" class="w3-bar-item w3-button">Keluar</a>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">

<div class="w3-container" style="padding:90px 16px" id="menu">
  <h2 class="w3-center"><b>Daftar Menu</b></h2>
  <hr>
  <div class="w3-row-padding">
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food1.jpg" style="width:100%">
      <h3>Paket Prasmanan Biasa</h3>
      <p class="w3-opacity">(Rp.50.000/porsi)</p>
      <p><button onclick="document.getElementById('food1').style.display='block'" class="w3-button w3-green w3-block">Detail</button>
</p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food2.jpg" alt="Jane" style="width:100%">
      <h3>Nasi Ikan Telur</h3>
      <p class="w3-opacity">(Rp.25.000/porsi)</p>
     <p><button onclick="document.getElementById('food2').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food3.jpg" alt="Mike" style="width:100%">
      <h3>Paket Prasmanan VIP</h3>
      <p class="w3-opacity">(Rp.75.000/porsi)</p>
      <p><button onclick="document.getElementById('food3').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
  </div>
  <div class="w3-row-padding">
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food4.jpg" alt="John" style="width:100%">
      <h3>Paket Nasi Ayam</h3>
      <p class="w3-opacity">(Rp.33.000/porsi)</p>
      <p><button onclick="document.getElementById('food4').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food5.jpg" alt="Jane" style="width:100%">
      <h3>Paket Nasi Ikan</h3>
      <p class="w3-opacity">(Rp.20.000/porsi)</p>
     <p><button onclick="document.getElementById('food5').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food6.jpg" alt="Mike" style="width:100%">
      <h3>Paket Soto Makassar</h3>
      <p class="w3-opacity">(Rp.23.000/porsi)</p>
     <p><button onclick="document.getElementById('food6').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
  </div>
  <div class="w3-row-padding">
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food7.jpg" alt="John" style="width:100%">
      <h3>Paket Sate Ayam</h3>
      <p class="w3-opacity">(Rp.15.000/porsi)</p>
      <p><button onclick="document.getElementById('food7').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food8.jpg" alt="Jane" style="width:100%">
      <h3>Paket Bakso</h3>
      <p class="w3-opacity">(Rp.12.000/porsi)</p>
     <p><button onclick="document.getElementById('food8').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
    <div class="w3-col m4 w3-margin-bottom">
      <img src="/catering/images/food9.jpg" alt="Mike" style="width:100%">
      <h3>Siomay</h3>
      <p class="w3-opacity">(Rp.20.000/porsi)</p>
     <p><button onclick="document.getElementById('food9').style.display='block'" class="w3-button w3-green w3-block">Detail</button></p>
    </div>
  </div>
</div>

<!-- pop up -->
 <div id="food1" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food1').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60" id="about">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food1.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Prasmanan Biasa</h1>
              <h4>(Rp.50.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Sup Biasa</li>
                <li>Bihun</li>
                <li>Asinan</li>
                <li>Ayam Semur</li>
                <li>Telur Balado</li>
                <li>Sambal Goreng</li>
                <li>Kerupuk</li>
                <li>Asinan</li>
                <li>Makanan Penutup (Es Buah + Aneka kue)</li>
              </ul>   
            </div>
            <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>

  <div id="food2" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food2').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food2.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Nasi Ikan Telur</h1>
              <h4>(Rp.25.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Sambal Goreng</li>
                <li>Capcay</li>
                <li>Mie Goreng</li>
                <li>Pencuci Mulut (Pisang)</li>
              </ul>   
            </div>
            <a href="/catering/customer/pesanan-2.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>

   <div id="food3" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food3').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food3.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Prasmanan VIP</h1>
              <h4>(Rp.75.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Sup Ayam Kumplit</li>
                <li>Capcay</li>
                <li>Asinan</li>
                <li>Telor Balado</li>
                <li>Ikan Asem Manis</li>
                <li>Udang Goreng Tepung</li>
                <li>Ayam Goreng</li>
                <li>Sambal Goreng</li>
                <li>Ayam Semur</li>
                <li>Kerupuk</li>
                <li>Makanan Penutup (Es Buah + Aneka kue + Aneka Puding + Buah)</li>
              </ul>   
            </div>
             <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
  <div id="food4" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food4').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food4.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Nasi Ayam</h1>
              <h4>(Rp.33.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Sambal Goreng</li>
                <li>Capcay</li>
                <li>Bihun Goreng</li>
                <li>Mie Goreng</li>
                <li>Pencuci Mulut (Pisang + kue)</li>
              </ul>   
            </div>
             <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
  <div id="food5" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food5').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food5.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Nasi Ikan</h1>
              <h4>(Rp.20.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Sambal Goreng</li>
                <li>Capcay</li>
                <li>Bihun Goreng</li>
                <li>Kerupuk</li>
              </ul>   
            </div>
              <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
  <div id="food6" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food6').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food6.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Soto Makassar</h1>
              <h4>(Rp.23.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Buras/Ketupat</li>
                <li>Daging</li>
                <li>Babat</li>
                <li>Sambal</li>
                <li>Jeruk</li>
                <li>Bawang Goreng</li>
              </ul>   
            </div>
              <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
  <div id="food7" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food7').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food7.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Sate Ayam</h1>
              <h4>(Rp.15.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Nasi Putih</li>
                <li>Lontong</li>
                <li>Bumbu Kacang</li>
                <li>Bumbu Kecap</li>
              </ul>   
            </div>
              <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
 <div id="food8" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food8').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food8.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Paket Bakso</h1>
              <h4>(Rp.12.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Buras</li>
                <li>Ketupat</li>
                <li>Tahu</li>
                <li>Bihun</li>
                <li>Mie</li>
                <li>Sambal</li>
                <li>Jeruk</li>
              </ul>   
            </div>
              <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
  <div id="food9" class="w3-modal">
    <div class="w3-modal-content w3-animate-top w3-card-4">
      <header class="w3-container w3-white w3-center"> 
        <span onclick="document.getElementById('food9').style.display='none'" class="close" title="Close Modal">&times;</span>
      </header>
      <div class="w3-container">
        <div class="w3-row w3-padding-60">
          <div class="w3-col m6 w3-padding-large w3-hide-small">
            <hr>
           <img src="/catering/images/food9.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
          </div>
            <div class="w3-col m6 w3-padding-medium">
              <h1>Siomay</h1>
              <h4>(Rp.20.000/porsi)</h4>
              <ul style="list-style-type:circle;">
                <li>Kentang</li>
                <li>Telur</li>
                <li>Kol</li>
                <li>Pare</li>
                <li>Tahu</li>
              </ul>   
            </div>
             <a href="/catering/customer/pesanan.php"><button class="w3-button w3-block w3-teal w3-padding-8 w3-section w3-right">PESAN</button></a>
          </div>
      </div>
    </div>
  </div>
<!-- End page content -->
</div>
  <br>
  <br>
  <br>


<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by Chessy</a></p>
</footer>

<script>
var modal = document.getElementById('food1');
var modal = document.getElementById('food2');
var modal = document.getElementById('food3');
var modal = document.getElementById('food4');
var modal = document.getElementById('food5');
var modal = document.getElementById('food6');
var modal = document.getElementById('food7');
var modal = document.getElementById('food8');
var modal = document.getElementById('food9');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

