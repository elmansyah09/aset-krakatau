<!DOCTYPE html>
<html>
<title>Nanda Permata Catering</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
/* Product Quantity */
.quantity {
 
  margin-right: 60px;
}
.quantity input {
  -webkit-appearance: none;
  border: none;
  text-align: center;
  width: 32px;
  font-size: 16px;
  color: #43484D;
  font-weight: 300;
}

button[class*=btn] {
  width: 30px;
  height: 30px;
  background-color: #E1E8EE;
  border-radius: 6px;
  border: none;
  cursor: pointer;
}
.minus-btn img {
  margin-bottom: 3px;
}
.plus-btn img {
  margin-top: 2px;
}
button:focus,
input:focus {
  outline:0;
}

/* button checkout */
.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 18%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}

/* button continue shopping */
.btn-shop {
  background-color: #35a5ce;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 22%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;

}

.btn-shop:hover {
  background-color: #3bacd6;
}

</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/catering/customer/index.php" class="w3-bar-item w3-button"><b>CV.</b> Nanda Permata Catering</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/catering/customer/index.php" class="w3-bar-item w3-button">Home</a>
      <a href="/catering/customer/caraorder.php" class="w3-bar-item w3-button">Cara Order</a>
      <a href="/catering/customer/menu.php" class="w3-bar-item w3-button">Daftar Menu</a>
      <a href="/catering/customer/kontak.php" class="w3-bar-item w3-button">Kontak Kami</a>
       <div class="w3-dropdown-hover w3-hide-small w3-right">
          <button class="w3-button" title="Notifications"><b>Hai!</b> Chessy</button>     
          <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="right:0">
            <a href="/catering/customer/profil_akun.php" class="w3-bar-item w3-button">Profil Akun</a>
            <a href="/catering/customer/pesanan.php" class="w3-bar-item w3-button">Keranjang</a>
            <a href="/catering/customer/pembayaran.php" class="w3-bar-item w3-button">Riwayat Pesanan</a>
            <a href="/catering/login.php" class="w3-bar-item w3-button">Keluar</a>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">
 <div class="w3-content" id="contact">
  <br>
  <br>
  <br>
    <h2 align="center"><b>Pembayaran</b></h2>
    <hr>
    <div class="w3-margin">
      <p align="center">Tidak ada pesanan yang belum dibayar</p>
    </ul>
  </div>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <hr>
</div>
</div>

<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by Chessy</a></p>
</footer>

<script>
var modal = document.getElementById('food1');
var modal = document.getElementById('food2');
var modal = document.getElementById('food3');
var modal = document.getElementById('food4');
var modal = document.getElementById('food5');
var modal = document.getElementById('food6');
var modal = document.getElementById('food7');
var modal = document.getElementById('food8');
var modal = document.getElementById('food9');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

