<!DOCTYPE html>
<html>
<title>Nanda Permata Catering</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
/* button checkout */
.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 25%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}



</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/catering/customer/index.php" class="w3-bar-item w3-button"><b>CV.</b> Nanda Permata Catering</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/catering/customer/index.php" class="w3-bar-item w3-button">Home</a>
      <a href="/catering/customer/caraorder.php" class="w3-bar-item w3-button">Cara Order</a>
      <a href="/catering/customer/menu.php" class="w3-bar-item w3-button">Daftar Menu</a>
      <a href="/catering/customer/kontak.php" class="w3-bar-item w3-button">Kontak Kami</a>
       <div class="w3-dropdown-hover w3-hide-small w3-right">
          <button class="w3-button" title="Notifications"><b>Hai!</b> Chessy</button>     
          <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="right:0">
            <a href="/catering/customer/profil_akun.php" class="w3-bar-item w3-button">Profil Akun</a>
            <a href="/catering/customer/pesanan.php" class="w3-bar-item w3-button">Keranjang</a>
            <a href="/catering/customer/pembayaran.php" class="w3-bar-item w3-button">Riwayat Pesanan</a>
            <a href="/catering/login.php" class="w3-bar-item w3-button">Keluar</a>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">
 <div class="w3-content" id="contact">
  <br>
  <br>
  <br>
    <h2 align="center"><b>Riwayat Pesanan</b></h2>
    <hr>
    <div class="w3-margin">
      <table>
        <tr>
          <th></th>
          <th></th>
          <th></th>
          <th><b>ID Order</b></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b>Produk</b></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b>Total</b></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b>Status</b></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b>Status Pengiriman</b></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th><b>Aksi</b></th>
          </tr> 
      </table>
      <ul class="w3-ul w3-hoverable w3-white">
        <table class="w3-table w3-bordered">
          <tr>
            <td>20192382</td>
            <td>Paket Prasmanan Biasa</td>
            <td>Rp. 500.000</td>
            <th></th>
            <td>Belum Bayar</td>
            <td>-</td>
            <td><a href="/catering/customer/pesan.php">Konfirmasi Pembayaran</a></td>
          </tr> 
           <tr>
            <td>20192234</td>
            <td>Nasi Ikan Telur</td>
            <td>Rp. 250.000</td>
            <th></th>
            <td>Dibayar</td>
            <td>Menunggu Pengiriman</td>
            <td><a href="/catering/file/INVOICE.pdf" target="_blank">Cetak Invoice</a></td>
          </tr> 
           <tr>
            <td>20193244</td>
            <td>Paket Prasmanan VIP</td>
            <td>Rp. 750.000</td>
            <th></th>
            <td>Dibatalkan</td>
            <td>-</td>
            <td><a href="/catering/customer/konfirmasi_pesanan.php"></a></td>
          </tr> 
        </table>
      </ul>
    </ul>
  </div>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <hr>
</div>
</div>








<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by Chessy</a></p>
</footer>

<script>
var modal = document.getElementById('food1');
var modal = document.getElementById('food2');
var modal = document.getElementById('food3');
var modal = document.getElementById('food4');
var modal = document.getElementById('food5');
var modal = document.getElementById('food6');
var modal = document.getElementById('food7');
var modal = document.getElementById('food8');
var modal = document.getElementById('food9');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

