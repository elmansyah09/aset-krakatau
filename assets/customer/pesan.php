<!DOCTYPE html>
<html>
<title>Nanda Permata Catering</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

* {
  box-sizing: border-box;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  margin: 0 -16px;
}

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

.container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}
input[type=text], input[type=date], input[type=time], select {
  width: 100%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}


.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}

</style>
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="/catering/customer/index.php" class="w3-bar-item w3-button"><b>CV.</b> Nanda Permata Catering</a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
      <a href="/catering/customer/index.php" class="w3-bar-item w3-button">Home</a>
      <a href="/catering/customer/caraorder.php" class="w3-bar-item w3-button">Cara Order</a>
      <a href="/catering/customer/menu.php" class="w3-bar-item w3-button">Daftar Menu</a>
      <a href="/catering/customer/kontak.php" class="w3-bar-item w3-button">Kontak Kami</a>
       <div class="w3-dropdown-hover w3-hide-small w3-right">
          <button class="w3-button" title="Notifications"><b>Hai!</b> Chessy</button>     
          <div class="w3-dropdown-content w3-card-4 w3-bar-block" style="right:0">
            <a href="/catering/customer/profil_akun.php" class="w3-bar-item w3-button">Profil Akun</a>
            <a href="/catering/customer/pesanan.php" class="w3-bar-item w3-button">Keranjang</a>
            <a href="/catering/customer/pembayaran.php" class="w3-bar-item w3-button">Riwayat Pesanan</a>
            <a href="/catering/login.php" class="w3-bar-item w3-button">Keluar</a>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">
 <div class="w3-content" id="contact">
  <br>
  <br>
  <br>
    <h2 align="center"><b>Detail Pembayaran</b></h2>
    <hr>
    <div class="row">
      <div class="col-75">
        <div class="container">
          <form action="/catering/customer/konfirmasi_pesanan.php">
            <div class="row">
              <div class="col-50">
                  <h4><b>Konfirmasi Pembayaran</b></h4>
                  <hr>
                  <label for="idorder">ID ORDER</label>
                  <input type="text" id="idorder" name="idorder" value="20191236" disabled>
                  <label for="banktujuan">Bank Tujuan</label>
                   <select id="banktujuan" name="banktujuan" required>
                    <option value="bca">BCA</option>
                    <option value="mandiri">MANDIRI</option>
                  </select>
                  <label for="bankanda">Bank Anda</label>
                  <input type="text" id="bankanda" name="bankanda" placeholder="Bank Anda" required>
                  <label for="rek">Rekening Atas Nama</label>
                  <input type="text" id="rek" name="rek" required placeholder="Rekening Atas Nama" required>
                  <label for="nominaltf">Nominal Transfer</label>
                  <input type="text" id="nominaltf" name="nominaltf" placeholder="-- Rp --" required>
                  <label for="tgltf">Tanggal Transfer</label>
                  <input type="date" id="tgltf" name="tgltf" required>
                  <label for="tgltf">Upload Bukti Transfer</label><br>
                  <input type="file" name="myFile" multiple required>
                  <input type="submit" value="KONFIRMASI" class="btn">
                  <br>
                  <br>
                </div>   
              <div class="col-50">
                <br>
                <p><b>Dear Chesy,</b></p>
                <p>Anda sekarang dapat melakukan transfer bank <br>
                   ke salah satu rekening bank yang tercantum di bawah ini:<br><br>
                   BCA 1123-5322-13 (a.n Nanda Permata)<br>
                   Mandiri 1236-2141-2144-0 (a.n Nanda Permata)<br><br>
                   Harap dicatat bahwa pembayaran harus dilakukan <br>
                   dalam <b>Waktu 1 x 24 JAM</b> untuk mengamankan pesanan Anda. <br>
                   Kegagalan melakukan pembayaran dalam kerangka waktu yang ditentukan <br>
                   akan mengakibatkan pembatalan pesanan Anda.<br>
                   Setelah Anda melakukan pembayaran, <br>
                   silakan beri tahu kami detail transaksi Anda di sini .</p>
                <p><b>Total</b> <span class="price" style="color:black"><b>Rp. 75.000</b></span></p>
              </div>
                
              </form>
            </div>
        </div>
      </div>
    </div>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <hr>
    </div>
    </div>

<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by Chessy</a></p>
</footer>

<script>
var modal = document.getElementById('food1');
var modal = document.getElementById('food2');
var modal = document.getElementById('food3');
var modal = document.getElementById('food4');
var modal = document.getElementById('food5');
var modal = document.getElementById('food6');
var modal = document.getElementById('food7');
var modal = document.getElementById('food8');
var modal = document.getElementById('food9');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>

