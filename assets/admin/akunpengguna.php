<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Halaman Admin Sistem Informasi Manajemen Aset</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/krakatau/template/admin/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="/krakatau/template/admin/css/fontastic.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="/krakatau/template/admin/vendor/font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/krakatau/assets/plugins/datatables/dataTables.bootstrap.css"> 
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">  -->
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/krakatau/template/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="/krakatau/template/admin/css/custom.css">
    <link rel="stylesheet" href="/krakatau/template/css/admin.css">


    
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
  </head>
 
  <body>
    <div class="page home-page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <!-- Search Box-->
        
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="index.php/admin" class="navbar-brand">
                  <div class="brand-text brand-big"><strong>Sistem Informasi Manajemen Aset</strong></div>
                  
                <!-- Toggle Button--><a id="toggle-btn"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <!-- Logout    -->
                <li class="nav-item"><a href="index.php/login/logout" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
              </ul>
             </div>
          </div>
        </nav>
      </header>


      <div class="page-content d-flex align-items-stretch">
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="/krakatau/template/image/avatar.png" class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4">User</h1>
              <p>Pengguna</p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Menu</span>

          <ul class="list-unstyled">
             <li class="active"> <a href="dashboard.php"><i class="icon-home"></i>Dashboard</a></li>

                <li> <a href='profil.php'><i class='fa fa-user-o'></i>Profil Pengguna</a></li>
                <li> <a href='akunpengguna.php'><i class='icon-interface-windows'></i>Data User</a></li>
                <li> <a href='asetmasuk.php'><i class="fa fa-files-o"></i>Data Aset Masuk</a></li>
                <li> <a href='asetkeluar.php'><i class="fa fa-server"></i>Data Aset Keluar</a></li>
                <li> <a href='laporan.php'><i class="fa fa-bar-chart"></i>Laporan</a></li>

             
          </ul>
        </nav>

        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Data User</h2>
            </div>
          </header>


          <!-- Dashboard Counts Section-->
    
          <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                
                <div class="col-lg-12">
                  <div class="card">
                  
                   <div class="card-body text-left">
                       <a href="akunpengguna_tambah.php"><button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah User</button></a>
                      </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Daftar User</h3>
                    </div>
                    <div class="card-body">
                      <table class="table">
                        <thead>
                          <tr>
                              <th>No.</th>
                              <th>Name</th>
                              <th>Username</th>
                              <th>Level</th>
                              <th align="center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                      </table>
                    </div>
                  
                </a>
              </div>
            </div>
          </section>
         
          
          <br>
          <br>
          <br>
          <br>
          <br>
          
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>Copyright &copy; 2021. All rights reserved.</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>PT <a href="#" class="external">Krakatau Technology Information</a></p>
                  <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
              </div>
            </div>
          </footer>
        

        </div>
      </div>
    </div>
      
      
    <!-- Javascript files-->

    <!-- DataTables -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>


    <script>
      $(function () {
        $('#datatables').DataTable();
      });
    </script>

  </body>
</html>