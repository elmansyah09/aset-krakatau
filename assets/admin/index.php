<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/krakatau/css/w3.css">
<body class="w3-content" style="max-width:500px">
<style>

form {
    border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 10px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #419bab;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

body {
    background-image: url("/krakatau/template/image/17973908.JPG");
    background-color: #cccccc;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
    font-family: system-ui; 
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
</style>
<body>
<br>
<hr>
<center><b><h2>Login Administrator</h2></b><h3>Sistem Informasi Manajemen Aset</h3></center>

<hr>
<div class="w3-content">

  <form action="/krakatau/admin/dashboard.php">
  <div class="container">
    <label><b>Username</b></label>
    <input type="text" placeholder="Masukan Username" name="username" required>

    <label><b>Password</b></label>
    <input type="password" placeholder="Masukan Password" name="password" required>
        
    <button type="submit">Login</button>
    <hr>
    <div class="login-box-body">
        
        <p class="help-block label-danger" style="padding:5px;"><span class="pull-right glyphicon glyphicon-exclamation-sign"></span><center></center></p>
        <center><p>PT Krakatau Technology Information</p></center>
      </div>

      
    
  </div>
   </div>
    </div>

 
</form>
</div>
</body>
</html>
