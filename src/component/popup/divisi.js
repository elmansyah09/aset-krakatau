$(document).ready(function () {
    var main_popup_divisi = {
        load: {
            css: [

            ],

            js: [

            ],
            success: function () {
                main_popup_divisi.init();
                main_popup_divisi.listeners();
            }
        },
        cmp: {
            
        },
        init: function () {
            var me = this;                        
            
        },
        listeners: function () {
            var me = this;

            $("#modal-divisi").delegate(".btn-pilih","click", function(){                
                let selected = me.get_selected();                
                if (selected.length > 0){
                    let data = selected[0];                
                    $("#modal-divisi").trigger("onPilih",[data]);
                    $("#modal-divisi").modal('hide');
                } else {
                    swal({
                        title: "Informasi!",
                        text: "Tidak ada data yang dipilih",
                        icon: "info",
                    });
                }
                
            });

            $("#modal-divisi").on("load", function(){
                me.load_data();
            });

            $(".btn-action-cari").on("click",function(){
                main_popup_divisi.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    main_popup_divisi.start = 0;
                    me.load_data();
                }
            });
        },
        id: '',
        selectedRow: [],
        firstLoad: {
            data_list: false
        },
        gridApi: {
            data_list: false
        },
        start: 0,
        isLoad: false,
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data-popup-divisi tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val()                    
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data-popup-divisi",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                app.requestAjax(app.data.api_url+"/master/divisi/get_master",params,"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result.data;                        
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>                                        
                                        <td>` + item.kode_divisi + `</td>                                        
                                        <td>` + item.nama_divisi + `</td>                                        
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                            $('[aria-label="Page navigation"]').html(data.paging);
                        }
                    } else {
                        swal("",res.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Daftar Karyawan:data-list: ",err);
            }
        },
    };

    app.loader(main_popup_divisi);
});