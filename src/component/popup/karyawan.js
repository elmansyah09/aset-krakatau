$(document).ready(function () {
    var main_popup_karyawan = {
        load: {
            css: [

            ],

            js: [

            ],
            success: function () {
                main_popup_karyawan.init();
                main_popup_karyawan.listeners();
            }
        },
        cmp: {
            
        },
        init: function () {
            var me = this;                        
            
        },
        listeners: function () {
            var me = this;

            $("#modal-karyawan").delegate(".btn-pilih","click", function(){                
                let selected = me.get_selected();                
                if (selected.length > 0){
                    let data = selected[0];                
                    $("#modal-karyawan").trigger("onPilih",[data]);
                    $("#modal-karyawan").modal('hide');
                } else {
                    swal({
                        title: "Informasi!",
                        text: "Tidak ada data yang dipilih",
                        icon: "info",
                    });
                }
                
            });

            $("#modal-karyawan").on("load", function(){
                me.load_data();
            });

            $(".btn-action-cari").on("click",function(){
                main_popup_karyawan.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    main_popup_karyawan.start = 0;
                    me.load_data();
                }
            });
        },
        id: '',
        selectedRow: [],
        firstLoad: {
            data_list: false
        },
        gridApi: {
            data_list: false
        },
        start: 0,
        isLoad: false,
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data-popup-karyawan tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val()                    
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data-popup-karyawan",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                app.requestAjax(app.data.api_url+"/master/karyawan/get",params,"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        if("data" in result) {
                            result.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>
                                        <td>` + item.nik_karyawan + `</td>
                                        <td>` + item.nama_karyawan + `</td>                                        
                                        <td>` + item.kode_divisi + `</td>                                        
                                        <td>` + item.nama_divisi + `</td>
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                        }
                    } else {
                        swal("",res.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Daftar Karyawan:data-list: ",err);
            }
        },
    };

    app.loader(main_popup_karyawan);
});