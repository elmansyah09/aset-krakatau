$(document).ready(function() {
	 var form_divisi_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				form_divisi_page.init();
				form_divisi_page.listeners();
			}
		},		
		log_flag: 'Form Divisi',
		cmp:{						
			form_data: '#form-divisi',			
		},
		init:function() {
			var me = this;					
		
		},
		listeners:function() {
			var me = this;					
			$(".btn-kembali").on("click",function(){
				window.location.href = app.data.site_url + "/master/divisi"
			});			

			$(".btn-action-simpan").on('click', function (event) {
                event.preventDefault();                
				me.simpan();                
            });


			if (app.ifvalnull(app.data.segment[4],'') != '') {
				$('.field-tambah').addClass("hidden");
				$('.field-ubah').removeClass("hidden");
				try {
					let uuid = atob(app.data.segment[4]);
					me.load_form(uuid);
				} catch (err) {
					console.error(me.log_flag+":getuuid:",err);
				}
			} else {
				$('.field-ubah').addClass("hidden");
				$('.field-tambah').removeClass("hidden");
			}
		},	
		load_form(uuid){
			try {
				let me = this,
					params = {
						uuid: btoa(uuid)
					};
				app.requestAjax(app.data.api_url+"/master/divisi/get_detail",params,"POST",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
							let data = result.data;
							app.clear_form(me.cmp.form_data);								
							app.set_form_value(me.cmp.form_data, data);        							
                        }
                    } else {
                        swal("",result.message,'warning');
                    }
                });
			} catch (err) {
				console.error(me.log_flag+":loadData:",err);
			}
		},
		simpan: function () {
            var me = this,
                form = $(me.cmp.form_data);

            var params = new FormData(form[0]);

            app.body_mask();
            setTimeout(() => {                
                if (form.valid()) {
                    app.requestAjaxForm(app.data.api_url + "/master/divisi/simpan", params, "POST", function (result) {
                        if (result.code == 200) {
                            swal({
                                title: "Berhasil!",
                                text: result.message,
                                icon: "success",
                            }).then(function () {
                                window.location.href = app.data.site_url + "master/divisi";
                            });
                        } else {							
                            if(result.code == 245)  {								
								swal({
									title: "Informasi!",
									text: result.data,
									icon: "info",
								});
							} else {
								swal({
									title: "Informasi!",
									text: result.message,
									icon: "warning",
								});
							}
                        }
                    });
                } else {
                    swal({
                        title: "Peringatan!",
                        text: 'Isi kolom dengan benar!',
                        icon: "warning",
                    });
                    setTimeout(function () {
                        app.body_unmask();
                    }, 500);
                }
            }, 500);


        },
	};

	app.loader(form_divisi_page);
});