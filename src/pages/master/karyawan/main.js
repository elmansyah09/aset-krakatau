$(document).ready(function() {
	 var master_karyawan_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				master_karyawan_page.init();
				master_karyawan_page.listeners();
			}
		},		
		cmp:{						
			
		},
		init:function() {
			var me = this;		         
            me.load_data();            
		},
		listeners:function() {
			var me = this;						
            
			$(".btn-tambah").on("click",function(){
				window.location.href = app.data.site_url + "master/karyawan/tambah"
			});
			
            $("#daftar-data").delegate(".btn-action-ubah","click",function(){                                                
                let data = $(this).parents("tr").data();                
                window.location.href = app.data.site_url + "master/karyawan/ubah/" + btoa(data.id_karyawan)
			});
			
            $("#daftar-data").delegate(".btn-action-hapus","click",function(){                                                
                let data = $(this).parents("tr").data();
                me.hapus(data);
			});

            $(".btn-action-cari").on("click",function(){
                master_karyawan_page.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    master_karyawan_page.start = 0;
                    me.load_data();
                }
            });

            $('[aria-label="Page navigation"]').delegate('.pagination a', 'click', function(event) {
				event.preventDefault();
				master_karyawan_page.start = ($(this).attr('data-ci-pagination-page') == 1) ? 0 : $(this).attr('data-ci-pagination-page');
                app.body_mask();
				setTimeout(() => {                    
                    me.load_data();
                }, 100);
				// me.get_data_pejabat();
			});

		},		
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val(),
                    m: master_karyawan_page.start,
                    per_page:master_karyawan_page.start,
                    perPage:master_karyawan_page.start,
                    limit:10
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                app.requestAjax(app.data.api_url+"/master/karyawan/get_master",params,"GET",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        let data = result.data;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>
                                        <td>` + item.nik_karyawan + `</td>                                        
                                        <td>` + item.nama_karyawan + `</td>                                        
                                        <td>` + app.ifvalnull(item.nama_divisi,'-') + `</td> 
                                        <td>`  + 
                                            `<button type="button" class="btn btn-success btn-action-ubah">Ubah</button> ` +
                                            `<button type="button" class="btn btn-danger btn-action-hapus">Hapus</button>`
                                            +
                                        `</td>                                        
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                            $(".ft-count").html(data.count);
                            $('[aria-label="Page navigation"]').html(data.paging);
                        }
                    } else {
                        swal("",res.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Master Jenos Barang:data-list: ",err);
            }
        },
		hapus: function (args) {
            var me = this;            
            swal({
                title: "Hapus data?",
                icon: "warning",
                buttons: true,
                buttons: ["Batal", "Hapus"],
                dangerMode: true,
            })
                .then((isDelete) => {
                    if (isDelete) {
                        var params = {
                            uuid: args.id_karyawan,
                        }
                        app.body_mask();
                        app.requestAjax(app.data.api_url + "/master/karyawan/hapus", params, "POST", function (result) {
                            if (result.code == 200) {
                                me.load_data(true);
                                swal({
                                    title: "Berhasil!",
                                    text: result.message,
                                    icon: "success",
                                });
                            } else {
                                swal({
                                    title: "Informasi!",
                                    text: result.message,
                                    icon: "warning",
                                });
                            }
                            app.body_unmask();
                        });
                    }
                });
        },
	};

	app.loader(master_karyawan_page);
});