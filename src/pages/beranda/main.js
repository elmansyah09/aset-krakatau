$(document).ready(function() {
	 var beranda_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				beranda_page.init();
				beranda_page.listeners();
			}
		},		
		cmp:{						
			
		},
		init:function() {
			var me = this;			
			me.load_data_aset_keseluruhan();
			me.load_data_aset_bulanan();
			me.load_data_jenis_aset();
			me.load_data_kondisi_aset();
		},
		listeners:function() {
			var me = this;					

			

		},		
		load_data_aset_keseluruhan: function(){
            app.body_mask();
            try {
                let cmpid = ".dashboard-total-aset";
                app.requestAjax(app.data.api_url+"/dashboard/aset_keseluruhan",{},"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <h2>` + item.jml + `</h2>
                                `;
                                $(cmpid).html(row);                                
                            });
							
                        }
                        
                    } else {
                        swal("",result.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Aset Jenis:data-list: ",err);
            }
        },
		load_data_aset_bulanan: function(){
            app.body_mask();
            try {
                let cmpid1 = ".dashboard-total-aset-masuk";
				let cmpid2 = ".dashboard-total-aset-keluar";
                app.requestAjax(app.data.api_url+"/dashboard/aset_bulanan",{},"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row1 = `
                                    <h2>` + item.jml_aset_masuk + `</h2>
                                `;
								let row2 = `
                                    <h2>` + item.jml_aset_keluar + `</h2>
                                `;
                                $(cmpid1).html(row1);                                
								$(cmpid2).html(row2);           
                            });
							
                        }
                        
                    } else {
                        swal("",result.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Aset Jenis:data-list: ",err);
            }
        },
		load_data_jenis_aset: function(){
            app.body_mask();
            try {
                let cmpid = ".dashboard-jenis-barang";
                app.requestAjax(app.data.api_url+"/dashboard/aset_jenis",{},"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <div class="item-dashboard">
										<div class="card">        
											<div class="card-header d-flex align-items-center bg-secondary">
												<h3 class="h4" style="color:white;">` + item.nama_jenis_barang + `</h3>
											</div>
											<div class="card-body">
												<h2>` +item.jml + `</h2>
											</div>
										</div>
									</div>
                                `;
                                $(cmpid).append(row);                                
                            });
							
                        }
                        
                    } else {
                        swal("",result.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Aset Jenis:data-list: ",err);
            }
        },

		load_data_kondisi_aset: function(){
            app.body_mask();
            try {
                let cmpid = ".dashboard-kondisi-barang";
                app.requestAjax(app.data.api_url+"/dashboard/aset_kondisi",{},"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <div class="item-dashboard">
										<div class="card">        
											<div class="card-header d-flex align-items-center">
												<h3 class="h4">` + item.nama_kondisi_barang + `</h3>
											</div>
											<div class="card-body">
												<h2>` +item.jml + `</h2>
											</div>
										</div>
									</div>
                                `;
                                $(cmpid).append(row);                                
                            });
							
                        }
                        
                    } else {
                        swal("",result.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Aset Jenis:data-list: ",err);
            }
        },
	};

	app.loader(beranda_page);
});