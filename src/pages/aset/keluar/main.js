$(document).ready(function() {
	 var aset_keluar_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				aset_keluar_page.init();
				aset_keluar_page.listeners();
			}
		},		
		cmp:{						
			
		},
		init:function() {
			var me = this;		         
            me.load_data();
            app.get_data_list(".f-kondisi-barang",app.data.api_url+"/master/kondisibarang/get",{},{
                display_value: 'nama_kondisi_barang',
                value: 'id_kondisi_barang',
				placeholder:'Pilih Kondisi Barang',
                is_filter: true,
            });
		},
		listeners:function() {
			var me = this;						

            // $(".f-jenis-barang").on("change",function(){
            //     aset_keluar_page.start = 0;
            //     me.load_data();
            //     app.get_data_list(".f-tipe-barang",app.data.api_url+"/master/tipebarang/get",{
			// 		f_jenis_barang: $(".f-jenis-barang").val()
			// 	},{
			// 		display_value: 'nama_tipe_barang',
			// 		value: 'id_tipe_barang',
			// 		placeholder:'Pilih Tipe Barang'
			// 	});                
            // });

            $(".f-kondisi-barang").on("change",function(){
                aset_keluar_page.start = 0;
                me.load_data();
            });

			$(".btn-tambah").on("click",function(){
				window.location.href = app.data.site_url + "aset/keluar/tambah"
			});
			
            $("#daftar-data").delegate(".btn-action-ubah","click",function(){                                                
                let data = $(this).parents("tr").data();                
                window.location.href = app.data.site_url + "aset/keluar/ubah/" + btoa(data.id_aset_keluar)
			});
			
            $("#daftar-data").delegate(".btn-action-hapus","click",function(){                                                
                let data = $(this).parents("tr").data();
                me.hapus(data);
			});

            $(".btn-action-cari").on("click",function(){
                aset_keluar_page.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    aset_keluar_page.start = 0;
                    me.load_data();
                }
            });

		},		
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val(),                  
                    f_kondisi_barang: $(".f-kondisi-barang").val()
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                app.requestAjax(app.data.api_url+"/aset/asetkeluar/get",params,"GET",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
                            result.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>
                                        <td>` + item.nomor_seri + `</td>
                                        <td>` + item.nama_barang + `</td>                                        
                                        <td align="right">` + app.formatRupiah(item.harga_barang) + `</td>                                                                                
                                        <td align="right">` + app.formatRupiah(item.nilai_buku) + `</td>
                                        <td>` + item.nama_kondisi_barang + `</td>                                        
                                                   
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                        }
                    } else {
                        swal("",res.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Aset Keluar:data-list: ",err);
            }
        },
		hapus: function (args) {
            var me = this;            
            swal({
                title: "Hapus data?",
                icon: "warning",
                buttons: true,
                buttons: ["Batal", "Hapus"],
                dangerMode: true,
            })
                .then((isDelete) => {
                    if (isDelete) {
                        var params = {
                            uuid: args.id_aset_keluar,
                        }
                        app.body_mask();
                        app.requestAjax(app.data.api_url + "/aset/asetkeluar/hapus", params, "POST", function (result) {
                            if (result.code == 200) {
                                me.load_data(true);
                                swal({
                                    title: "Berhasil!",
                                    text: result.message,
                                    icon: "success",
                                });
                            } else {
                                swal({
                                    title: "Informasi!",
                                    text: result.message,
                                    icon: "warning",
                                });
                            }
                            app.body_unmask();
                        });
                    }
                });
        },
	};

	app.loader(aset_keluar_page);
});