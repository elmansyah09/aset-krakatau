$(document).ready(function() {
	 var form_aset_keluar_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				form_aset_keluar_page.init();
				form_aset_keluar_page.listeners();
			}
		},		
		log_flag: 'Form Aset Keluar',
		cmp:{						
			form_data: '#form-aset-keluar',			
		},
		init:function() {
			var me = this;						 
			app.get_data_list(".cb-kondisi-barang",app.data.api_url+"/master/kondisibarang/get",{},{
                display_value: 'nama_kondisi_barang',
                value: 'id_kondisi_barang',
				placeholder:'Pilih Kondisi Barang'
            });
		},
		listeners:function() {
			var me = this;					
			$(".btn-kembali").on("click",function(){
				window.location.href = app.data.site_url + "/aset/keluar"
			});			
			
			$(".btn-cari-aset").on('click', function (event) {
                event.preventDefault();                
				$("#modal-aset").trigger("load");
				$("#modal-aset").modal("show");
            });

			$("#modal-aset").on('onPilih', function(evt, data) {
                if(data) {					
                    $("[name=id_aset]").val(data.id_aset);
                    $("[name=nama_barang]").val(data.nama_barang);
					$("[name=nomor_seri]").val(data.nomor_seri);					
                }
            });

			$(".btn-action-simpan").on('click', function (event) {				
                event.preventDefault();                
				me.simpan();                
            });

			if (app.ifvalnull(app.data.segment[4],'') != '') {
				try {
					let uuid = atob(app.data.segment[4]);
					me.load_form(uuid);
				} catch (err) {
					console.error(me.log_flag+":getuuid:",err);
				}
			}
		},	
		load_form(uuid){
			try {
				let me = this,
					params = {
						uuid: btoa(uuid)
					};
				app.requestAjax(app.data.api_url+"/aset/asetkeluar/get_detail",params,"POST",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
							let data = result.data;
							app.clear_form(me.cmp.form_data);								
							app.set_form_value(me.cmp.form_data, data);        
							$(".cb-tipe-barang").val(data.id_tipe_barang).trigger('change.select2');
                        }
                    } else {
                        swal("",result.message,'warning');
                    }
                });
			} catch (err) {
				console.error(me.log_flag+":loadData:",err);
			}
		},
		simpan: function () {
            var me = this,
                form = $(me.cmp.form_data);

            var params = new FormData(form[0]);

            app.body_mask();
            setTimeout(() => {                
                if (form.valid()) {
                    app.requestAjaxForm(app.data.api_url + "/aset/asetkeluar/simpan", params, "POST", function (result) {
                        if (result.code == 200) {
                            swal({
                                title: "Berhasil!",
                                text: result.message,
                                icon: "success",
                            }).then(function () {
                                window.location.href = app.data.site_url + "aset/keluar/";
                            });
                        } else {
                            swal({
                                title: "Informasi!",
                                text: result.message,
                                icon: "warning",
                            });
                        }
                    });
                } else {
                    swal({
                        title: "Peringatan!",
                        text: 'Isi kolom dengan benar!',
                        icon: "warning",
                    });
                    setTimeout(function () {
                        app.body_unmask();
                    }, 500);
                }
            }, 500);


        },
	};

	app.loader(form_aset_keluar_page);
});