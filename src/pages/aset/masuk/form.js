$(document).ready(function() {
	 var form_aset_masuk_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				form_aset_masuk_page.init();
				form_aset_masuk_page.listeners();
			}
		},		
		log_flag: 'Form Aset Masuk',
		cmp:{						
			form_data: '#form-aset-masuk',			
		},
		init:function() {
			var me = this;					
			app.get_data_list(".cb-jenis-barang",app.data.api_url+"/master/jenisbarang/get",{},{
                display_value: 'nama_jenis_barang',
                value: 'id_jenis_barang',
				placeholder:'Pilih Jenis Barang'
            });

			app.get_data_list(".cb-kondisi-barang",app.data.api_url+"/master/kondisibarang/get",{},{
                display_value: 'nama_kondisi_barang',
                value: 'id_kondisi_barang',
				placeholder:'Pilih kondisi Barang'
            });
		},
		listeners:function() {
			var me = this;					
			$(".btn-kembali").on("click",function(){
				window.location.href = app.data.site_url + "/aset/masuk"
			});			

			$(".btn-action-simpan").on('click', function (event) {
                event.preventDefault();                
				me.simpan();                
            });

			$(".btn-cari-karyawan").on('click', function (event) {
                event.preventDefault();                
				$("#modal-karyawan").trigger("load");
				$("#modal-karyawan").modal("show");
            });

			$("#modal-karyawan").on('onPilih', function(evt, data) {
                if(data) {					
                    $("[name=id_penanggung_jawab]").val(data.id_karyawan);
                    $("[name=nama_penanggung_jawab]").val(data.nama_karyawan);					
                }
            });

			$(".cb-jenis-barang").on("change",function(){
                app.get_data_list(".cb-tipe-barang",app.data.api_url+"/master/tipebarang/get",{
					f_jenis_barang: $(".cb-jenis-barang").val()
				},{
					display_value: 'nama_tipe_barang',
					value: 'id_tipe_barang',
					placeholder:'Pilih Tipe Barang'
				});
            });

			if (app.ifvalnull(app.data.segment[4],'') != '') {
				$('.field-tambah').addClass("hidden");
				$('.field-ubah').removeClass("hidden");
				try {
					let uuid = atob(app.data.segment[4]);
					me.load_form(uuid);
				} catch (err) {
					console.error(me.log_flag+":getuuid:",err);
				}
			} else {
				$('.field-ubah').addClass("hidden");
				$('.field-tambah').removeClass("hidden");
			}
		},	
		load_form(uuid){
			try {
				let me = this,
					params = {
						uuid: btoa(uuid)
					};
				app.requestAjax(app.data.api_url+"/aset/asetmasuk/get_detail",params,"POST",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
							let data = result.data;
							app.clear_form(me.cmp.form_data);								
							app.set_form_value(me.cmp.form_data, data);        
							setTimeout(() => {								
								$(".cb-jenis-barang").val(data.id_jenis_barang).trigger('change.select2');
								setTimeout(() => {								
									$(".cb-kondisi-barang").val(data.id_kondisi_barang).trigger('change.select2');
									$(".cb-tipe-barang").val(data.id_tipe_barang).trigger('change.select2');
								}, 500);
							}, 500);
                        }
                    } else {
                        swal("",result.message,'warning');
                    }
                });
			} catch (err) {
				console.error(me.log_flag+":loadData:",err);
			}
		},
		simpan: function () {
            var me = this,
                form = $(me.cmp.form_data);

            var params = new FormData(form[0]);

            app.body_mask();
            setTimeout(() => {                
                if (form.valid()) {
                    app.requestAjaxForm(app.data.api_url + "/aset/asetmasuk/simpan", params, "POST", function (result) {
                        if (result.code == 200) {
                            swal({
                                title: "Berhasil!",
                                text: result.message,
                                icon: "success",
                            }).then(function () {
                                window.location.href = app.data.site_url + "/aset/masuk/";
                            });
                        } else {
                            swal({
                                title: "Informasi!",
                                text: result.message,
                                icon: "warning",
                            });
                        }
                    });
                } else {
                    swal({
                        title: "Peringatan!",
                        text: 'Isi kolom dengan benar!',
                        icon: "warning",
                    });
                    setTimeout(function () {
                        app.body_unmask();
                    }, 500);
                }
            }, 500);


        },
	};

	app.loader(form_aset_masuk_page);
});