$(document).ready(function() {
	 var login_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				login_page.init();
				login_page.listeners();
			}
		},		
		cmp:{						
			form_ubah_sandi: "#form-ubah-sandi"
		},
		init:function() {
			var me = this;			
		},
		listeners:function() {
			var me = this;					

			$('.btn-ganti-sandi').on('click', function(event) {
				event.preventDefault();
				$("#modal-ubah-sandi").find("[name='userid']").val(app.data.user.userid);
				$("#modal-ubah-sandi").modal("show");
			});
			
			$("#modal-ubah-sandi").on('click', '.btn-simpan', function(event) {
				event.preventDefault();
				me.ganti_sandi();
			});

		},		
		id: '',
		selectedRow:[],
		firstLoad:{
			data_list:false
		},
		gridApi:{
			data_list:false
		},
		start:0,
		isLoad: false,		        
		ganti_sandi: function() {
			var me = this,
				form = $(me.cmp.form_ubah_sandi),						
				formData = form.serializeArray(),
				params = app.convert_form(formData);
						
			app.body_mask();
			if (form.valid()) {
				app.requestAjax(app.data.api_url + "/Authentication/change_password", params, "POST", function(result) {
					if (result.success) {						
						$("#modal-ubah-sandi").modal("hide");
						swal({
							title: "Berhasil!",
							text: result.msg,
							icon: "success",
						}).then(function() {
							window.location = app.data.base_admin;
						});
					} else {
						swal({
							title: "Informasi!",
							text: result.msg,
							icon: "warning",
						});
					}
				});
			} else {
				swal({
					title: "Peringatan!",
					text: 'Isi kolom dengan benar!',
					icon: "warning",
				});
				setTimeout(function() {
					app.body_unmask();
				}, 500);
			}
		},
	};

	app.loader(login_page);
});