$(document).ready(function() {
	 var login_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				login_page.init();
				login_page.listeners();
			}
		},		
		cmp:{			
			form_login: "#form-login",
		},
		init:function() {
			var me = this;			
			localStorage.setItem("token_sipoyan", "");
		},
		listeners:function() {
			var me = this;					

			$("#form-login").on('click', '.btn-action-login', function(event) {
				event.preventDefault();
				me.doLogin();
			});
			

		},		
		id: '',
		selectedRow:[],
		firstLoad:{
			data_list:false
		},
		gridApi:{
			data_list:false
		},
		start:0,
		isLoad: false,		
        doLogin: function() {
			localStorage.setItem("token_sipoyan", "");
			var me = this,
				form = $(me.cmp.form_login),
				params = {
					username: form.find("[name='username']").val(),
					password: form.find("[name='password']").val()
				}
						
			app.body_mask();
			if (form.valid()) {
				app.requestAjax(app.data.api_url + "/Authentication/login", params, "POST", function(result) {
					if (result.success) {						
						if (result.token) {
							app.requestAjax(app.data.api_url + "/Authentication/initToken", {token:result.token}, "POST", function(result_token) {
								swal({
									title: "Berhasil!",
									text: result.msg,
									icon: "success",
								}).then(function() {
									localStorage.setItem("token_sipoyan", result.token);
									window.location = app.data.base_main;
								});
							})
						}
					} else {
						swal({
							title: "Informasi!",
							text: result.msg,
							icon: "warning",
						});
					}
				});
			} else {
				swal({
					title: "Peringatan!",
					text: 'Isi kolom dengan benar!',
					icon: "warning",
				});
				setTimeout(function() {
					app.body_unmask();
				}, 500);
			}


		},
	};

	app.loader(login_page);
});