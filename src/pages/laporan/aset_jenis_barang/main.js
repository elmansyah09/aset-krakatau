$(document).ready(function() {
	 var aset_jenis_barang_laporan_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				aset_jenis_barang_laporan_page.init();
				aset_jenis_barang_laporan_page.listeners();
			}
		},		
		cmp:{						
			
		},
		init:function() {
			var me = this;		         
            me.load_data();
            app.get_data_list(".f-jenis-barang",app.data.api_url+"/master/jenisbarang/get",{},{
                display_value: 'nama_jenis_barang',
                value: 'id_jenis_barang',
				placeholder:'Pilih Jenis Barang',
                is_filter: true
            });
		},
		listeners:function() {
			var me = this;						

            $(".f-jenis-barang").on("change",function(){
                aset_jenis_barang_laporan_page.start = 0;
                me.load_data();
                // app.get_data_list(".f-tipe-barang",app.data.api_url+"/master/tipebarang/get",{
				// 	f_jenis_barang: $(".f-jenis-barang").val()
				// },{
				// 	display_value: 'nama_tipe_barang',
				// 	value: 'id_tipe_barang',
				// 	placeholder:'Pilih Tipe Barang'
				// });                
            });

            $(".f-tipe-barang").on("change",function(){
                aset_jenis_barang_laporan_page.start = 0;
                me.load_data();
            });

			$(".btn-action-cetak").on("click",function(){				
                window.open(app.data.api_url+"/laporan/asetjenisbarang/cetak","_blank");
			});
			
            $("#daftar-data").delegate(".btn-action-ubah","click",function(){                                                
                let data = $(this).parents("tr").data();                
                window.location.href = app.data.site_url + "aset/masuk/ubah/" + btoa(data.id_aset)
			});
			
            $("#daftar-data").delegate(".btn-action-hapus","click",function(){                                                
                let data = $(this).parents("tr").data();
                me.hapus(data);
			});

            $(".btn-action-cari").on("click",function(){
                aset_jenis_barang_laporan_page.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    aset_jenis_barang_laporan_page.start = 0;
                    me.load_data();
                }
            });

            $('[aria-label="Page navigation"]').delegate('.pagination a', 'click', function(event) {
				event.preventDefault();
				aset_jenis_barang_laporan_page.start = ($(this).attr('data-ci-pagination-page') == 1) ? 0 : $(this).attr('data-ci-pagination-page');
                app.body_mask();
				setTimeout(() => {                    
                    me.load_data();
                }, 100);
				// me.get_data_pejabat();
			});

		},		
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val(),                  
                    f_jenis_barang: $(".f-jenis-barang").val(),
                    f_tipe_barang: $(".f-tipe-barang").val(),
                    m: aset_jenis_barang_laporan_page.start,
                    per_page:aset_jenis_barang_laporan_page.start,
                    perPage:aset_jenis_barang_laporan_page.start,
                    limit:10
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                $(".ft-count").html('0');
                $('[aria-label="Page navigation"]').html('');
                app.requestAjax(app.data.api_url+"/laporan/asetaktiva/get",params,"GET",function(result) {
                    app.body_unmask();                    
                    if (result.code == 200) {
                        let data = result.data;
                        if("data" in data) {
                            data.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>
                                        <td>` + item.nomor_seri + `</td>
                                        <td>` + item.nama_barang + `</td>                                        
                                        <td>` + item.nama_jenis_barang + `</td>
                                        <td align="center">` + item.tgl_pembelian + `</td>
                                        <td align="right">` + app.formatRupiah(item.harga_barang) + `</td>
                                        <td align="center">` + item.jangka_waktu + `</td>                                                                                
                                        <td align="right">` + app.formatRupiah(Math.round(item.harga_barang/item.jangka_waktu)+'') + `</td>
                                        <td align="right">` + app.formatRupiah(item.nilai_buku) + `</td>
                                        <td align="center">` + item.nama_penanggung_jawab + `</td>
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                        }
                        $(".ft-count").html(data.count);
                        $('[aria-label="Page navigation"]').html(data.paging);
                    } else {
                        swal("",result.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Posyandu Aset:data-list: ",err);
            }
        },
		hapus: function (args) {
            var me = this;            
            swal({
                title: "Hapus data?",
                icon: "warning",
                buttons: true,
                buttons: ["Batal", "Hapus"],
                dangerMode: true,
            })
                .then((isDelete) => {
                    if (isDelete) {
                        var params = {
                            uuid: args.id_aset,
                        }
                        app.body_mask();
                        app.requestAjax(app.data.api_url + "/aset/asetmasuk/hapus", params, "POST", function (result) {
                            if (result.code == 200) {
                                me.load_data(true);
                                swal({
                                    title: "Berhasil!",
                                    text: result.message,
                                    icon: "success",
                                });
                            } else {
                                swal({
                                    title: "Informasi!",
                                    text: result.message,
                                    icon: "warning",
                                });
                            }
                            app.body_unmask();
                        });
                    }
                });
        },
	};

	app.loader(aset_jenis_barang_laporan_page);
});