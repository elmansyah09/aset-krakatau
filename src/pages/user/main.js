$(document).ready(function() {
	 var pengguna_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				pengguna_page.init();
				pengguna_page.listeners();
			}
		},		
		cmp:{						
			
		},
		init:function() {
			var me = this;		         
            me.load_data();
		},
		listeners:function() {
			var me = this;	
					
			$(".btn-tambah").on("click",function(){
				window.location.href = app.data.site_url + "user/tambah"
			});

			$("#daftar-data").delegate(".btn-action-ubah","click",function(){                                                
                let data = $(this).parents("tr").data();                
                window.location.href = app.data.site_url + "user/ubah/" + btoa(data.id_admin)
			});

            $("#daftar-data").delegate(".btn-action-ubah-password","click",function(){                                                
                let data = $(this).parents("tr").data();                
                window.location.href = app.data.site_url + "user/ubahpassword/" + btoa(data.id_admin)
			});
			
            $("#daftar-data").delegate(".btn-action-hapus","click",function(){                                                
                let data = $(this).parents("tr").data();
                me.hapus(data);
			});

            $(".btn-action-cari").on("click",function(){
                pengguna_page.start = 0;
                me.load_data();
            });
            
            $(".f-search").on('keyup', function (e) {
                if (e.keyCode == 13) {
                    pengguna_page.start = 0;
                    me.load_data();
                }
            });

		},		
        get_selected: function() {
            let row = [],
                rowAktif = $("#daftar-data tbody tr.active");
            if(rowAktif.length > 0) {
                row.push(rowAktif.data());
            }
            return row;
        },
        get_params:function() {
            let me = this,
                params = {
                    f_search: $(".f-search").val()                    
                };
            return params;
        },
        load_data: function(){
            app.body_mask();
            try {
                 var me = this,
                    cmpid = "#daftar-data",                    
                    params = me.get_params();

                $(cmpid).find('tbody').html('');
                app.requestAjax(app.data.api_url+"/user/app/get",params,"GET",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
                            result.data.forEach(item => {
                                let row = `
                                    <tr>
                                        <td>` + item.no + `</td>
                                        <td>` + item.nama_karyawan + `</td>
                                        <td>` + item.username + `</td>
                                        <td align="center">` + item.nama_level + `</td>                                        
                                        <td>`  + 
                                            `<button type="button" class="btn btn-success btn-action-ubah">Ubah</button> ` +
                                            `<button type="button" class="btn btn-info btn-action-ubah-password">Ubah Password</button> ` +
                                            `<button type="button" class="btn btn-danger btn-action-hapus">Hapus</button>`
                                            +
                                        `</td>     
                                    </tr>
                                `;
                                $(cmpid).find('tbody').append(row);
                                let lastRow = $(cmpid).find('tbody tr:last');
                                lastRow.data(item);
                            });
                        }
                    } else {
                        swal("",res.message,'warning');
                    }
                });
            } catch (err) {
                app.body_unmask();            
                console.error("Posyandu Aset:data-list: ",err);
            }
        },
		hapus: function (args) {
            var me = this;            
            swal({
                title: "Hapus data?",
                icon: "warning",
                buttons: true,
                buttons: ["Batal", "Hapus"],
                dangerMode: true,
            })
                .then((isDelete) => {
                    if (isDelete) {
                        var params = {
                            uuid: args.id_admin,
                        }
                        app.body_mask();
                        app.requestAjax(app.data.api_url + "/user/app/hapus", params, "POST", function (result) {
                            if (result.code == 200) {
                                me.load_data(true);
                                swal({
                                    title: "Berhasil!",
                                    text: result.message,
                                    icon: "success",
                                });
                            } else {
                                swal({
                                    title: "Informasi!",
                                    text: result.message,
                                    icon: "warning",
                                });
                            }
                            app.body_unmask();
                        });
                    }
                });
        },
	};

	app.loader(pengguna_page);
});