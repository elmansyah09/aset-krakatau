$(document).ready(function() {
	 var form_user_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				form_user_page.init();
				form_user_page.listeners();
			}
		},		
		log_flag: 'Form User',
		cmp:{						
			form_data: '#form-user',			
		},
		init:function() {
			var me = this;						 
		},
		listeners:function() {
			var me = this;					
			$(".btn-kembali").on("click",function(){
				window.location.href = app.data.site_url + "/user"
			});			

			$(".btn-action-simpan").on('click', function (event) {
                event.preventDefault();                
				me.simpan();                
            });

			$(".btn-cari-karyawan").on('click', function (event) {
                event.preventDefault();                
				$("#modal-karyawan").trigger("load");
				$("#modal-karyawan").modal("show");
            });

			$("#modal-karyawan").on('onPilih', function(evt, data) {
                if(data) {					
                    $("[name=id_karyawan]").val(data.id_karyawan);
                    $("[name=nama_karyawan]").val(data.nama_karyawan);					
                }
            });

			if (app.ifvalnull(app.data.segment[3],'') != '') {
				try {
					let uuid = atob(app.data.segment[3]);
					me.load_form(uuid);
				} catch (err) {
					console.error(me.log_flag+":getuuid:",err);
				}
			}
		},	
		load_form(uuid){
			try {
				let me = this,
					params = {
						uuid: btoa(uuid)
					};
				app.requestAjax(app.data.api_url+"/user/app/get_detail",params,"POST",function(result) {
                    app.body_unmask();
                    if (result.code == 200) {
                        if("data" in result) {
							let data = result.data;
							app.clear_form(me.cmp.form_data);								
							app.set_form_value(me.cmp.form_data, data);        
							// $(".cb-posyandu").val(data.id_posyandu).trigger('change.select2');
                        }
                    } else {
                        swal("",result.message,'warning');
                    }
                });
			} catch (err) {
				console.error(me.log_flag+":loadData:",err);
			}
		},
		simpan: function () {
            var me = this,
                form = $(me.cmp.form_data);

            var params = new FormData(form[0]);
			
			if(form.attr("tipe") == "ganti_password") {
				params.append("is_ganti_password",1);
			}
            app.body_mask();
            setTimeout(() => {                
                if (form.valid()) {
                    app.requestAjaxForm(app.data.api_url + "/user/app/simpan", params, "POST", function (result) {
                        if (result.code == 200) {
                            swal({
                                title: "Berhasil!",
                                text: result.message,
                                icon: "success",
                            }).then(function () {
                                window.location.href = app.data.site_url + "/user";
                            });
                        } else {
                            if(result.code == 245)  {								
								swal({
									title: "Informasi!",
									text: result.data,
									icon: "info",
								});
							} else {
								swal({
									title: "Informasi!",
									text: result.message,
									icon: "warning",
								});
							}
                        }
                    });
                } else {
                    swal({
                        title: "Peringatan!",
                        text: 'Isi kolom dengan benar!',
                        icon: "warning",
                    });
                    setTimeout(function () {
                        app.body_unmask();
                    }, 500);
                }
            }, 500);


        },
	};

	app.loader(form_user_page);
});