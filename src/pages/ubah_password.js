$(document).ready(function() {
	 var form_ubah_password_page = {
		load: {
			css: [

			],
			
			js: [

			],
			success: function() {
				form_ubah_password_page.init();
				form_ubah_password_page.listeners();
			}
		},		
		log_flag: 'Form Ubah Password',
		cmp:{						
			form_data: '#form-ubah-password',			
		},
		init:function() {
			var me = this;						 
		},
		listeners:function() {
			var me = this;					
			$(".btn-action-simpan").on('click', function (event) {
                event.preventDefault();                
				me.simpan();                
            });
			
		},		
		simpan: function () {
            var me = this,
                form = $(me.cmp.form_data);

            var params = new FormData(form[0]);
			
            app.body_mask();
            setTimeout(() => {                
                if (form.valid()) {
                    app.requestAjaxForm(app.data.api_url + "/user/app/ubah_password", params, "POST", function (result) {
                        if (result.code == 200) {
                            swal({
                                title: "Berhasil!",
                                text: result.message,
                                icon: "success",
                            }).then(function () {
                                window.location.href = app.data.site_url;
                            });
                        } else {
                            if(result.code == 245)  {								
								swal({
									title: "Informasi!",
									text: result.data,
									icon: "info",
								});
							} else {
								swal({
									title: "Informasi!",
									text: result.message,
									icon: "warning",
								});
							}
                        }
                    });
                } else {
                    swal({
                        title: "Peringatan!",
                        text: 'Isi kolom dengan benar!',
                        icon: "warning",
                    });
                    setTimeout(function () {
                        app.body_unmask();
                    }, 500);
                }
            }, 500);


        },
	};

	app.loader(form_ubah_password_page);
});