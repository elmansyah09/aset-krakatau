<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Template {
	
	function __construct(){
		$this->_ci = &get_instance();
	}
	
	function view($view,$data=NULL,$folder = "section",$header = "_header"){
	
		if(!isset($data['title'])) $data['title'] = NULL;
		
		$data['_header'] = $this->_ci->load->view($folder."/_header",$data,TRUE);
		
		if(is_array($view)){
			$data['_content'] = array();
			foreach ($view as $t){
				array_push($data['_content'],$this->_ci->load->view($t,$data,TRUE));
			}
		} else {
			if($view !== '') $data['_content'] = $this->_ci->load->view($view,$data,TRUE);
			else $data['_content'] = "";
		}
		$data['_footer'] = $this->_ci->load->view($folder.'/_footer',$data,TRUE);

		$this->_ci->load->view('template',$data,FALSE);
	}

	function get_data_user(){
		$data = $this->_ci->session->all_userdata();
		unset($data['password']);
		return $data;
	}
}