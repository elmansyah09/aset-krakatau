<?php 

/**
 * Elmansyah Fauzi Rachman
 */
class Master extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);                
	}

	public function jenisbarang($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Jenis Barang",
			"sub_title" => "Daftar Jenis Barang",
			"menu_aktif" => "master",
			"sub_menu_aktif" => "jenis_barang",
		);
		if($type == "daftar") {
			$this->template->view("pages/master/jenis_barang/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Jenis Barang";
			$this->template->view("pages/master/jenis_barang/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Jenis Barang";
			$this->template->view("pages/master/jenis_barang/form",$data);
		} else {
			show_404();
		}
	}
      
	public function tipebarang($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Tipe Barang",
			"sub_title" => "Daftar Tipe Barang",
			"menu_aktif" => "master",
			"sub_menu_aktif" => "tipe_barang",
		);
		if($type == "daftar") {
			$this->template->view("pages/master/tipe_barang/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Tipe Barang";
			$this->template->view("pages/master/tipe_barang/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Tipe Barang";
			$this->template->view("pages/master/tipe_barang/form",$data);
		} else {
			show_404();
		}
	}

	public function kondisi($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Kondisi Barang",
			"sub_title" => "Daftar Kondisi Barang",
			"menu_aktif" => "master",
			"sub_menu_aktif" => "kondisi_barang",
		);
		if($type == "daftar") {
			$this->template->view("pages/master/kondisi/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Kondisi Barang";
			$this->template->view("pages/master/kondisi/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Kondisi Barang";
			$this->template->view("pages/master/kondisi/form",$data);
		} else {
			show_404();
		}
	}

	public function karyawan($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Karyawan",
			"sub_title" => "Daftar Karyawan",
			"menu_aktif" => "master",
			"sub_menu_aktif" => "karyawan",
		);
		if($type == "daftar") {
			$this->template->view("pages/master/karyawan/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Karyawan";
			$this->template->view("pages/master/karyawan/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Karyawan";
			$this->template->view("pages/master/karyawan/form",$data);
		} else {
			show_404();
		}
	}

	public function divisi($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Divisi",
			"sub_title" => "Daftar Divisi",
			"menu_aktif" => "master",
			"sub_menu_aktif" => "divisi",
		);
		if($type == "daftar") {
			$this->template->view("pages/master/divisi/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Divisi";
			$this->template->view("pages/master/divisi/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Divisi";
			$this->template->view("pages/master/divisi/form",$data);
		} else {
			show_404();
		}
	}
	
}