<?php 

/**
 * 
 */
class Aset extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"auth_method" => ":ALL"	
		);
		parent::__construct($config);
	}

	public function masuk($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Aset Masuk",
			"sub_title" => "Daftar Aset Masuk",
			"menu_aktif" => "aset_masuk",
		);
		if($type == "daftar") {
			$this->template->view("pages/aset/masuk/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Aset Masuk";
			$this->template->view("pages/aset/masuk/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Aset Masuk";
			$this->template->view("pages/aset/masuk/form",$data);
		} else {
			show_404();
		}
	}

	public function keluar($type = "daftar")
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data Aset Keluar",
			"sub_title" => "Daftar Aset Keluar",
			"menu_aktif" => "aset_keluar",
		);
		if($type == "daftar") {
			$this->template->view("pages/aset/keluar/main",$data);
		} else if($type == "tambah") {
			$data["sub_title"] = "Tambah Data Aset Keluar";
			$this->template->view("pages/aset/keluar/form",$data);
		} else if($type == "ubah") {
			$data["sub_title"] = "Ubah Data Aset Keluar";
			$this->template->view("pages/aset/keluar/form",$data);
		} else {
			show_404();
		}
	}

}