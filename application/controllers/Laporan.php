<?php 

/**
 * Elmansyah Fauzi Rachman
 */
class Laporan extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"auth_method" => ":ALL"			
		);
		parent::__construct($config);                
	}

	public function asettetap()
	{
		$data = array(
            "data_app" => $this->get_data_app(),            
			"title_page" => "Laporan Aset Tetap",
			"sub_title" => "Laporan Aset Tetap",
			"menu_aktif" => "laporan",
			"sub_menu_aktif" => "aset_tetap",
		);
		$this->template->view("pages/laporan/aset_tetap/main",$data);		
	}
      
	public function asetdijual()
	{
		$data = array(
            "data_app" => $this->get_data_app(),            
			"title_page" => "Laporan Aset Dijual",
			"sub_title" => "Laporan Aset Dijual",
			"menu_aktif" => "laporan",
			"sub_menu_aktif" => "aset_dijual",
		);
		$this->template->view("pages/laporan/aset_dijual/main",$data);		
	}

	public function asetjenis()
	{
		$data = array(
            "data_app" => $this->get_data_app(),            
			"title_page" => "Laporan Aset Per Jenis Barang",
			"sub_title" => "Laporan Aset Per Jenis Barang",
			"menu_aktif" => "laporan",
			"sub_menu_aktif" => "aset_jenis",
		);
		$this->template->view("pages/laporan/aset_jenis_barang/main",$data);		
	}

	public function asetkondisi()
	{
		$data = array(
            "data_app" => $this->get_data_app(),            
			"title_page" => "Laporan Aset Per Kondisi Barang",
			"sub_title" => "Laporan Aset Per Kondisi Barang",
			"menu_aktif" => "laporan",
			"sub_menu_aktif" => "aset_kondisi",
		);
		$this->template->view("pages/laporan/aset_kondisi_barang/main",$data);		
	}
	
}