<?php 

/**
 * 
 */
class Main extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"auth_method" => ":ALL"	
		);
		parent::__construct($config);
	}

	public function index()
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Dashboard"
		);
		$this->template->view("pages/beranda/main",$data);
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function ubahpassword()
	{
		$data = array(
            "data_app" => $this->get_data_app(),			
		);
		$this->template->view("pages/ubah_password",$data);
	}
	
}