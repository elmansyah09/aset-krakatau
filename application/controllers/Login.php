<?php 

/**
 * 
 */
class Login extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"allow_origin" => true			
		);
		parent::__construct($config);		
	}

	public function index()
	{				
		$data = array(
			"data_app" => $this->get_data_app(),
		);		
		if($this->session->userdata("token")) {
			redirect("main");
		} else {
			$this->load->view("pages/login",$data);
		}
	}
}