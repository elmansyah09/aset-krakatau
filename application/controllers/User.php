<?php 

/**
 * 
 */
class User extends EFR_Controller
{
	
	function __construct()
	{
		
		$config = array(
			"auth_method" => ":ALL"
		);
		parent::__construct($config);
	}

	public function index()
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data User",
			"sub_title" => "Daftar User",
			"menu_aktif" => "user",
		);
		$this->template->view("pages/user/main",$data);		
	}

	public function tambah()
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data User",
			"sub_title" => "Tambah Data User",
			"menu_aktif" => "user",
		);
		$this->template->view("pages/user/form",$data);		
	}

	public function ubah($id)
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data User",
			"sub_title" => "Ubah Data User",
			"menu_aktif" => "user",
		);
		$this->template->view("pages/user/form_ubah",$data);		
	}

	public function ubahpassword($id)
	{
		$data = array(
            "data_app" => $this->get_data_app(),
			"title_page" => "Data User",
			"sub_title" => "Ubah Password User",
			"menu_aktif" => "user",
		);
		$this->template->view("pages/user/form_ganti_password",$data);		
	}

}