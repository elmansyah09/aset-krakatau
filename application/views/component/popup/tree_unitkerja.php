<!-- modal tree unitkerja -->
<div class="modal fade main-modal modal-form" id="modal-tree-unitkerja">
    <div class="modal-dialog"> 
      <div class="modal-content">
        <div class="modal-header">          
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size:34px;">&times;</span></button>
            <h3 class="modal-title text-color-gray">Unitkerja</h3>
        </div>
        <div class="modal-body">
          <div style="height:400px;overflow:auto;">
            <ul id="cmpTreeUnitkerja" class="tree-base tree-no-icon"></ul>
          </div>
        </div>
        <div class="modal-footer" style="padding-bottom: 0px;padding-top: 0px">          
            <div class="row">                
                <div class="col-xs-6 no-padding-side">
                  <button type="button" class="btn btn-flat btn-block" data-dismiss="modal">Batal</button>
                </div>
                <div class="col-xs-6 no-padding-side">
                  <button type="button" class="btn btn-dark-2 btn-flat btn-block btn-pilih" id="btn-pilih">Pilih</button>
                </div>          
            </div>
        </div>
      </div>
    </div>
</div>
<!-- tutup modal tree unitkerja -->

<script src="<?php echo config_item("url_src_component") ?>popup/tree_unitkerja.js"></script>