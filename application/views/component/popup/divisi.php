<!-- modal pejabat penetap -->
<div class="modal main-modal modal-form" id="modal-divisi">
    <div class="modal-dialog modal-lg"> 
      <div class="modal-content">
        <div class="modal-header">          
            <h3 class="modal-title text-color-gray">Divisi</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size:34px;">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-6">
              <div class="input-group">
                  <input name="f_search" class="form-control input-lg f-search" placeholder="" type="text">
                  <span class="btn input-group-addon btn-action-cari">Cari</span>
              </div>
            </div>
          </div>          
          <div style="height:400px;overflow:auto;">            
          <br>
            <table id="daftar-data-popup-divisi" class="table divisi-table popup-table">
              <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Divsi</th>
                    <th>Kode Divisi</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
            <div class="row">              
                  <div class="col-md-7">
                    <nav aria-label="Page navigation" id="nav" style="padding: 0 10px;"></nav>
                  </div>
                  <div class="col-md-5" style="text-align:right;">
                           
                  </div>              
            </div>
          </div>
        </div>
        <div class="modal-footer" style="padding-bottom: 0px;padding-top: 0px">          
          <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary btn-block btn-pilih" id="btn-pilih" style="margin-top:0px;">Pilih</button>
            <!-- <div class="row">                
                <div class="col-xs-6 no-padding-side">
                  <button type="button" class="btn btn-secondary btn-flat btn-block" data-dismiss="modal">Batal</button>
                </div>
                <div class="col-xs-6 no-padding-side">
                  <button type="button" class="btn btn-primary btn-flat btn-block btn-pilih" id="btn-pilih">Pilih</button>
                </div>          
            </div> -->
        </div>
      </div>
    </div>
</div>
<!-- tutup modal pejabat penetap -->

<script src="<?php echo config_item("url_src_component") ?>popup/divisi.js"></script>