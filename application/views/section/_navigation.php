    <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <!-- Search Box-->
        
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="<?php echo site_url(); ?>" class="navbar-brand">
                  <div class="brand-text brand-big"><strong>Sistem Informasi Manajemen Aset</strong></div>
                  
                <!-- Toggle Button--><a id="toggle-btn"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <li class="nav-item"><a href="<?php echo site_url("main/ubahpassword"); ?>" class="nav-link">Ubah Password</i></a></li>
                |
                <!-- Logout    -->
                <li class="nav-item"><a href="<?php echo site_url("api.php/authentication/logout")?>" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
              </ul>
             </div>
          </div>
        </nav>
      </header>


      <div class="page-content d-flex align-items-stretch">
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar"><img src="<?php echo config_item('url_asset'); ?>/template/image/avatar.png" class="img-fluid rounded-circle"></div>
            <div class="title">
              <h1 class="h4"><?php echo ifunsetempty($data_app["user"],"nama_karyawan","-"); ?></h1>
              <p><?php echo ifunsetempty($data_app["user"],"nama_usergroup","-"); ?></p>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Menu</span>
          <?php
          $menuActive = isset($menu_aktif)?$menu_aktif:'';
          $subMenuActive = isset($sub_menu_aktif)?$sub_menu_aktif:'';          
          
          ?>
          <ul class="list-unstyled">
            <li class="<?php echo ($menuActive==''?'active':''); ?>"> <a href="<?php echo base_url(); ?>"><i class="icon-home"></i>Dashboard</a></li>
            <!-- <li class="<?php echo ($menuActive=='profil'?'active':''); ?>"> <a href='<?php echo base_url('profil'); ?>'><i class='fa fa-user-o'></i>Profil Pengguna</a></li> -->
            <li class="<?php echo ($menuActive=='aset_masuk'?'active':''); ?>"> <a href='<?php echo base_url('aset/masuk'); ?>'><i class="fa fa-files-o"></i>Data Aset Masuk</a></li>
            <li class="<?php echo ($menuActive=='aset_keluar'?'active':''); ?>"> <a href='<?php echo base_url('aset/keluar'); ?>'><i class="fa fa-server"></i>Data Aset Keluar</a></li>
            <li class="<?php echo ($menuActive=='laporan'?'active':''); ?>"> 
              <a class="menu-parent"><i class="fa fa-bar-chart"></i>Laporan</a>
              <div class="sub-menu <?php echo ($menuActive=='laporan'?'':'out'); ?>">
                <div class="item-sub-menu <?php echo ($subMenuActive=='aset_tetap'?'active':''); ?>"> <a href='<?php echo base_url('laporan/asettetap'); ?>'>Aset Tetap</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='aset_dijual'?'active':''); ?>"> <a href='<?php echo base_url('laporan/asetdijual'); ?>'>Aset Dijual</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='aset_jenis'?'active':''); ?>"> <a href='<?php echo base_url('laporan/asetjenis'); ?>'>Aset Jenis Barang</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='aset_kondisi'?'active':''); ?>"> <a href='<?php echo base_url('laporan/asetkondisi'); ?>'>Aset Kondisi Barang</a></div>
              </div>
            </li>
            <?php if($data_app["hak_akses"] == "1")  { ?>
            <li class="<?php echo ($menuActive=='user'?'active':''); ?>"> <a href='<?php echo base_url('user'); ?>'><i class='icon-interface-windows'></i>Data User</a></li>
            <?php } ?>
            <?php if($data_app["hak_akses"] == "1")  { ?>
              <li class="<?php echo ($menuActive=='master'?'active':''); ?>"> 
                <a class="menu-parent"><i class="fa fa-database"></i>Master</a>
                <div class="sub-menu <?php echo ($menuActive=='master'?'':'out'); ?>">
                  <div class="item-sub-menu <?php echo ($subMenuActive=='jenis_barang'?'active':''); ?>"> <a href='<?php echo base_url('master/jenisbarang'); ?>'>Jenis Barang</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='tipe_barang'?'active':''); ?>"> <a href='<?php echo base_url('master/tipebarang'); ?>'>Tipe Barang</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='kondisi_barang'?'active':''); ?>"> <a href='<?php echo base_url('master/kondisi'); ?>'>Kondisi</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='karyawan'?'active':''); ?>"> <a href='<?php echo base_url('master/karyawan'); ?>'>Karyawan</a></div>
                  <div class="item-sub-menu <?php echo ($subMenuActive=='divisi'?'active':''); ?>"> <a href='<?php echo base_url('master/divisi'); ?>'>Divisi</a></div>
                </div>
              </li>
            <?php } ?>
          </ul>
        </nav>    

      <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom"><?php echo isset($title_page)?$title_page:"-" ?></h2>
            </div>
          </header>


          <!-- Dashboard Counts Section-->

  <script>
    $(function () {    
      $(".menu-parent").on("click",function() {        
        $(this).parents('ul').find('li.active').find(".sub-menu").toggleClass("out");
        $(this).parents('ul').find('li.active').toggleClass("active");
        $(this).parent().toggleClass("active");
        $(this).next(".sub-menu").toggleClass("out");
      })
    });
  </script>