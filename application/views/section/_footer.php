        <br>
          <br>
          <!-- Page Footer-->
          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-6">
                  <p>Copyright &copy; 2021. All rights reserved.</p>
                </div>
                <div class="col-sm-6 text-right">
                  <p>PT <a href="#" class="external">Krakatau Information Technology</a></p>
                  <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
              </div>
            </div>
          </footer>
        </div>
    </div>
      
      
    <!-- Javascript files-->

    <!-- DataTables -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
    
    <script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>datepicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>datepicker/locales/bootstrap-datepicker.id.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>JQuery-validation/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>JQuery-validation/dist/localization/messages_id.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('url_plugin') ?>JQuery-validation/css/cmxform.css">

    <script src="<?php echo config_item('url_asset'); ?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo config_item('url_asset'); ?>/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

    
    <script>
      $(function () {
          $(document).on('show.bs.modal', '.modal', function() {
            if(!$(this).hasClass("in")){
              var zIndex = 1040 + (10 * $('.modal:visible').length);
              // $('html').css('overflow', 'hidden');
              
              $(this).css('z-index', zIndex);
              setTimeout(function() {
                  $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                  $('body').css('padding-right', '0px');
              }, 0);
            }
          });

          $(document).on('hidden.bs.modal', '.modal', function() {
              $('html').css('overflow', 'auto');
          });
          $('[data-toggle="tooltip"]').tooltip();
          $(".btn-kembali-beranda").on("click",function(){
            window.location.href = app.data.site_url + "main"
          });
          $(".popup-table").delegate('tbody tr','click',function(cmp){
            if($(this).hasClass('active')){
              $(this).removeClass('active');
            } else {
              $(this).parents('table').find('tr.active').removeClass('active');
              $(this).addClass("active");
            }
          });
        $('#datatables').DataTable();
      });
    </script>

  </body>
</html>
