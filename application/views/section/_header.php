<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Manajemen Aset</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/admin/vendor/bootstrap/css/bootstrap.min.css">
    
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/admin/css/fontastic.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/admin/vendor/font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/assets/plugins/datatables/dataTables.bootstrap.css"> 
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">  -->
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/admin/css/custom.css">
    <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/template/css/admin.css">

    <script src="<?php echo config_item('url_plugin').'jQuery/jquery-3.1.0.min.js' ?>"></script> 
  <script src="<?php echo $this->config->item('url_plugin') ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('url_plugin') ?>sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo config_item('url_plugin') ?>utils/startup.js"></script>
    
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
  </head>
    <?php 

    if (!isset($data_app)) {
        $data_app = array();
    }

    ?>

    <script type="text/javascript">
        app.data = <?php echo json_encode($data_app) ?>;
        $(document).ready(function() {
        $('.input-date').datepicker({
              format: 'dd/mm/yyyy',
              language: "id",
              forceParse: false,
              autoclose: true,
            });
        $(".input-year").datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
        $('.select2').select2();
      });
    </script>
    <body>
        <div class="page home-page">
        <?php $this->view("section/_navigation"); ?>