<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-left">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
              
            </div>
          
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo isset($sub_title)?$sub_title:"Form" ?></h3>
          </div>

          <div class="card-body">
            <form class="form-horizontal" id="form-kondisi-barang">
              <div class="line"></div>
              <input type="hidden" name="id_kondisi_barang" class="hidden">              
              
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama Kondisi <span class="red">*</span></label>
                <div class="col-sm-9">
                  <input type="text" name="nama_kondisi_barang" class="form-control" placeholder="Masukan Kondisi Barang" required>
                </div>
              </div>          

            </form>
          </div>

            <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                  <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                </div>
              </div>

        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/master/kondisi/form.js"></script></script>