<section class="tables">   
  <div class="container-fluid">
    <div class="row">
      
      <div class="col-lg-12">
        <div class="card">
        
          <div class="card-body text-left">
              <div class="row">
                  <div class="col-lg-8">
                      <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-tambah">Tambah Data</button>
                  </div>                  
                  <div class="col-lg-4">
                      <div class="input-group">
                          <input name="f_search" class="form-control input-lg f-search" placeholder="" type="text">
                          <span class="btn input-group-addon btn-action-cari">Cari</span>
                      </div>
                    </div>
              </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Daftar Divisi</h3>
          </div>
          <div class="card-body">
            <table id="daftar-data" class="table">
              <thead>
                <tr>
                    <th>No.</th>                    
                    <th>Kode Divisi</th>
                    <th>Nama Divisi</th>                                
                    <th align="center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
            <div class="row">              
                  <div class="col-md-7">
                    <nav aria-label="Page navigation" id="nav" style="padding: 0 10px;"></nav>
                  </div>
                  <div class="col-md-5" style="text-align:right;">
                           
                  </div>              
            </div>
          </div>
        
      </a>
    </div>
  </div>
</section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/master/divisi/main.js"></script></script>