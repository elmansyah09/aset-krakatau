<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-left">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
              
            </div>
          
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo isset($sub_title)?$sub_title:"Form" ?></h3>
          </div>

          <div class="card-body">
            <form class="form-horizontal" id="form-tipe-barang">
              <div class="line"></div>
              <input type="hidden" name="id_tipe_barang" class="hidden">              
              
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Jenis Barang <span class="red">*</span></label>
                <div class="col-sm-9">
                  <select name="id_jenis_barang" class="form-control select2 cb-jenis-barang" required>                  
                  </select>
                  <!-- <input type="text" name="jnsbarang" class="form-control" placeholder="Masukan Jenis Barang"> -->
                </div>
              </div>
              
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama Jenis Barang <span class="red">*</span></label>
                <div class="col-sm-9">
                  <input type="text" name="nama_tipe_barang" class="form-control" placeholder="Masukan Nama Jenis Barang" required>
                </div>
              </div>            

            </form>
          </div>

            <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                  <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                </div>
              </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->view("component\popup\karyawan"); ?>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/master/tipe_barang/form.js"></script></script>