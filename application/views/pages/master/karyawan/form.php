<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-left">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
              
            </div>
          
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo isset($sub_title)?$sub_title:"Form" ?></h3>
          </div>

          <div class="card-body">
            <form class="form-horizontal" id="form-karyawan">
              <div class="line"></div>
              <input type="hidden" name="id_karyawan" class="hidden">              

              <div class="form-group row">
                <label class="col-sm-3 form-control-label">NIK <span class="red">*</span></label>
                <div class="col-sm-9">
                  <input type="text" name="nik_karyawan" class="form-control" placeholder="Masukan NIK" required>
                </div>
              </div>
              
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama <span class="red">*</span></label>
                <div class="col-sm-9">
                  <input type="text" name="nama_karyawan" class="form-control" placeholder="Masukan Nama" required>
                </div>
              </div>  

              <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Divisi <span class="red">*</span></label>
                  <div class="col-sm-9">                  
                    <div class="input-group">
                        <input name="nama_divisi" class="form-control popup-divisi input-normal" placeholder="Divisi" type="text" readonly require>
                        <input type="hidden" class="hidden" name="id_divisi">
                        <span class="btn input-group-addon btn-action-cari btn-cari-divisi">Cari</span>
                    </div>
                  </div>
                </div>        

            </form>
          </div>

            <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                  <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                </div>
              </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->view("component\popup\divisi"); ?>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/master/karyawan/form.js"></script></script>