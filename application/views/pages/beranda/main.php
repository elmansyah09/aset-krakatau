
    
      

        
    
<section class="tables">   
<div class="container-fluid">
    <div class="row">

    <div class="col-lg-4">
        <div class="card">
        
        <div class="card-header d-flex align-items-center bg-main">
            <h3 class="h4">Total Aset Keseluruhan</h3>
        </div>
        <div class="card-body dashboard-total-aset text-center">
            <i class="fa fa-spinner w3-xxxlarge"></i>
        </div>
        </div>
    </div>   
    
    <div class="col-lg-4">
        <div class="card">
        
        <div class="card-header d-flex align-items-center bg-main">
            <h3 class="h4">Total Aset Masuk - <?php echo get_textual_month(date("n")).' '.date("Y") ?></h3>
        </div>
        <div class="card-body dashboard-total-aset-masuk text-center">
            <i class="fa fa-spinner w3-xxxlarge"></i>
        </div>
        </div>
    </div>   

    <div class="col-lg-4">
        <div class="card">        
            <div class="card-header d-flex align-items-center bg-main">
                <h3 class="h4">Total Aset Keluar <?php echo get_textual_month(date("n")).' '.date("Y") ?></h3>
            </div>
            <div class="card-body dashboard-total-aset-keluar text-center">
                <i class="fa fa-spinner w3-xxxlarge"></i>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="card">        
            <div class="card-header d-flex align-items-center bg-main">
                <h3 class="h4">Jenis Barang</h3>
            </div>
            <div class="card-body dashboard-jenis-barang">
                
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">        
            <div class="card-header d-flex align-items-center bg-main">
                <h3 class="h4">Kondisi Barang</h3>
            </div>
            <div class="card-body dashboard-kondisi-barang">
                
            </div>
        </div>
    </div>
<!--     
    <div class="col-lg-12">
        <div class="card">
        
        <div class="card-header d-flex align-items-center bg-main">
            <h3 class="h4">Log Activity</h3>
        </div>
        <div class="card-body">
            <table class="table">
            <thead>
                <tr>
                <th width='10'>No</th>
                <th>Waktu</th>
                <th>Aktivitas</th>
                <th>User</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </table>
        </div>
        </div>
    </div> -->

    </div>
</div> 
</section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/beranda/main.js"></script></script>