<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $this->config->item("application_name") ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo config_item('url_asset'); ?>/css/w3.css">        
        <script src="<?php echo config_item('url_plugin').'jQuery/jquery-3.1.0.min.js' ?>"></script>        
        <script src="<?php echo $this->config->item('url_plugin') ?>sweetalert/sweetalert.min.js"></script>
        <script src="<?php echo config_item('url_plugin') ?>utils/startup.js"></script>
    </head>
    <?php 

    if (!isset($data_app)) {
        $data_app = array();
    }

    ?>

    <script type="text/javascript">
        app.data = <?php echo json_encode($data_app) ?>;
    </script>
<body class="w3-content" style="max-width:500px">
<style>
    body {
        overflow-x: hidden;
    }
    
    form {
        border: 3px solid #f1f1f1;
    }

    input[type=text], input[type=password] {
        width: 100%;
        padding: 12px 20px;
        margin: 10px 0;
        display: inline-block;
        border: 1px solid #ccc;
        box-sizing: border-box;
    }

    button {
        background-color: #419bab;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
    }

    body {
        background-image: url("<?php echo config_item('url_asset'); ?>/template/image/bg-login.jpg");
        background-size: 100%;
        background-color: #cccccc;
    }

    button:hover {
        opacity: 0.8;
    }

    .cancelbtn {
        width: auto;
        padding: 10px 18px;
        background-color: #f44336;
    }

    .imgcontainer {
        text-align: center;
        margin: 24px 0 12px 0;
    }

    img.avatar {
        width: 40%;
        border-radius: 50%;
    }

    .container {
        padding: 16px;
        font-family: system-ui; 
    }

    span.psw {
        float: right;
        padding-top: 16px;
    }

    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
        span.psw {
        display: block;
        float: none;
        }
        .cancelbtn {
        width: 100%;
        }
    }
</style>
<br>
<hr>
<center><b><h2>Login Administrator</h2></b><h3>Sistem Informasi Manajemen Aset</h3></center>

<hr>
<div class="w3-content">

  <form id="form-login">
  <div class="container">
    <label><b>Username</b></label>
    <input type="text" placeholder="Masukan Username" name="username" required>

    <label><b>Password</b></label>
    <input type="password" placeholder="Masukan Password" name="password" required>
        
    <button class="btn-action-login">Login</button>
    <hr>
    <div class="login-box-body">
        
        <p class="help-block label-danger" style="padding:5px;"><span class="pull-right glyphicon glyphicon-exclamation-sign"></span><center></center></p>
        <center><p>PT Krakatau Information Technology</p></center>
      </div>
        
  </div>
   </div>
    </div>

 
    </form>
</div>

</script>
<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/app/login.js"></script></script>
<script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>JQuery-validation/dist/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('url_plugin') ?>JQuery-validation/dist/localization/messages_id.js"></script>
</body>
</html>

