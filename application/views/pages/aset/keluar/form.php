<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-left">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
              
            </div>
          
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo isset($sub_title)?$sub_title:"Form" ?></h3>
          </div>

          <div class="card-body">
            <form class="form-horizontal" id="form-aset-keluar">
              <div class="line"></div>
              <input type="hidden" class="hidden" name="id_aset_keluar">
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">No. Seri <span class="red">*</span></label>
                <div class="col-sm-9">                  
                  <div class="input-group">
                      <input name="nomor_seri" class="form-control popup-aset" placeholder="Masukan Nomor Seri" type="text" required>
                      <input type="hidden" class="hidden" name="id_aset">
                      <span class="btn input-group-addon btn-action-cari btn-cari-aset">Cari</span>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama Barang</label>
                <div class="col-sm-9">
                  <input type="text" name="nama_barang" class="form-control" placeholder="Nama Barang" readonly>
                </div>
              </div>
                <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kondisi Barang <span class="red">*</span></label>
                <div class="col-sm-9">
                  <select name="id_kondisi_barang" class="form-control select2 cb-kondisi-barang" required placeholder="Masukan Kondisi Barang">                  
                  </select>
                </div>
              </div>            
              
            </form>
          </div>


            <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                  <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                </div>
              </div>

        </div>
      </div>
    </div>
  </div>
</section>

<?php $this->view("component\popup\aset"); ?>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/aset/keluar/form.js"></script></script>