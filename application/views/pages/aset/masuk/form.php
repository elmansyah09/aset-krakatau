<section class="forms"> 
  <div class="container-fluid">
    <div class="row">

      
      <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-left">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
              
            </div>
          
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo isset($sub_title)?$sub_title:"Form" ?></h3>
          </div>

          <div class="card-body">
            <form class="form-horizontal" id="form-aset-masuk">
              <div class="line"></div>
              <input type="hidden" name="id_aset_masuk" class="hidden">
              <input type="hidden" name="id_aset" class="hidden">
              <div class="form-group row field-ubah hidden">
                <label class="col-sm-3 form-control-label">No. Seri</label>
                <div class="col-sm-9">
                  <input type="text" name="nomor_seri" class="form-control" placeholder="Masukan Nomor Seri" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Nama Barang <span class="red">*</span></label>
                <div class="col-sm-9">
                  <input type="text" name="nama_barang" class="form-control" placeholder="Masukan Nama Barang" required>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Jenis Barang <span class="red">*</span></label>
                <div class="col-sm-9">
                  <select name="id_jenis_barang" class="form-control select2 cb-jenis-barang" required>                  
                  </select>
                  <!-- <input type="text" name="jnsbarang" class="form-control" placeholder="Masukan Jenis Barang"> -->
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Tipe Barang</label>
                <div class="col-sm-9">                  
                  <select name="id_tipe_barang" class="form-control select2 cb-tipe-barang">                  
                    <option disabled selected>Pilih Tipe Barang</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Tanggal Pembelian</label>
                <div class="col-sm-9">
                  <input type="text" name="tanggal_pembelian" class="form-control input-date" readonly>
                </div>
              </div>
                <div class="form-group row">
                <label class="col-sm-3 form-control-label">Harga Pembelian</label>
                <div class="col-sm-9">
                  <input type="number" name="harga_barang" class="form-control" placeholder="Masukan Harga Pembelian">
                </div>
              </div>
                <div class="form-group row">
                <label class="col-sm-3 form-control-label">Harga Buku</label>
                <div class="col-sm-9">
                  <input type="number" name="nilai_buku" class="form-control" placeholder="Masukan Harga Buku">
                </div>
              </div>
              <div class="form-group row field-tambah">
                <label class="col-sm-3 form-control-label">Jumlah</label>
                <div class="col-sm-9">
                  <input type="number" name="jumlah" class="form-control" placeholder="Masukan Jumlah Barang">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Jangka Waktu</label>
                <div class="col-sm-9">
                  <input type="number" name="jangka_waktu" class="form-control" placeholder="Masukan Jangka Waktu">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Detail Barang</label>
                <div class="col-sm-9">
                  <textarea name="keterangan" rows="4" class="form-control" placeholder="Masukan Detail Barang"></textarea>                  
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Kondisi Barang</label>
                <div class="col-sm-9">
                  <!-- <input type="text" name="kondisibrg" class="form-control" placeholder="Masukan Kondisi Barang"> -->
                  <select name="id_kondisi_barang" class="form-control select2 cb-kondisi-barang" placeholder="Masukan Kondisi Barang">                  
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-3 form-control-label">Penanggung Jawab <span class="red">*</span></label>
                <div class="col-sm-9">                  
                  <div class="input-group">
                      <input name="nama_penanggung_jawab" class="form-control popup-karyawan input-normal" placeholder="Penanggung Jawab" type="text" readonly>
                      <input type="hidden" class="hidden" name="id_penanggung_jawab">
                      <span class="btn input-group-addon btn-action-cari btn-cari-karyawan">Cari</span>
                  </div>
                </div>
              </div>
                <!-- <div class="form-group row">
                <label class="col-sm-3 form-control-label">File</label>
                <div class="col-sm-9">
                  <input type="file" name="file" class="form-control">
                </div>
              </div> -->

              
            </form>
          </div>

            <div class="line"></div>
              <div class="form-group row">
                <div class="col-sm-4 offset-sm-3">
                  <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                  <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                </div>
              </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php $this->view("component\popup\karyawan"); ?>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/aset/masuk/form.js"></script></script>