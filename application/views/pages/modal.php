<!-- modal tambah soal-evaluasi -->
<div class="modal fade main-modal modal-ubah-sandi" id="modal-ubah-sandi">
    <div class="modal-dialog" style="width: 460px !important;"> 
      <div class="modal-content">
        <div class="modal-header">          
          <h3 class="modal-title text-color-gray">Ubah Sandi</h3>
        </div>
        <div class="modal-body">
            <form id="form-ubah-sandi" class="form">
              <div class="row" style="padding:10px;">
                <input type="hidden" name="userid" class="hidden">
                <div class="row">
                    <div class="col-sm-5">
                        <label class="text-color-grey">Sandi Lama <span class="text-color-red"></span></label>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <label class="text-color-grey">Sandi Baru <span class="text-color-red"></span></label>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="new_password" required="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <label class="text-color-grey">Konirmasi Sandi Baru <span class="text-color-red"></span></label>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="conf_new_password" required="">
                        </div>
                    </div>
                </div>        
              </div>
            </form>
        </div>
        <div class="modal-footer" style="padding-bottom: 0px;padding-top: 0px">          
            <div class="row">                
                <div class="col-xs-6 no-padding">
                  <button type="button" class="btn btn-flat btn-block" data-dismiss="modal">Batal</button>
                </div>
                <div class="col-xs-6 no-padding">
                  <button type="button" class="btn btn-primary btn-flat btn-block btn-simpan" id="btn-simpan">Simpan</button>
                </div>          
            </div>
        </div>
      </div>
    </div>
</div>
<!-- tutup modal tambah user -->