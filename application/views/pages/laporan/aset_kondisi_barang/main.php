<section class="tables">   
  <div class="container-fluid">
    <div class="row">
      
      <div class="col-lg-12">
        <div class="card">
        
          <div class="card-body text-left">
              <div class="row">
                  <div class="col-lg-6">
                      <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-action-cetak"><i class="fa fa-print"></i> Cetak</button>
                  </div>
                  <div class="col-lg-6">
                      <div class="row">
                        <div class="col-lg-6">
                            <select class="form-control select2 f-kondisi-barang">                  
                              <option disabled selected>Pilih Kondisi Barang</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                          <div class="input-group">
                              <input name="f_search" class="form-control input-lg f-search" placeholder="" type="text">
                              <span class="btn input-group-addon btn-action-cari">Cari</span>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Daftar Aset Kondisi Barang</h3>
          </div>
          <div class="card-body">            
              <table id="daftar-data" class="table">
                <thead>
                  <tr>
                      <th>No.</th>
                      <th>No Seri</th>
                      <th>Nama Barang</th>                      
                      <th>Kondisi Barang</th>
                      <th>Tgl Pembelian</th>
                      <th>Harga Barang (Rp)</th>
                      <th>Jangka Waktu</th>
                      <th>Penyusutan Perbulan (Rp)</th>
                      <th>Nilai Buku (Rp)</th>
                      <th>Penanggung Jawab</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>           
              <div class="row">              
                  <div class="col-md-7">
                    <nav aria-label="Page navigation" id="nav" style="padding: 0 10px;"></nav>
                  </div>
                  <div class="col-md-5" style="text-align:right;">
                      <div style="padding:10px;">
                        Total : <span class="ft-count">0</span>
                      </div>              
                  </div>              
            </div> 
          </div>
        
      </a>
    </div>
  </div>
</section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/laporan/aset_kondisi_barang/main.js"></script></script>