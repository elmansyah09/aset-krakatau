 <section class="forms"> 
    <div class="container-fluid">
      <div class="row">

        
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body text-left">
              
              </div>
            
            <div class="card-header d-flex align-items-center">
              <h3 class="h4">Ubah Password</h3>
            </div>
            <div class="card-body">
              <form id="form-ubah-password" class="form-horizontal" tipe="ganti_password">
                <input type="hidden" class="hidden" name="id_admin">                
                <div class="line"></div>
        
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Password Lama <span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="password" name="old_password" class="form-control" placeholder="Masukan Password Lama" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Password Baru <span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="password" name="password" class="form-control" placeholder="Masukan Password Baru" required>
                  </div>
                </div>

                 <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Konfirmasi Password Baru<span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="password" name="confirm_password" class="form-control" placeholder="Konfirmasi Masukan Password Baru" required>
                  </div>
                </div>

                <div class="line"></div>
                <div class="form-group row">
                  <div class="col-sm-4 offset-sm-3">
                    <button class="btn btn-primary btn-action-simpan" >Ubah</button> 
                    <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                  </div>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/ubah_password.js"></script></script>