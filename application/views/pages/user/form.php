 <section class="forms"> 
    <div class="container-fluid">
      <div class="row">

        
        <div class="col-lg-12">
          <div class="card">
              <div class="card-body text-left">
              <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-kembali">Kembali</button>
                
              </div>
            
            <div class="card-header d-flex align-items-center">
              <h3 class="h4">Tambah Data User</h3>
            </div>
            <div class="card-body">
              <form id="form-user" class="form-horizontal">
                <input type="hidden" class="hidden" name="id_admin">
                <div class="line"></div>

                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Karyawan <span class="red">*</span></label>
                  <div class="col-sm-9">                  
                    <div class="input-group">
                        <input name="nama_karyawan" class="form-control popup-karyawan input-normal" placeholder="Karyawan" type="text" readonly>
                        <input type="hidden" class="hidden" name="id_karyawan">
                        <span class="btn input-group-addon btn-action-cari btn-cari-karyawan">Cari</span>
                    </div>
                  </div>
                </div>
        
                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Username <span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="text" name="username" class="form-control" placeholder="Masukan Username" required>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Password <span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="password" name="password" class="form-control" placeholder="Masukan Password" required>
                  </div>
                </div>

                 <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Konfirmasi Password <span class="red">*</span></label>
                  <div class="col-sm-9">
                    <input type="password" name="confirm_password" class="form-control" placeholder="Konfirmasi Masukan Password" required>
                  </div>
                </div>

                  <div class="form-group row">
                  <label class="col-sm-3 form-control-label">Level</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="level">
                      <option value="1">Superadmin</option>
                      <option value="2">Admin</option>
                      
                    </select>
                  </div>
                </div>                  
                
                <div class="line"></div>
                <div class="form-group row">
                  <div class="col-sm-4 offset-sm-3">
                    <button class="btn btn-primary btn-action-simpan" >Simpan</button> 
                    <button type="reset" class="btn btn-secondary btn-batal">Batal</button> 
                  </div>
                </div>
                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php $this->view("component\popup\karyawan"); ?>
<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/user/form.js"></script></script>