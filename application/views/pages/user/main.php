<section class="tables">   
    <div class="container-fluid">
      <div class="row">
        
        <div class="col-lg-12">
          <div class="card">
          
            <div class="card-body text-left">
                <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-tambah">Tambah User</button>
              </div>
            <div class="card-header d-flex align-items-center">
              <h3 class="h4">Daftar User</h3>
            </div>
            <div class="card-body">
              <table id="daftar-data" class="table">
                <thead>
                  <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Username</th>
                      <th>Level</th>
                      <th align="center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          
        </a>
      </div>
    </div>
  </section>

<script type="text/javascript" src="<?php echo config_item('url_src_pages') ?>/user/main.js"></script></script>