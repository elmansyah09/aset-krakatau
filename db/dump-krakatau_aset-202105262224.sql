-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: krakatau_aset
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(64) NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `level` char(1) NOT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_dokumen`
--

DROP TABLE IF EXISTS `app_dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_dokumen` (
  `id_dokumen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dokumen` varchar(100) DEFAULT NULL,
  `nama_dokumen_geneated` varchar(100) DEFAULT NULL,
  `tipe_dokumen` varchar(50) DEFAULT NULL,
  `ukuran_dokumen` varchar(10) DEFAULT NULL,
  `lokasi_dokumen` varchar(50) DEFAULT NULL,
  `referensi_dokumen_id` int(11) NOT NULL,
  `sumber_dokumen` varchar(20) DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_dokumen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aset`
--

DROP TABLE IF EXISTS `aset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aset` (
  `id_aset` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(50) NOT NULL,
  `nomor_seri` varchar(20) DEFAULT NULL,
  `id_jenis_barang` int(11) DEFAULT NULL,
  `id_tipe_barang` int(11) DEFAULT NULL,
  `jangka_waktu` int(3) DEFAULT NULL,
  `tanggal_pembelian` date DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `nilai_buku` double DEFAULT NULL,
  `jumlah` decimal(10,0) DEFAULT NULL,
  `harga_barang` double DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `id_kondisi_barang` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_aset`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aset_keluar`
--

DROP TABLE IF EXISTS `aset_keluar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aset_keluar` (
  `id_aset_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_input` date DEFAULT NULL,
  `id_aset` int(11) NOT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `id_kondisi_barang` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_aset_keluar`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aset_masuk`
--

DROP TABLE IF EXISTS `aset_masuk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aset_masuk` (
  `id_aset_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_input` datetime DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_aset_masuk`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `aset_masuk_detail`
--

DROP TABLE IF EXISTS `aset_masuk_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aset_masuk_detail` (
  `id_aset_masuk_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_aset_masuk` int(11) NOT NULL,
  `id_aset` int(11) NOT NULL,
  PRIMARY KEY (`id_aset_masuk_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jenis_barang`
--

DROP TABLE IF EXISTS `jenis_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_barang` (
  `id_jenis_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_barang` varchar(50) NOT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_jenis_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `nik_karyawan` varchar(20) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `kondisi_barang`
--

DROP TABLE IF EXISTS `kondisi_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kondisi_barang` (
  `id_kondisi_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kondisi_barang` varchar(50) NOT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_kondisi_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mutasi_aset`
--

DROP TABLE IF EXISTS `mutasi_aset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mutasi_aset` (
  `id_mutasi_aset` int(11) NOT NULL AUTO_INCREMENT,
  `id_aset` int(11) NOT NULL,
  `id_divisi_tujuan` int(11) NOT NULL,
  `id_karyawan_penanggung_jawab` int(11) NOT NULL,
  `tanggal_input` datetime DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_mutasi_aset`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipe_barang`
--

DROP TABLE IF EXISTS `tipe_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipe_barang` (
  `id_tipe_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe_barang` varchar(50) NOT NULL,
  `id_jenis_barang` int(11) NOT NULL,
  `dibuat_oleh` int(11) DEFAULT NULL,
  `tanggal_buat` datetime DEFAULT NULL,
  `tanggal_ubah` datetime DEFAULT NULL,
  `diubah_oleh` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'aktif',
  PRIMARY KEY (`id_tipe_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'krakatau_aset'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-26 22:24:35
