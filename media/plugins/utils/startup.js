app = {};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

app.startup = function(args) {
    app.loader(args);
};

app.disable_select =   function readonly_select(objs, action) {
        if (action===true)
            objs.prepend('<div class="disabled-select"></div>');
        else
            $(".disabled-select", objs).remove();
    }

 $.fn.drags = function(opt) {

        opt = $.extend({handle:"",cursor:"move"}, opt);

        if(opt.handle === "") {
            var $el = this;
        } else {
            var $el = this.find(opt.handle);
        }

        return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
            if(opt.handle === "") {
                var $drag = $(this).addClass('draggable');
            } else {
                var $drag = $(this).addClass('active-handle').parent().addClass('draggable');
            }
            var z_idx = $drag.css('z-index'),
                drg_h = $drag.outerHeight(),
                drg_w = $drag.outerWidth(),
                pos_y = $drag.offset().top + drg_h - e.pageY,
                pos_x = $drag.offset().left + drg_w - e.pageX;
            $drag.css('z-index', 1000).parents().on("mousemove", function(e) {
                $('.draggable').offset({
                    top:e.pageY + pos_y - drg_h,
                    left:e.pageX + pos_x - drg_w
                }).on("mouseup", function() {
                    $(this).removeClass('draggable').css('z-index', z_idx);
                });
            });
            e.preventDefault(); // disable selection
        }).on("mouseup", function() {
            if(opt.handle === "") {
                $(this).removeClass('draggable');
            } else {
                $(this).removeClass('active-handle').parent().removeClass('draggable');
            }
        });

    }

app.onReady = function(args) {
    var load_js = args.load.js_second || {};    
    var loaded = {};
    loaded.js = '';

    for (i = 0; i < load_js.length; i++) {
        app.load_js(load_js[i]);
        loaded.js += load_js[i] + ' ';
    }

    $(document).ajaxStop(function() {
        app.body_unmask();
    });

    $(document).on('show.bs.modal', '.modal', function() {
        var zIndex = 1010 + (10 * $('.modal:visible').length);
        $('html').css('overflow', 'hidden');
        alert(zIndex);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            // $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            $('body').css('padding-right', '0px');
        }, 0);
    });

    $(document).on('hidden.bs.modal', '.modal', function() {
        $('html').css('overflow', 'auto');
    });
};

app.loader = function(args) {
    var load_css = args.load.css || {};
    var load_js = args.load.js || {};


    var loaded = {};
    loaded.css = '';
    loaded.js = '';
    for (i = 0; i < load_css.length; i++) {
        app.load_css(load_css[i]);
        loaded.css += load_css[i] + ' ';
    }

    for (i = 0; i < load_js.length; i++) {
        app.load_js(load_js[i]);
        loaded.js += load_js[i] + ' ';
    }

    if (typeof args.load.success == 'function') {
        typeof args.load.success();
    }
};

app.load_js = function(url) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('script');
    link.type = 'text/javascript';
    link.charset = 'UTF-8';
    link.src = url;
    head.appendChild(link);
    // document.write('<script type="text/javascript" charset="UTF-8" src="' + url + '"></script>');
};

app.load_css = function(url) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = url;
    link.media = 'all';
    head.appendChild(link);
    // document.write('<link href="' + url + '" rel="stylesheet">');
};

app.body_mask = function() {
    $('.loader-mask').show();
    $('body').css('overflow', 'hidden');
};

app.body_unmask = function() {
    $('.loader-mask').hide();
    $('body').css('overflow', 'auto');
};

app.load_submenu = function(){
    var me = this;
    $(".submenu-restric").hide();
    var menu = app.data.site_url.replace(app.data.base_url,"") + "/" + app.data.segment[1];
    app.requestAjax(app.data.site_url + "/Simplelist/get_submenu",{link:menu},"POST",function(result) 
    {
        result.data.forEach(function(rec) {
            
            $(rec.FITURID).show();

        });
                        
    });
}

app.progress_dialog_show = function(tilte) {
    var modal = $('#progressDialog');
    if (tilte)
    {
        modal.find("title-modal").html(tilte);
    }
    modal.modal({backdrop:"static"});
};

app.progress_dialog_hide = function() {
    var modal = $('#progressDialog');
    modal.modal("hide");
};

app.convert_form = function(param) {
    var out = {};
    for (var i = 0; i < param.length; i++) {
        out[param[i].name] = param[i].value;
    }

    return out;
};

app.params_serialize = function(params) {
    
    var out = {};
    params.forEach(function(item, index, arr) {

    });

    return out;
};

app.obj_serialize = function(obj) {
    var out = "";
    for(let name in obj) {
        if(out != ""){
            out += "&";
        }

        out += name+"="+obj[name];
    }

    return out;
};

app.set_form_value = function(frm, data, display) {
   $.each(data, function(key, value) {
       value = value || '-';
       value = (value == 'null') ? '-' : value;
       value = (typeof display != 'undefined' && value == '-') ? '' : value;
       var $ctrl = $('[name=' + key + ']', frm);

       switch ($ctrl.attr("type")) {
           case "text":
           case "hidden":
               $ctrl.val(value);
           case "radio":
           case "checkbox":
               $ctrl.attr("checked", true);
               $ctrl.click();
           case "html":
               if ($ctrl.attr("type") == "html") {
                   $ctrl.html(value);
               }
           case "select":
               $ctrl.val(value);
            case "file": break;
           default:
                $ctrl.val(value);
       }       
       if ($ctrl.is('.input-date')) {
           if (value != null && value != '') {
               value = (isNaN(new Date(value))) ? app.formatDate(value, '/') : value;
               console.log(value);
               $ctrl.datepicker('update', new Date(value));
           }
       }

       if ($ctrl.is('.select2:not([multiple])')) {
           $ctrl.val(value).trigger('change.select2');
           $ctrl.val(value).trigger('change');
       }
   });
};

app.requestAjax = function(url,params,method,callback,arg_async) {
    
    var asyncroun = true;
    try {
        if (arg_async === false) {
            asyncroun = false;
        }
    } catch(e) {
        asyncroun = true;
    }
    
    $.ajax({
        url: url,
        type: method,
        dataType: 'json',
        async:asyncroun,
        data: params,
        beforeSend: function (xhr) {            
            xhr.setRequestHeader("token", localStorage.getItem("token_ppnpn"));      
            xhr.setRequestHeader("iswb", 989);         
            app.is_mobile(function () {
            });
        },
    })
    .done(function(result) {
        if (callback) 
        {
            callback(result);
        }
    })
    .fail(function(result) {                                
        if(result.responseText.indexOf("Authentication Failed") !== -1) {
            window.location.href = app.data.base_url;
        } else {
            swal({
                title: "Informasi!",
                text: '('+result.status+') '+result.statusText,                         
                icon: "warning",
            });
        }
    })
    .always(function() {
        setTimeout(function() {
            app.body_unmask();
        },500);
    });     

};

app.requestAjaxForm = function(url,params,method,callback) {
    $.ajax({
        url: url,
        type: method,
        dataType: 'json',
        data: params,        
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function (xhr) {            
            xhr.setRequestHeader("token", localStorage.getItem("token_ykp"));          
            xhr.setRequestHeader("iswb", 989);
            app.is_mobile(function () {
            });
        },
    })
    .done(function(result) {        
        if(result.code == 403 && result.message == "Authentifiction Failed") {
            window.location.href = app.data.base_url;
        }
        if (callback) 
        {
            callback(result);
        }
    })
    .fail(function(result) {                                
        if(result.responseText.indexOf("Authentication Failed") !== -1) {
            window.location.href = app.data.base_url;
        } else {
            swal({
                title: "Informasi!",
                text: '('+result.status+') '+result.statusText,                         
                icon: "warning",
            });
        }
    })
    .always(function() {
        setTimeout(function() {
            app.body_unmask();
        },500);
    });     

};

app.image_exists = function(image_url) {

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;
};

app.clear_form = function(form) {
    try{
        $(form).validate().resetForm();
    } catch(e) {

    }

    try {
        $(form).find('.input-date').val('').datepicker('update');
    } catch(e) {
        
    }

    $(form).find('input[type=text], input[type=hidden], input[type=search], input[type=number], input[type=password], input[type=date], input[type=email], input[type=file], textarea, select').val('');
    $(form).find('.select2').val('').trigger('change');
    $(form).find('.displayfield').html('');
    
    $(form).find('.error').removeClass('error');
};

app.random_char = function(jml) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    jml = (typeof jml == 'undefined') ? 8 : jml;

    for (var i = 0; i < jml + 1; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};
app.submit_form = function(form, button, callback) {
    $(form).delegate('input', 'keyup keypress', function(e) {
        if (!$(this).is('.input-date, .select2-search__field') && e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $(form).delegate('input', 'keyup', function(e) {
        if (!$(this).is('.input-date, .select2-search__field') && e.keyCode === 13) {
            $(button).click();
        }
    });

    $(button).on('click', function(e) {
        e.preventDefault();
        if (typeof callback == 'function') {
            callback.call(this);
        }
    });
};
app.showErrors = function(errorMessage, errormap, errorlist) {
    if (errorMessage) {
        var val = this;
        errormap.forEach(function(error, index) {
            val.settings.highlight.call(val, error.element, val.settings.errorClass, val.settings.validClass);
            val.showLabel(error.element, error.message);
        });
    } else {
        this.defaultShowErrors();
    }

};
app.fixValidFieldStyles = function($form, validator) {
    var errors = {};
    $form.find("input,select").each(function(index) {
        var name = $(this).attr("name");
        errors[name] = validator.errorsFor(name);
    });
    validator.showErrors(errors);
    var invalidFields = $form.find("." + validator.settings.errorClass);
    if (invalidFields.length) {
        invalidFields.each(function(index, field) {
            if ($(field).valid()) {
                $(field).removeClass(validator.settings.errorClass);
            }
        });
    }
};
app.round10 = function(number, precision) {
    var factor = Math.pow(10, precision),
        tempNumber = number * factor,
        roundedTempNumber = Math.round(tempNumber);

    return roundedTempNumber / factor;
};
app.getFileExtension = function(filename) {
    return filename.split('.').pop();
};
app.loading = '<div class="loading" style="z-index:100000;">' +
    '               <svg viewBox="0 0 32 32" width="32" height="32">' +
    '                   <circle id="spinner" cx="16" cy="16" r="14" fill="none"></circle>' +
    '               </svg>' +
    '           </div>';
app.get_param = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

app.readURL = function(input, target) {
    target = (typeof target == 'undefined') ? '.img-thumbnail img' : target;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(target)
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
};

app.is_mobile = function(callback) {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        if (typeof callback == 'function') {
            callback.call(this);
        }
    }
};

app.nl2br = function(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
};

app.calculateAge = function(birthday) { // birthday is a date
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
};

app.formatDate = function(date, str) {
    var str = (str) ? str : '-';
    return date.split(str).reverse().join(str);
};

app.check_fitur = function(fiturid) {
    var akses = false,
        res = app.fitur.filter(function(row) {
            return row.fiturid == fiturid;
        });

    if (res.length > 0) {
        var akses = true;
    }

    return akses;
};

app.dateDiff = function(dt1, dt2)
{
    /*
     * setup 'empty' return object
     */
    var ret = {days:0, months:0, years:0};

    /*
     * If the dates are equal, return the 'empty' object
     */
    if (dt1 == dt2) return ret;

    /*
     * ensure dt2 > dt1
     */
    if (dt1 > dt2)
    {
        var dtmp = dt2;
        dt2 = dt1;
        dt1 = dtmp;
    }

    /*
     * First get the number of full years
     */

    var year1 = dt1.getFullYear();
    var year2 = dt2.getFullYear();

    var month1 = dt1.getMonth();
    var month2 = dt2.getMonth();

    var day1 = dt1.getDate();
    var day2 = dt2.getDate();

    /*
     * Set initial values bearing in mind the months or days may be negative
     */

    ret['years'] = year2 - year1;
    ret['months'] = month2 - month1;
    ret['days'] = day2 - day1;

    /*
     * Now we deal with the negatives
     */

    /*
     * First if the day difference is negative
     * eg dt2 = 13 oct, dt1 = 25 sept
     */
    if (ret['days'] < 0)
    {
        /*
         * Use temporary dates to get the number of days remaining in the month
         */
        var dtmp1 = new Date(dt1.getFullYear(), dt1.getMonth() + 1, 1, 0, 0, -1);

        var numDays = dtmp1.getDate();

        ret['months'] -= 1;
        ret['days'] += numDays;

    }

    /*
     * Now if the month difference is negative
     */
    if (ret['months'] < 0)
    {
        ret['months'] += 12;
        ret['years'] -= 1;
    }

    return ret;
};


app.date_format = function(date,format_now)
{
    
    if (format_now == 'd/m/Y') 
    {   
        var temp = date.split("/");
        var date = temp[2]+"-"+temp[1]+"-"+temp[0];
    }


    return date;
};

app.ifvalnull = function(val,dflt) {
    if (val == null)
    {
        val = dflt;
    }
    return val;
}

app.dateFormatIndonesia = function(tgl, separator) 
{
    var format = new Date(tgl);
    var bulan = ['Januari', 'Feburari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var new_date = (format.getDate()+'') + separator + bulan[format.getMonth()] + separator + (''+format.getFullYear());
    
    return new_date;
};

app.get_data_list = function(cmp,link,args,config, callback) {
    $(cmp).select2({
        placeholder: 'Loading...'
    });

    try{
        var can_clear = config.can_clear;        
    }catch(e)
    {
        var can_clear = false;            
    }

    try
    {
        var placeholder = config.placeholder;
    }
    catch(e)
    {
        var placeholder = '';
    }

    try
    {
        var display_value = config.display_value;
    }
    catch(e)
    {
        var display_value = 'text';
    }

    try
    {
        var value = config.value;
    }
    catch(e)
    {
        var value = 'id';
    }

    try
    {
        var is_filter = config.is_filter;
    }
    catch(e)
    {
        var is_filter = false;
    }
    
    $.ajax({
            url: link,
            method: 'POST',
            dataType:'json',
            data: args,
            beforeSend: function (xhr) {            
                xhr.setRequestHeader("token", localStorage.getItem("token_ykp"));      
                xhr.setRequestHeader("iswb", 989);         
                app.is_mobile(function () {
                });
            },
        })
        .done(function(result) {            
            $(cmp).html('');
            $(cmp).select2({
                allowClear:can_clear,
                placeholder: placeholder
            });            
            if(placeholder != ""){
                $(cmp).append('<option disabled selected>'+placeholder+'</option>');
            }
            if(is_filter){
                $(cmp).append('<option value="">Semua</option>');

            }
            $.each(result.data, function(index, val) {
                var html = '<option value="' + val[value] + '" >' + val[display_value] + '</option>';
                $(cmp).append(html);

            });

            // $(cmp).val('').trigger('change');
            if(placeholder == ""){
                $(cmp).val('');
            }
            if (callback) {
                callback(result,$(cmp));
            }
        })
        .fail(function() {
            console.log('gagal terhubungan dengan server');
        });
};

app.get_data_list_extra_attributes = function(cmp,link,args,config, callback) {
    $(cmp).select2({
        placeholder: 'Loading...'
    });

    try{
        var can_clear = config.can_clear;        
    }catch(e)
    {
        var can_clear = true;            
    }

    try
    {
        var placeholder = config.placeholder;
    }
    catch(e)
    {
        var placeholder = '';
    }

    try
    {
        var display_value = config.display_value;
    }
    catch(e)
    {
        var display_value = 'text';
    }

    try
    {
        var value = config.value;
    }
    catch(e)
    {
        var value = 'id';
    }

    try
    {
        var extra = config.extra_attributes;
    }
    catch(e)
    {
        var extra = {};
    }

    $.ajax({
            url: link,
            method: 'POST',
            dataType:'json',
            data: args,
            beforeSend: function (xhr) {            
                xhr.setRequestHeader("token", localStorage.getItem("token_ykp"));      
                xhr.setRequestHeader("iswb", 989);         
                app.is_mobile(function () {
                });
            },
        })
        .done(function(result) {            
            $(cmp).html('');
            $(cmp).select2({
                allowClear:can_clear,
                placeholder: placeholder
            });
            $.each(result.data, function(index, val) {
                var extra_html = '';
                $.each(extra, function(idx,v){
                    extra_html = extra_html+' '+idx+'="'+val[v]+'"';
                });
                var html = '<option value="' + val[value] + '" '+extra_html+'>' + val[display_value] + '</option>';
                $(cmp).append(html);

            });

            //$(cmp).val('').trigger('change');
            if (callback) {
                callback(result)
            }
        })
        .fail(function() {
            console.log('gagal terhubungan dengan server');
        });
};

app.download_dokumen = function (filename,filedownload) {
    var me = this,
        params = {
            f:btoa(filename)                    
        };
    app.requestAjax(app.data.site_url+"app/check_dokumen",params,"POST",function(result) {
        if ('success' in result)
        {   
            if (!result.success && 'message' in result)
            {
                swal({
                    title: "Informasi!",
                    text: result.msg,
                    icon: "warning",
                });
            }
            else
            {
                var link = document.createElement("a");
                link.download = filedownload;
                link.href = app.data.url_client_dokumen+filename;
                link.click();
            }
        }
        
    });
};

app.anti_null = function(string){
    if (string==null) {
        return "-";
    }else{
        return string;
    }
}

app.default_null = function(data, standar){
    for(var name in data){
        data[name] = app.ifvalnull(data[name], standar);       
    }

    return data;
}

app.findValueFromArrayObject = function(arr,name,k) {
    var out = -1;
    for (var i = 0; i < arr.length; i++) 
    {
        if (name in arr[i])
        {
            if(arr[i][name] == k)
            {
                out = i;
                break;
            }            
        }        
    }

    return out;
  }

  app.list_requestAjax = function(url,params,method,name_cmp,config,callback) {
    app.requestAjax(url,params,method,function(result) {
       var content = "";     
        
        if ("data" in result)
        {
            var source =
            {                
                localData: result.data,
                dataType: "array",
                dataFields: config.dataFields
                    
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            var gridDiv = document.querySelector(name_cmp);
            if (result.data.length == 0)
            {                
                // $(name_cmp).html('');                
                $(name_cmp).jqxDataTable(
                    {
                        width: '100%',
                        height: 'calc(100vh - 180px)',
                        source: dataAdapter,
                        pagerPosition: 'bottom',
                        theme: 'light',
                        columnsResize: ("columnsResize" in config)?config.columnsResize:true,     
                        pageable: ("pageable" in config)?config.pageable:true,
                        altRows: ("altRows" in config)?config.altRows:true,
                        ready: function () {
                            setTimeout(() => {
                                $("span:contains('www.jqwidgets.com')").hide();
                            }, 200);
                        },
                        columns: config.columnDefs,
                    });
            } 
            else
            {                 
                // $(name_cmp).html('');
                $(name_cmp).jqxDataTable(
                {
                    width: '100%',
                    height: 'calc(100vh - 180px)',
                    source: dataAdapter,                
                    pagerPosition: 'bottom',
                    theme: 'light',
                    columnsResize: ("columnsResize" in config)?config.columnsResize:true,     
                    pageable: ("pageable" in config)?config.pageable:true,
                    altRows: ("altRows" in config)?config.altRows:true,
                    ready:function(){ 
                    setTimeout(() => {                    
                        $("span:contains('www.jqwidgets.com')").hide();                  
                    }, 200);
                    },
                    columns: config.columnDefs
                });
            }                        
        } 
        callback(result);
    });
}

app.aggrid_arrayData = function(data,name_cmp,config,callback) {
    
   var content = "",
        gridConfig = {
            defaultColDef: false,
            columnDefs: [],
            components:false,
            enableRangeSelection: true,
            rowSelection: 'single',
            enableColResize: true,
            enableRowResize: true,
            //rowHeight : 30,
            rowDragManaged: false,                
            rowMultiSelectWithClick:false,                        
            enableFilter: false, 
            enableSorting: true,                    
            rowHeight: 30,   
            headerHeight: 30,         
            comparator:false, 
            enableServerSideSorting: false,       
            allowContextMenuWithControlKey: true,                    
        },
        gridEvents = {
            onSelectionChanged:null,
            onRowDoubleClicked: null,
            onGridSizeChanged: () => {
                gridConfig.api.sizeColumnsToFit();
            },        
            onColumnResized:function() {
                
            },
            onGridReady:function() {
                
            },
            getRowClass:function() {

            }
        };

       for(var idx in gridConfig) 
       {
            if (idx in config)
            {
                gridConfig[idx] = config[idx];
            }
       }

       for(var idx in gridEvents) 
       {
            if (idx in config && typeof config[idx] == 'function')
            {                    
                gridConfig[idx] = config[idx];

            } else {
                gridConfig[idx] = gridEvents[idx];                    
            }
       }

       

    if (data)
    {
        var gridDiv = document.querySelector(name_cmp);
        if (data.length == 0)
        {
            $(name_cmp).html('');
            var gridOptions = gridConfig;                
            new agGrid.Grid(gridDiv, gridOptions);
            gridOptions.api.setRowData(data);

        } 
        else
        {       
            $(name_cmp).html('');
            var gridOptions = gridConfig;
           
            new agGrid.Grid(gridDiv, gridOptions);

            gridOptions.api.setRowData(data);                                                                                            
        }                        
    } 
    if (callback) {
        callback(gridOptions);    
    }
}

app.formatRupiah = function(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}